/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author ADMIN
 */
public class PeriodDate {

    public static String countDate(Timestamp inputTimestamp) {
        // Lấy thời gian hiện tại
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

        // Tính khoảng thời gian giữa hai thời điểm
        Duration duration = Duration.between(inputTimestamp.toInstant(), currentTimestamp.toInstant());
        // Tính số giây, phút, giờ, ngày, tháng và năm
        long secondsAgo = duration.getSeconds();
        long minutesAgo = duration.toMinutes();
        long hoursAgo = duration.toHours();
        long daysAgo = duration.toDays();
        long monthsAgo = daysAgo / 30; // Giả sử mỗi tháng có 30 ngày
        long yearsAgo = daysAgo / 365; // Giả sử mỗi năm có 365 ngày

        String result = getFormattedDuration(yearsAgo, monthsAgo, daysAgo, hoursAgo, minutesAgo, secondsAgo);
        return result;
    }

    public static String getFormattedDuration(long years, long months, long days, long hours, long minutes, long seconds) {
        if (years > 0) {
            return years + (years == 1 ? " year ago" : " years ago");
        } else if (months > 0) {
            return months + (months == 1 ? " month ago" : " months ago");
        } else if (days > 0) {
            return days + (days == 1 ? " day ago" : " days ago");
        } else if (hours > 0) {
            return hours + (hours == 1 ? " hour ago" : " hours ago");
        } else if (minutes > 0) {
            return minutes + (minutes == 1 ? " minute ago" : " minutes ago");
        } else {
            return seconds + (seconds == 1 ? " second ago" : " seconds ago");
        }
    }

    public static void main(String[] args) {
        // Tạo một biến dạng Instant từ timestamp
        //Instant inputInstant = Instant.ofEpochMilli(1642631532000); // Ví dụ: 2023-01-20 12:12:12
//
//        String formattedDuration = countDate(inputInstant);
//        System.out.println(formattedDuration);
    }
}
