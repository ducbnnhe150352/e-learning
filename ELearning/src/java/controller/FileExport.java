/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseDAO;
import dal.EnrollDAO;
import dal.LessonDAO;
import dal.ReviewDAO;
import dal.UserDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Course;
import model.Enroll;
import model.Lesson;
import model.Review;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "FileExport", urlPatterns = {"/FileExport"})
public class FileExport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FileExport</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FileExport at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String select = request.getParameter("select");

        if (select.equals("0")) {
            ArrayList<User> listUser = new ArrayList<User>();
            listUser = new UserDAO().selectAll();
            request.setAttribute("listUser", listUser);
        } else if (select.equals("1")) {
            ArrayList<Course> listCourse = new ArrayList<Course>();
            listCourse = new CourseDAO().selectAll();
            request.setAttribute("listCourse", listCourse);
        } else if (select.equals("2")) {
            ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
            listLesson = new LessonDAO().selectAll();
            request.setAttribute("listLesson", listLesson);
        }else if(select.equals("3")){
            ArrayList<Enroll> listEnroll = new ArrayList<Enroll>();
            listEnroll = new EnrollDAO().selectAll();
            request.setAttribute("listEnroll", listEnroll);
        }else if(select.equals("4")){
              ArrayList<Review> listReview = new ArrayList<Review>();
            listReview = new ReviewDAO().selectAll();
            request.setAttribute("listReview", listReview);
        }
        // request.getRequestDispatcher("/admin/exportFile.jsp?select=user").forward(request, response);

        request.setAttribute("select", select);
        RequestDispatcher rd = request.getRequestDispatcher("/admin/exportFile.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
