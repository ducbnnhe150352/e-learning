/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.CourseDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;

import java.util.ArrayList;
import model.Category;
import model.User;
import java.io.File;
import model.Course;

/**
 *
 * @author KinNhun
 */
@MultipartConfig
public class TeacherEditCourses extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TeacherEditCourses</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TeacherEditCourses at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryDAO cd = new CategoryDAO();
        ArrayList<Category> listCategory = cd.selectAll();
        CourseDAO courseDAO = new CourseDAO();
        String id = request.getParameter("courseID");
        int courseID = Integer.parseInt(id);
        Course course = courseDAO.selectById(courseID);

        request.setAttribute("listCategory", listCategory);
        request.setAttribute("course", course);
        request.getRequestDispatcher("/teacher/editCourse.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("courseID");
        int courseID = Integer.parseInt(id);
        String courseName = request.getParameter("courseName");
        int categoryId = Integer.parseInt(request.getParameter("category"));
        String description = request.getParameter("description");

        CourseDAO courseDAO = new CourseDAO();

        Part part = request.getPart("courseImg");
        String fileName = part.getSubmittedFileName();

        // Get the existing image path for the course you're about to update
        String existingImagePath = courseDAO.getCourseImageByCourseID(courseID); // Replace with your actual method to retrieve the image path

        boolean check =new  CourseDAO().editCourses(courseID, courseName, categoryId, description, fileName);

        if (check) {
            String path2 = getServletContext().getRealPath("") + "imageStorage\\course";
            String path_new = path2.replace(String.valueOf("\\build"), "");
            File file = new File(path_new);

            // Delete the old image
            if (existingImagePath != null && !existingImagePath.isEmpty()) {
                File existingImageFile = new File(path_new + File.separator + existingImagePath);
                if (existingImageFile.exists()) {
                    existingImageFile.delete();
                }
            }

            // Save the new image
            part.write(path_new + File.separator + fileName);
        } else {
            System.out.println("Error");
        }

        response.sendRedirect("about-courses?courseID=" + courseID);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
