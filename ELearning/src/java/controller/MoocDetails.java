/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.ExamDAO;
import dal.MoocDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Exam;
import model.Mooc;
import util.Helpers;

/**
 *
 * @author Thanh
 */
public class MoocDetails extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MoocDetails</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MoocDetails at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String moocidp = request.getParameter("moocid");
        String searchQuery = request.getParameter("searchQuery");
        int currentPage = Helpers.parseInt(request.getParameter("page"), 1);
        int pageSize = 5;
        int courseId = Helpers.parseInt(request.getParameter("courseid"), 1);

        // Exam DAO logic
        ExamDAO examDao = new ExamDAO();
        List<Exam> examList = examDao.selectExamByCourseIdAndName(courseId, searchQuery, currentPage, pageSize);

        // Mooc DAO logic
        int moocid;
        MoocDAO md = new MoocDAO();
        if (moocidp == null) {
            moocid = md.getFisrtMoocID(courseId);
        } else {
            moocid = Integer.parseInt(moocidp);
        }
        Mooc m = md.selectById(moocid);

        if (moocid == -1 || m == null) {
            String msg = "mooc does not exist";
            request.setAttribute("msg", msg);
        }

        int startPage = Math.max(1, currentPage - 5 / 2);
        int endPage = Math.min(startPage + 5 - 1, new ExamDAO().getTotalPages(courseId, searchQuery, pageSize));
        // Setting the attributes
        request.setAttribute("examList", examList);
        request.setAttribute("searchQuery", searchQuery);
        request.setAttribute("pageSize", pageSize);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("mooc", m);
        request.setAttribute("courseid", courseId);
        request.setAttribute("totalPages", examDao.getTotalPages(courseId, searchQuery, pageSize));
        request.setAttribute("maxVisiblePages", 5);
        request.setAttribute("startPage", startPage);
        request.setAttribute("endPage", endPage);

        request.getRequestDispatcher("/Lessons/MoocDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String courseid = request.getParameter("courseid");
        String moocname = request.getParameter("moocname");
        String moocid = request.getParameter("moocid");
        MoocDAO md = new MoocDAO();
        boolean c = md.updateMooc(moocid, moocname);
        response.sendRedirect("MoocDetails?courseid=" + courseid + "&moocid=" + moocid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
