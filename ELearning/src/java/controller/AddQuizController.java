package controller;

import com.google.gson.Gson;
import dal.ExamDAO;
import dal.QuizDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Course;
import model.Exam;
import model.Quiz;
import util.Helpers;

import java.io.IOException;
import java.io.PrintWriter;

public class AddQuizController extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EnrollServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EnrollServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String examIdPara = request.getParameter("examId").trim();
       int examId = Helpers.parseInt(examIdPara, -1);
       if(examId == -1){
           handleException(response);
           return;
       }
        Quiz quiz = new Quiz();
       Exam exam = new ExamDAO().selectExamByExamId(examId);
       if(exam == null){
           handleException(response);
           return;
       }
       quiz.setExamId(exam);
       quiz.setQuizContent("New question");
       boolean flagInsert = new QuizDAO().AddQuiz(quiz);
       if(flagInsert){
           Quiz newestQuiz = new QuizDAO().selectLastQuiz();
           String objectToString = new Gson().toJson(newestQuiz);
           response.getWriter().write(objectToString);
       }
       else {
           handleException(response);
           return;
       }

    }

    private void handleException(HttpServletResponse response) throws IOException {
        response.setStatus(400);
        response.getWriter().write("Error when add new question");
        return;
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo


    () {
        return "Short description";
    }// </
}