/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dal.CommentDAO;
import dal.ReplyDAO;
import dal.ReplyReactionDAO;
import dal.UserDAO;
import dal.UserReactionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Reply;
import model.ReplyReaction;
import model.User;
import model.UserReaction;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "Comment", urlPatterns = {"/Comment"})
public class Comment extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Comment</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Comment at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String action = request.getParameter("action");
            int myVariable = Integer.parseInt(request.getParameter("dataValue"));
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("acc");
            System.out.println(user);
            if ("get".equals(action)) {

                //int userId = Integer.parseInt(request.getParameter("userid"));
                System.out.println(user.getUserId());

                CommentDAO d = new CommentDAO();
                ArrayList<model.Comment> listComment = new ArrayList<model.Comment>();
                listComment = d.selectCommentByLessonId(myVariable);

                UserReactionDAO userReactionDao = new UserReactionDAO();
                ArrayList<UserReaction> listCommentReaction = new ArrayList<UserReaction>();
                listCommentReaction = userReactionDao.selectCommentIdReaction(myVariable, user.getUserId());
                //     System.out.println("ketquala"+listCommentReaction);

                ReplyDAO replyDao = new ReplyDAO();
                Map<Integer, Integer> mapTotalReply = new HashMap<>();
                mapTotalReply = replyDao.getListTotalReplyByCommentId();

                System.out.println(listComment);
                System.out.println(listCommentReaction);
                System.out.println(mapTotalReply);

                //  ArrayList<Integer> listTotalReply = new ArrayList<>();
                //   listTotalReply = replyDao.getListTotalReplyByCommentId();
                Gson gson = new Gson();

                String listCommentJson = gson.toJson(listComment);
                String listCommentReactionJson = gson.toJson(listCommentReaction);
                String mapTotalReplyJson = gson.toJson(mapTotalReply);

                //   String listlistTotalReplyJson = gson.toJson(listTotalReply);
                JsonObject responseObject = new JsonObject();
                responseObject.addProperty("listComment", listCommentJson);
                responseObject.addProperty("listCommentReaction", listCommentReactionJson);
                responseObject.addProperty("mapTotalReply", mapTotalReplyJson);

                String jsonResponse = responseObject.toString();

                // Gửi JSON về cho Ajax
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(jsonResponse);
            } else if ("deleleComment".equals(action)) {
                int lessonId = Integer.parseInt(request.getParameter("lessonId"));

                CommentDAO commentDao = new CommentDAO();
                int check = commentDao.deleteCommentById(myVariable);
                if (check == 1) {
                    CommentDAO d = new CommentDAO();
                    ArrayList<model.Comment> listComment = new ArrayList<model.Comment>();
                    listComment = d.selectCommentByLessonId(lessonId);
                    String jsonList = new Gson().toJson(listComment);

                    // Gửi JSON về cho Ajax
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(jsonList);
                } else {
                    response.setContentType("text/plain");
                    response.getWriter().write("failed");
                }
            } else if ("getReply".equals(action)) {
                // int userId = Integer.parseInt(request.getParameter("userId"));

                // System.out.println("commentidduc" + myVariable);
                //  System.out.println("useridduc" + userId);
                ReplyDAO replyDAO = new ReplyDAO();
                ArrayList<Reply> listReply = new ArrayList<Reply>();
                listReply = replyDAO.selectAllReplyByCommentId(myVariable);

                ReplyReactionDAO replyReactionDao = new ReplyReactionDAO();
                ArrayList<ReplyReaction> listReplyReaction = new ArrayList<ReplyReaction>();
                listReplyReaction = replyReactionDao.selectReplyIdReaction(myVariable, user.getUserId());

                System.out.println("size la " + listReplyReaction.size());

                Gson gson = new Gson();

                String listReplyJson = gson.toJson(listReply);
                String listReplyReactionJson = gson.toJson(listReplyReaction);

                //   String listlistTotalReplyJson = gson.toJson(listTotalReply);
                JsonObject responseObject = new JsonObject();
                responseObject.addProperty("listReply", listReplyJson);
                responseObject.addProperty("listReplyReaction", listReplyReactionJson);

                String jsonResponse = responseObject.toString();

                // Gửi JSON về cho Ajax
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(jsonResponse);

            } else if ("saveEditComment".equals(action)) {
                String content = request.getParameter("content");
                int dataValue = Integer.parseInt(request.getParameter("dataValue"));
                System.out.println(content);
                System.out.println(dataValue);

                CommentDAO commentDao2 = new CommentDAO();
                int check = commentDao2.updateCommentContent(dataValue, content);
                String mess = null;
                if (check == 1) {
                    mess = "ok";
                } else {
                    mess = "notok";
                }
                String jsonList = new Gson().toJson(mess);
                // Gửi JSON về cho Ajax
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(jsonList);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
