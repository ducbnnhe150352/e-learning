/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.ReplyDAO;
import dal.ReplyReactionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ReplyReaction", urlPatterns = {"/ReplyReaction"})
public class ReplyReaction extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ReplyReaction</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ReplyReaction at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
          String action = request.getParameter("action");
        int replyId = Integer.parseInt(request.getParameter("dataValue"));
        ReplyDAO replyDao = new ReplyDAO();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("acc");

        if ("addlike".equals(action)) {
            replyDao.addLikeByReplyId(replyId);
            if (new ReplyReactionDAO().checkReplyReation(user.getUserId(), replyId)) {
                new ReplyReactionDAO().AddLikeReplyReaction(user.getUserId(), replyId);
            } else {
                new ReplyReactionDAO().AddNewLikeReplyReaction(user.getUserId(), replyId);
            }
        } else if ("sublike".equals(action)) {
            replyDao.subLikeByReplyId(replyId);
            new ReplyReactionDAO().SubLikeReplyReaction(user.getUserId(), replyId);
        } else if ("subdislike".equals(action)) {
            replyDao.subDisLikeByReplyId(replyId);
            new ReplyReactionDAO().SubDisLikeReplyReaction(user.getUserId(), replyId);

        } else if ("sublikeandaddislike".equals(action)) {
            replyDao.subLikeByReplyId(replyId);
            ReplyDAO replydao2 = new ReplyDAO();
            replydao2.addDisLikeByReplyId(replyId);
            new ReplyReactionDAO().SubLikeReplyReaction(user.getUserId(), replyId);
            new ReplyReactionDAO().AddDisLikeReplyReaction(user.getUserId(), replyId);

        } else if ("adddislike".equals(action)) {
            replyDao.addDisLikeByReplyId(replyId);
            if (new ReplyReactionDAO().checkReplyReation(user.getUserId(), replyId)) {
                new ReplyReactionDAO().AddDisLikeReplyReaction(user.getUserId(), replyId);
            } else {
                new ReplyReactionDAO().AddNewDisLikeReplyReaction(user.getUserId(), replyId);
            }
        } else if ("subdislikeaddlike".equals(action)) {
            replyDao.subDisLikeByReplyId(replyId);
            ReplyDAO replyDao3 = new ReplyDAO();
            replyDao3.addLikeByReplyId(replyId);
            new ReplyReactionDAO().SubDisLikeReplyReaction(user.getUserId(), replyId);
            new ReplyReactionDAO().AddLikeReplyReaction(user.getUserId(), replyId);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
