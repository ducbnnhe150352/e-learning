package controller;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class ExportCertificateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String certId = request.getParameter("id");
        String baseUrl = request.getRequestURL().toString();
        String htmlContent = fetchHtmlContent(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath(), certId);

        String fontFamilies = "Open+Sans|Pinyon+Script|Rochester";
        String encodedFontFamilies = URLEncoder.encode(fontFamilies, "UTF-8");
        String fontUrl = "https://fonts.googleapis.com/css?family=" + encodedFontFamilies;

        // Replace the font URL in your HTML content with the encoded one
        htmlContent = htmlContent.replace(
                "https://fonts.googleapis.com/css?family=Open+Sans|Pinyon+Script|Rochester",
                fontUrl);
        try (OutputStream os = response.getOutputStream()) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();

            // Set the output stream
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            builder.toStream(os);

            // Set the base URL
            builder.withUri(request.getRequestURL().toString());

            // Set the HTML content
            builder.withHtmlContent(htmlContent, request.getRequestURL().toString());

            // Run the PDF conversion
            builder.run();

            // Set the response content type to PDF
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=\"certificate.pdf\"");

            // Write the PDF bytes to the servlet response
            baos.writeTo(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fetchHtmlContent(String scheme, String serverName, int serverPort, String contextPath, String certId) throws IOException {
        // Construct the URL to the certificatedetail servlet
        URL url = new URL(scheme, serverName, serverPort, contextPath+ "/certificatedeatil?id=" + certId);

        // Get the response stream from the connection
        InputStream inputStream = url.openStream();

        // Read the response into a String
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder htmlResponse = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            htmlResponse.append(line);
        }
        reader.close();

        return htmlResponse.toString();
    }
}
