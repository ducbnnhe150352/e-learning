/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.MoocDAO;
import dal.CourseDAO;
import dal.LessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Course;
import model.Lesson;
import model.Mooc;
import model.User;

/**
 *
 * @author Thanh
 */
public class CreateLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateLesson</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateLesson at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        User u = new User();
        u.setFullName("ThanhDV");
        CourseDAO cd = new CourseDAO();
        String CourseIDp = request.getParameter("courseid");
        String moocIDp = request.getParameter("moocid");
        int moocid = Integer.parseInt(moocIDp);
        int CourseID = Integer.parseInt(CourseIDp);
        Course c = cd.selectById(CourseID);
        LessonDAO ld = new LessonDAO();
        ArrayList<Lesson> le = ld.selectLessonByMoocId(moocid);
        request.setAttribute("courseid", CourseID);
        int lastNumberLesson = 0;
        if (le.isEmpty()) {
            lastNumberLesson = new LessonDAO().GetMaxNumberLessonByCourseId(CourseID);
            if (lastNumberLesson == 0) {
                lastNumberLesson = 0;
            }
        } else {
            lastNumberLesson = le.get(le.size() - 1).getLessonNumber();
        }

        request.setAttribute("courcename", c.getCourseName());
        request.setAttribute("nameuser", u.getFullName());
        request.setAttribute("moocid", moocid);
        request.setAttribute("lastNumberLesson", lastNumberLesson + 1);
        request.getRequestDispatcher("/Lessons/createLesson.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String lessonNumber = request.getParameter("lessonNumber");
        String lessonName = request.getParameter("lessonName");
        String lessonUrl = request.getParameter("lessonUrl");
        if (lessonUrl.length() > 32) {
            lessonUrl = extractVideoId(lessonUrl);
        }
        String Description = request.getParameter("Description");
        StringBuffer stringBuffer = new StringBuffer(Description);
        if (Description.contains("'")) {
            int a = Description.indexOf("'");
            System.out.println(a);
            stringBuffer.insert(a, "'");
        }

        String moocid = request.getParameter("moocid");
        LessonDAO ld = new LessonDAO();
        ld.AddAnewLesson(lessonNumber, lessonName, moocid, lessonUrl, Description);
        LessonDAO ld2 = new LessonDAO();
        int id = ld2.getLessonIdByNumberAndMoocID(lessonNumber, moocid);
        String CourseID = request.getParameter("courseid");
        response.sendRedirect("UpdateLesson?courseid=" + CourseID + "&moocid=" + moocid.trim() + "&lessonid=" + id);
    }

    public String extractVideoId(String youtubeUrl) {
        StringBuilder id = new StringBuilder("");
        for (int i = 32; i < youtubeUrl.length(); i++) {
            if (youtubeUrl.charAt(i) != '&') {
                id.append(youtubeUrl.charAt(i));
            } else {
                break;
            }
        }
        return id.toString();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
