/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.CourseDAO;
import dal.MoocDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Course;

/**
 *
 * @author Thanh
 */
public class CreateMooc extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateMooc</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateMooc at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String courseidp = request.getParameter("courseid");
        int courseid = Integer.parseInt(courseidp);
        CourseDAO cd = new CourseDAO();
        int number = cd.getNumberMax(courseidp);
        request.setAttribute("lastnumber", number+1);
        request.setAttribute("courseid", courseid);
        request.getRequestDispatcher("/Lessons/CreateMooc.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String moocnumber = request.getParameter("moocnumber");
        String moocname = request.getParameter("moocname");
        String courseid = request.getParameter("courseid");
        int number = Integer.parseInt(moocnumber);
        int course = Integer.parseInt(courseid);
        MoocDAO md = new MoocDAO();
        boolean c = md.addANewMooc(moocnumber, moocname, courseid);
        MoocDAO md1 = new MoocDAO();
        int moocid = md1.GetIdMooc(number, course);
        if(c){
            response.sendRedirect("MoocDetails?moocid="+moocid+"&courseid="+courseid);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
