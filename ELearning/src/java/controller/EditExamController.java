package controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dal.ExamDAO;
import dal.QuizDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Course;
import model.Exam;
import model.Quiz;
import util.Helpers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class EditExamController extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EnrollServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EnrollServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int examId = Helpers.parseInt(request.getParameter("examId"), -1);
        ExamDAO dao = new ExamDAO();
        Exam exam = dao.selectExamByExamId(examId);
        request.setAttribute("exam", exam);
        if(examId != -1){
            List<Quiz> quizList = new QuizDAO().selectAllQuizByExamID(examId);
            request.setAttribute("listQuiz", quizList);
        }
        request.getRequestDispatcher("/edit-exam/edit-exam.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String examExamId = request.getParameter("examId").trim();
        String examName = request.getParameter("examName").trim();
        String examDuration = request.getParameter("examDuration").trim();
        String examQuantity = request.getParameter("examQuantity").trim();
        String examCourseId = request.getParameter("examCourseId").trim();
        ExamDAO dao = new ExamDAO();
        Exam exam = new Exam();
        exam.setExamName(examName);
        exam.setDuration(Helpers.parseInt(examDuration, 1));
        exam.setQuantity(Helpers.parseInt(examQuantity, 1));
        Course course = new Course();
        course.setCourseId(Helpers.parseInt(examCourseId, 1));
        exam.setCourseId(course);
        boolean flagUpdate = false;
        if (Helpers.isNullOrEmpty(examExamId) || Helpers.parseInt(examExamId, -1) == -1) {
            flagUpdate = dao.addExam(exam);
        }
        else{
            int examId = Helpers.parseInt(examExamId, 1);
            exam.setExamId(examId);
            flagUpdate = dao.updateExam(exam);
        }
        if(flagUpdate){
            if(Helpers.isNullOrEmpty(examExamId)){
                dao = new ExamDAO();
                Exam newestExam = dao.selectLastExam();
                String objectToString = new Gson().toJson(newestExam);
                response.getWriter().write(objectToString);
            }
            else{
                response.getWriter().write("Save successfully");
            }
        }
        else{
            response.getWriter().print("Failed when save");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo


    () {
        return "Short description";
    }// </
}
