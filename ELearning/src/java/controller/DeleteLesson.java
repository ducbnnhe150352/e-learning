/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.LessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Lesson;

/**
 *
 * @author Thanh
 */
public class DeleteLesson extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteLesson</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteLesson at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("lessonid");
        String moocid = request.getParameter("moocid");
        String courseid = request.getParameter("courseid");
        String lessonNumber = request.getParameter("lessonNumber");
        LessonDAO ld = new LessonDAO();
        ld.deleteLesson(id, moocid);
        int id1 = -1;
        if (lessonNumber.equals("1")) {
            LessonDAO lsd = new LessonDAO();
            id1 = lsd.getLessonIdByNumberAndMoocID("1", moocid);
            if (id1 == -1) {
                String msg = "Lesson not exist in mooc";
                request.setAttribute("msg", msg);
            } else {
                LessonDAO ld1 = new LessonDAO();
                Lesson lu = ld1.selectById(id1);
                request.setAttribute("Lesson", lu);
            }

        } else {
            LessonDAO ld2 = new LessonDAO();
            String num = (Integer.parseInt(lessonNumber) - 1) + "";
            int id2 = ld2.getLessonIdByNumberAndMoocID(num, moocid);
            LessonDAO ld1 = new LessonDAO();
            Lesson lu = ld1.selectById(id2);
            request.setAttribute("Lesson", lu);
        }
        request.setAttribute("courseid", Integer.parseInt(courseid));
        request.setAttribute("moocid", moocid);
        request.getRequestDispatcher("/Lessons/updateLesson.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
