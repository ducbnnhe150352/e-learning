/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AccountDAO;
import dal.CertificateDao;
import dal.UserDAO;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.*;
import model.Certificate;
import model.User;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.servlet.ServletException;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author admin
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 10)   	// 10 MB
public class UpdateProfileServlet extends HttpServlet {

    private static final String DATA_DIRECTORY = "\\imageStorage\\user";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateProfileServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateProfileServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //String aid_raw = request.getParameter("aid");
        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("acc");

            AccountDAO profileDao = new AccountDAO();
            User users = profileDao.getAccountById(user.getUserId());
            //User user = profileDao.getAccountById(aid);
//            request.setAttribute("account", user);
//            request.setAttribute("passAcc", user.getPassword());
            System.out.println("PassAcc" + users.getPassword());
            request.setAttribute("up", users);
            request.setAttribute("passAcc", users.getPassword());
            CertificateDao certificateDao = new CertificateDao();
            List<Certificate> certificateList = certificateDao.getCertificateList(user.getUserId());
            request.setAttribute("userid", user.getUserId());
            request.setAttribute("certificate", certificateList);
            request.getRequestDispatcher("userProfile/ProfileUser.jsp").forward(request, response);

        } catch (Exception e) {
            Logger.getLogger(UpdateProfileServlet.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String uploadFolder = getServletContext().getRealPath("")
//                + File.separator + DATA_DIRECTORY;// constructs the folder where uploaded file will be stored
        String path2 = getServletContext().getRealPath("") + "imageStorage\\user";

        String path_new = path2.replace(String.valueOf("\\build"), "");
        File uploadDir = new File(path_new);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        HttpSession session = request.getSession(false); //get session from request
        User loginUser = (User) session.getAttribute("acc");
        String avatarPath = null;
        Part avatar = request.getPart("avatar"); //vatar.getSubmittedFileName()) != null || !FilenameUtils.getExtensiget part object of avatar
        String fileExtension = FilenameUtils.getExtension(avatar.getSubmittedFileName()).trim();
        if (!fileExtension.equals("")) {
            String fileName = "ava_" + loginUser.getUserId() + "." + fileExtension; // create name for picture
            InputStream is = avatar.getInputStream();
            Files.copy(is, Paths.get(path_new + File.separator + fileName), StandardCopyOption.REPLACE_EXISTING); //upload to server image
            avatarPath = fileName;
        }
        String name = request.getParameter("name").trim();
        String mobile = request.getParameter("mobile").trim();
        String address = request.getParameter("address").trim();
        String aid_raw = request.getParameter("aid").trim();
        String gender = request.getParameter("gender").trim();
        String oldpass = request.getParameter("oldpass").trim();
        String newpass = request.getParameter("newpass").trim();
        String repass = request.getParameter("repass").trim();
        String button = request.getParameter("update").trim();
        String dob = request.getParameter("dob").trim();

        System.out.println("button" + button);

        User us = loginUser;
        AccountDAO profileDao = new AccountDAO();

        try {
            us.setFullName(name);
            if (gender.equals("Male")) {
                us.setGender(true);
            }
            if (gender.equals("Female")) {
                us.setGender(false);
            }

            us.setPhone(mobile);
            us.setAddress(address);
            if (avatarPath != null) {
                us.setAvatar(avatarPath);
            }
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
            java.sql.Date dobConvert = new java.sql.Date(date.getTime());
            us.setDateOfBirth(dobConvert);
            int aid = Integer.parseInt(aid_raw);
            if (button.equals("profile")) {
                profileDao.updateProfile(us, aid_raw);
                request.setAttribute("updateSuccess", true);
            }
            if (button.equals("pass")) {
                profileDao.changePassword(newpass, aid);
                request.setAttribute("updateSuccess", true);
            }
            System.out.println(oldpass);
//            int aid = Integer.parseInt(aid_raw);
//
//            if (button.equals("profile")) {
//                us.setFullName(name);
//               // us.setDateOfBirth(dateOfBirth);
//                us.setPhone(mobile);
//                us.setAddress(address);
//                us.setEmail(email);
//                us.setUserId(aid);
//                int checkUpdate = profileDao.updateProfile(us);
//                if (checkUpdate != 0) {
//                    doGet(request, response);
//                } else {
//                    System.out.println("error server");
//                }
//            }
//            if (button.equals("password")) {
//                int checkChangePass = profileDao.changePassword(newpass, aid);
//                if (checkChangePass != 0) {
//                    doGet(request, response);
//                } else {
//                    System.out.println("error server");
//                }
//            }
            doGet(request, response);

            System.out.println(button);
        } catch (Exception e) {
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
