package controller;

import com.google.gson.Gson;
import dal.ChoiceDAO;
import dal.ExamDAO;
import dal.QuizDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Choice;
import model.Exam;
import model.Quiz;
import util.Helpers;

import java.io.IOException;
import java.io.PrintWriter;

public class AddChoiceController extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EnrollServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EnrollServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String quizIdPara = request.getParameter("quizId").trim();
        int quizId = Helpers.parseInt(quizIdPara, -1);
        if(quizId == -1){
            handleException(response);
            return;
        }
        Choice choice = new Choice();
        Quiz quiz = new QuizDAO().selectById(quizId);
        if(quiz == null){
            handleException(response);
            return;
        }
        choice.setQuizId(quiz);
        choice.setDescription("New answer");
        choice.setIsTrue(false);
        boolean flagInsert = new ChoiceDAO().addChoice(choice);
        if(flagInsert){
            Choice newestChoice = new ChoiceDAO().selectLastestChoice();
            String objectToString = new Gson().toJson(newestChoice);
            response.getWriter().write(objectToString);
        }
        else {
            handleException(response);
            return;
        }

    }

    private void handleException(HttpServletResponse response) throws IOException {
        response.setStatus(400);
        response.getWriter().write("Error when add new answer");
        return;
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo


    () {
        return "Short description";
    }// </
}