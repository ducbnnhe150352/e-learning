/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.CourseDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;

import java.util.ArrayList;
import model.Category;
import model.User;
import java.io.File;



@MultipartConfig
public class TeacherAddCourses extends HttpServlet {

    private static final String DATA_DIRECTORY = "\\imageStorage\\course";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TeacherAddCourses</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TeacherAddCourses at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryDAO cd = new CategoryDAO();
        ArrayList<Category> listCategory = cd.selectAll();
        request.setAttribute("listCategory", listCategory);
        request.getRequestDispatcher("/teacher/createNewCourse.jsp").forward(request, response);
        }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    //   @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        HttpSession session = request.getSession();
//        User u = (User) session.getAttribute("teacher");
//
//        CourseDAO courseDAO = new CourseDAO();
//
//        int userId = u.getUserId();
//        int categoryId = Integer.parseInt(request.getParameter("category"));
//        String courseImg = request.getParameter("courseImg");
//        String courseName = request.getParameter("courseName");
//        int newVersionId = Integer.parseInt(request.getParameter("newVersionId"));
//        String description = request.getParameter("description");
//
//        boolean insertResult = courseDAO.insertCourse(userId, categoryId, courseImg, courseName, newVersionId, description);
//        String mess = null;
//        if (insertResult) {
//          
//
//        } else {
//
//           
//        }
//       response.sendRedirect("all-courses");
//       // request.getRequestDispatcher("").forward(request, response);
//    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("teacher");

        CourseDAO courseDAO = new CourseDAO();

        int userId = u.getUserId();
        int categoryId = Integer.parseInt(request.getParameter("category"));
        String courseName = request.getParameter("courseName");
        String description = request.getParameter("description");

        Part part = request.getPart("courseImg");
        System.out.println(part);

        String fileName = part.getSubmittedFileName();
        System.out.println(fileName);
        
        
      boolean check= courseDAO.insertCourse(userId, categoryId, fileName, courseName, description);
            //imgDAO dao = new imgDAO();
       // int check = courseDAO.saveImgInFolder(fileName);
        if (check == true) {
            String path2 = getServletContext().getRealPath("") + "imageStorage\\course";
            
            System.out.println(path2);
            
            String path_new = path2.replace(String.valueOf("\\build"), "");
            
            System.out.println(path_new);
            
            File file = new File(path_new);
            part.write(path_new + File.separator + fileName);
        } else {
            System.out.println("error");
        }
        response.sendRedirect("all-courses");
        // Xử lý tệp tải lên
//        Part filePart = request.getPart("courseImg"); // Tên trường <input type="file>
//        String fileName = getFileName(filePart);

//        // Lưu tệp vào thư mục
//        String uploadFolder = getServletContext().getRealPath("") + File.separator + DATA_DIRECTORY;
//        File uploadDir = new File(uploadFolder);
//        if (!uploadDir.exists()) {
//            uploadDir.mkdirs();
//        }
//        String filePath = uploadFolder + File.separator + fileName;
//        filePart.write(filePath);
//
//        boolean insertResult = courseDAO.insertCourse(userId, categoryId, filePath, courseName, newVersionId, description);
//        String mess = null;
//        if (insertResult) {
//            // Xử lý khi tạo khóa học thành công
//            // Redirect hoặc gửi thông báo thành công
//        } else {
//            // Xử lý khi có lỗi xảy ra
//            // Redirect hoặc gửi thông báo lỗi
//        }
    }

// Phương thức để lấy tên tệp từ Part
    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
