/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dal.TeacherDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "RequestTeacher", urlPatterns = {"/RequestTeacher"})
public class RequestTeacher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RequestTeacher</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RequestTeacher at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String job = request.getParameter("job");
        String place = request.getParameter("place");
        String website = request.getParameter("website");
        String facebook = request.getParameter("facebook");
        String linkedin = request.getParameter("linkedin");
        String cv = request.getParameter("cv");
        String about = request.getParameter("about");

        System.out.println(job);
        System.out.println(place);
        System.out.println(website);
        System.out.println(facebook);
        System.out.println(linkedin);
        System.out.println(cv);
        System.out.println(about);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("acc");
        String job = request.getParameter("job");
        String place = request.getParameter("place");
        String website = request.getParameter("website");
        String facebook = request.getParameter("facebook");
        String linkedin = request.getParameter("linkedin");
        String cv = request.getParameter("cv");
        String about = request.getParameter("about");
        long timestamp = System.currentTimeMillis();
        Timestamp timestampObj = new Timestamp(timestamp);
        int check = new TeacherDAO().insertTeacherRequest(user.getUserId(), job, place, website, facebook, linkedin, about, cv, timestampObj);

        String jsonList = new Gson().toJson(check);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonList);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
