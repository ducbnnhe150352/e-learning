/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dal.CertificateDao;
import dal.EnrollDAO;
import dal.ExamDAO;
import dal.ExamDetailDAO;
import dal.LessonDAO;
import dal.ProgressDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Exam;
import model.Lesson;
import model.User;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "Exam", urlPatterns = {"/Exam"})
public class ExamDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Exam</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Exam at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        if (action.equals("updateProgress")) {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("acc");
            int userId = user.getUserId();

            int courseId = Integer.parseInt(request.getParameter("courseId"));
            int count = Integer.parseInt(request.getParameter("count"));

            int totalExam = new ExamDAO().countNumberExam(courseId);
            int totallesson = new LessonDAO().countLessonByCourseId(courseId);

            int total = totalExam + totallesson;

            int progess = Math.round(count * 100 / total);
            if (progess == 100) {
                new CertificateDao().addCertificate(userId, courseId);
            }
            new EnrollDAO().updateProgress(userId, courseId, progess);

        } else {

            int dataValue = Integer.parseInt(request.getParameter("dataValue"));
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("acc");
            int userId = user.getUserId();
            System.out.println(dataValue);

            ExamDAO examDao = new ExamDAO();
            Exam exam = new Exam();
            exam = examDao.selectExamByExamId(dataValue);

            ExamDetailDAO examDetailDao = new ExamDetailDAO();
            ArrayList<model.ExamDetail> listExamDetail = new ArrayList<model.ExamDetail>();
            listExamDetail = examDetailDao.selectExamDetailByUserExamID(userId, dataValue);

            Gson gson = new Gson();
            String examObject = gson.toJson(exam);
            String examDetailList = gson.toJson(listExamDetail);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            response.getWriter().write("{\"list\":" + examDetailList + ",\"object\":" + examObject + "}");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
