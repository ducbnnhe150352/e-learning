/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Comment;
import model.Reply;
import model.User;

/**
 *
 * @author ADMIN
 */
public class ReplyDAO extends DBContext {

    public ArrayList selectAllReplyByCommentId(int commentId) {
        ArrayList<Reply> listReply = new ArrayList<Reply>();
        Reply reply = null;
        try {
            String sql = "SELECT * FROM Reply where CommentID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, commentId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                int replyId = rs.getInt("ReplyID");
                Comment comment = new CommentDAO().selectById(commentId);
                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                String content = rs.getString("Content");
                Timestamp dateReply = rs.getTimestamp("DateReply");
                String image = rs.getString("Image");
                Boolean status = rs.getBoolean("Status");
                int like = rs.getInt("Like");
                int disLike = rs.getInt("DisLike");

                reply = new Reply(replyId, comment, user, content, dateReply, image, status, like, disLike);

                listReply.add(reply);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listReply;
    }

    public int addNewReply(int commentId, int userId, String content, Timestamp commentDate) {
        try {
            String sql = "INSERT INTO Reply\n"
                    + "( CommentID, UserID, Content, DateReply, [Image], Status, [Like], Dislike)\n"
                    + "VALUES( ?, ?, ?, ?,'', 0, 0, 0);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, commentId);
            st.setInt(2, userId);
            st.setString(3, content);
            st.setTimestamp(4, commentDate);
            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public Map getListTotalReplyByCommentId() {
        // ArrayList<Integer> listTotalReply = new ArrayList<>();
        Map<Integer, Integer> mapTotalReply = new HashMap<>();

        try {
            String sql = "SELECT  CommentID, COUNT(CommentID) as total\n"
                    + "FROM Reply\n"
                    + "GROUP BY CommentID;";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                mapTotalReply.put(rs.getInt("CommentID"), rs.getInt("total"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return mapTotalReply;
    }

    public Reply selectReplyById(int replyId) {
        Reply reply = null;
        try {
            String sql = "Select * from Reply where ReplyID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, replyId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int commentId = rs.getInt("CommentID");
                Comment comment = new CommentDAO().selectById(commentId);
                int userId = rs.getInt("UserId");
                User user = new UserDAO().selectById(userId);
                String content = rs.getString("Content");
                Timestamp dateReply = rs.getTimestamp("DateReply");
                String image = rs.getString("image");
                boolean status = rs.getBoolean("Status");
                int like = rs.getInt("Like");
                int dislike = rs.getInt("Dislike");
                reply = new Reply(replyId, comment, user, content, dateReply, image, status, like, dislike);
            }
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return reply;
    }

    public int addLikeByReplyId(int replyId) {
        try {
            String sql = "UPDATE Reply\n"
                    + "SET [Like] = [Like] + 1\n"
                    + "WHERE ReplyID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, replyId);

            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public int subLikeByReplyId(int replyId) {
        try {
            String sql = "UPDATE Reply\n"
                    + "SET [Like] = [Like] - 1\n"
                    + "WHERE ReplyID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, replyId);

            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public int addDisLikeByReplyId(int replyId) {
        try {
            String sql = "UPDATE Reply\n"
                    + "SET [Dislike] = [Dislike] + 1\n"
                    + "WHERE ReplyID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, replyId);

            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int subDisLikeByReplyId(int replyId) {
        try {
            String sql = "UPDATE Reply\n"
                    + "SET [Dislike] = [Dislike] - 1\n"
                    + "WHERE ReplyID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, replyId);

            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }



    public static void main(String[] args) {
        ReplyDAO d = new ReplyDAO();
//        ArrayList<Reply> listReply = new ArrayList<Reply>();
//        listReply = d.selectAllReplyByCommentId(1);
//        for (Reply reply : listReply) {
//            System.out.println(reply.toString());
//        }
//        long currentTimeMillis = System.currentTimeMillis();
//
// Convert it to a Timestamp
//        Timestamp currentTimestamp = new Timestamp(currentTimeMillis);
//        int check = new ReplyDAO().addNewReply(1, 2, "1+1=2", currentTimestamp);
//        System.out.println(check);
        ArrayList<Integer> listTotalReply = new ArrayList<>();
        // listTotalReply = d.getListTotalReplyByCommentId();
        Map<Integer, Integer> mapTotalReply = new HashMap<>();
        mapTotalReply = d.getListTotalReplyByCommentId();
        for (Map.Entry<Integer, Integer> entry : mapTotalReply.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();
            System.out.println("commentid: " + key + ", total: " + value);
        }

        // System.out.println(listTotalReply);
    }

}
