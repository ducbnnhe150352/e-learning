/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Comment;
import model.Reply;
import model.ReplyReaction;
import model.User;
import model.UserReaction;

/**
 *
 * @author ADMIN
 */
public class ReplyReactionDAO extends DBContext {

    public ArrayList selectReplyIdReaction(int commentId, int userId) {
        ArrayList<ReplyReaction> listReplyReaction = new ArrayList<ReplyReaction>();
        try {
            String sql = "select rr.ReplyID, rr.UserID, rr.isDislike, rr.isLike from Reply as r inner join ReplyReaction as rr\n"
                    + " on r.ReplyID=rr.ReplyID\n"
                    + " where r.CommentID=? and rr.UserID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, commentId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User user = new UserDAO().selectById(userId);
                int replyId = rs.getInt("ReplyID");
                Reply reply = new ReplyDAO().selectReplyById(replyId);
                boolean isLike = rs.getBoolean("isLike");
                boolean isDisLike = rs.getBoolean("isDislike");
                ReplyReaction replyReaction = new ReplyReaction(user, reply, isLike, isDisLike);
                listReplyReaction.add(replyReaction);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listReplyReaction;
    }

    public int AddNewLikeReplyReaction(int userId, int commentId) {
        try {
            String sql = "INSERT INTO ReplyReaction\n"
                    + "(UserID, ReplyID, isLike, isDisLike)\n"
                    + "VALUES(?, ?, 1, 0);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, commentId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int AddLikeReplyReaction(int userId, int commentId) {
        int rs = 0;
        try {
            String sql = "UPDATE ReplyReaction\n"
                    + "SET isLike=1\n"
                    + "WHERE UserID=? AND ReplyID=?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, commentId);
            rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rs;
    }

    public int SubLikeReplyReaction(int userId, int commentId) {
        try {
            String sql = "UPDATE ReplyReaction\n"
                    + "SET isLike=0\n"
                    + "WHERE UserID=? AND ReplyID=?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, commentId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int AddNewDisLikeReplyReaction(int uuserId, int commentId) {
        try {
            String sql = "INSERT INTO ReplyReaction\n"
                    + "(UserID, ReplyID, isLike, isDisLike)\n"
                    + "VALUES(?, ?, 0, 1);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uuserId);
            st.setInt(2, commentId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public int AddDisLikeReplyReaction(int userId, int commentId) {
        try {
            String sql = "UPDATE ReplyReaction\n"
                    + "SET isDisLike=1\n"
                    + "WHERE UserID=? AND ReplyID=?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, commentId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int SubDisLikeReplyReaction(int uuserId, int commentId) {
        try {
            String sql = "UPDATE ReplyReaction\n"
                    + "SET isDisLike=0\n"
                    + "WHERE UserID=? AND ReplyID=?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, uuserId);
            st.setInt(2, commentId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public boolean checkReplyReation(int userId, int commentId) {

        try {
            String sql = "Select * from ReplyReaction where UserID=? and ReplyID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, commentId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

   

    public static void main(String[] args) {
        ArrayList<UserReaction> listUserReaction = new ArrayList<UserReaction>();
        listUserReaction = new ReplyReactionDAO().selectReplyIdReaction(1, 0);
        System.out.println(listUserReaction.size());
    }

}
