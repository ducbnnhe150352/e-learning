/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *
 * @author ADMIN
 */
public class ProgressDAO extends DBContext implements DAO {

    public static int getLessonLearnedByUserId(int userId) {

        try {
            String sql = "select SUM(LessonNumber)  from Progress\n"
                    + "where UserID=? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public static int getTotalLessonByCourseID(int courseId) {

        try {
            String sql = "Select COUNT(*) from Course AS c\n"
                    + "INNER JOIN Mooc AS m on c.CourseID=m.CourseID\n"
                    + "INNER JOIN Lesson as l on m.MoocID= l.MoocID\n"
                    + "WHERE c.CourseID =?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int getNumberLessonLerned(int courseID, int userID) {
        try {
            String sql = "select NumberLearned from Progress2 \n"
                    + "where  CourseID=? and UserID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseID);
            st.setInt(2, userID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("NumberLearned");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int addLessonProgress(int userId, int courseId) {
        try {
            String sql = "UPDATE Progress2\n"
                    + "SET NumberLearned=NumberLearned+1\n"
                    + "Where UserID=? and CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int addNewProgress(int userId, int courseId) {
        try {
            String sql = "INSERT INTO Progress2\n"
                    + "(UserID, CourseID, NumberLearned)\n"
                    + "VALUES(?, ?, 0);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    @Override
    public ArrayList selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object selectById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    //////////////////////
    public int selectFristLessonId(int courseId) {
        try {
            String sql = "select top 1 l.LessonID from Mooc as m \n"
                    + "inner join Lessons as l\n"
                    + "on m.MoocID=l.MoocID\n"
                    + "inner join Course as c\n"
                    + "on c.CourseID=m.CourseID\n"
                    + "where c.CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("LessonID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public int checkLesson(int userId, int courseId, int lessonId) {
        try {
            String sql = "select State from Progress where \n"
                    + "UserID=? and CourseID=? and LessonID =? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            st.setInt(3, lessonId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                boolean check = rs.getBoolean("State");
                if (check == true) {
                    return 1;
                } else if (check == false) {
                    return 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public int countNumberLearned(int userId, int courseId) {
        try {
            String sql = "select count(*) from Progress where\n"
                    + "UserID=? and CourseID=? and State =1";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int insertLessonLearned(int userId, int lessonId, int courseId) {
        try {
            String sql = "INSERT INTO Progress\n"
                    + "(UserID, LessonID, State, CourseID)\n"
                    + "VALUES(?, ?, 0, ?);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, lessonId);
            st.setInt(3, courseId);
            return st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            
        }
        return 0;
    }

    public int UpdateLessonLearned(int userId, int lessonId, int courseId) {
        try {
            String sql = "UPDATE Progress\n"
                    + "SET State=1 \n"
                    + "where UserID=? and LessonID=? and CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, lessonId);
            st.setInt(3, courseId);
            return st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int getNextLessonId(int currentNumberLesson, int courseId) {
        try {
            String sql = "DECLARE @CurrentRowId INT;\n"
                    + "                    SET @CurrentRowId =?;\n"
                    + "				  select  TOP 1 LessonID From Lessons as l inner join Mooc as m\n"
                    + "                  on l.MoocID= m.MoocID inner join Course as c\n"
                    + "                   on c.CourseID=m.CourseID\n"
                    + "                 WHERE c.CourseID=? and LessonNumber > @CurrentRowId  ORDER BY l.LessonNumber ASC;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, currentNumberLesson);
            st.setInt(2, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("LessonID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        ProgressDAO d = new ProgressDAO();
        int a = d.selectFristLessonId(93);
        System.out.println(a
        );
    }
}
