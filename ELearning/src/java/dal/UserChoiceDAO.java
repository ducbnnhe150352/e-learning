/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Choice;
import model.ExamDetail;
import model.UserChoice;

/**
 *
 * @author ADMIN
 */
public class UserChoiceDAO extends DBContext {

    public ArrayList<UserChoice> selectUserChoiceByUserExamID(int examDetailId) {
        ArrayList<UserChoice> listUserChoice = new ArrayList<>();
        UserChoice userChoice = null;
        try {
            String sql = "select * from ExamDetail as ed  inner join UserChoices as uc\n"
                    + "on ed.ExamDetailID=uc.ExamDetailID\n"
                    + "where ed.ExamDetailID=? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, examDetailId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int userChoiceId = rs.getInt("UserChoiceID");
                ExamDetail examDetail = new ExamDetailDAO().selectById(examDetailId);
                int choiceId = rs.getInt("ChoiceID");
                Choice choice = new ChoiceDAO().selectById(choiceId);
                userChoice = new UserChoice(userChoiceId, examDetail, choice);

                listUserChoice.add(userChoice);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listUserChoice;
    }

    public int addUserChoice(int examDetailId, int choiceId) {
        int check = 0;
        try {
            final String sql = "INSERT INTO UserChoices\n"
                    + "( ExamDetailID, ChoiceID)\n"
                    + "VALUES(?,?);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, examDetailId);
            st.setInt(2, choiceId);
            check = st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return check;
    }

    public boolean deleteUserChoiceByChoiceId(int choiceId){
        String sqlQuery = "DELETE FROM UserChoice WHERE [ChoiceID] = ?";
        try{
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, choiceId);
            return statement.executeUpdate() > 0;
        }
        catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public boolean deleteUserChoiceByExamDetailId(int examDetailId){
        String sqlQuery = "DELETE FROM UserChoice WHERE [ExamDetailID] = ?";
        try{
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, examDetailId);
            return statement.executeUpdate() > 0;
        }
        catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    public static void main(String[] args) {
                ArrayList<UserChoice> listUserChoice = new ArrayList<>();

       listUserChoice= new UserChoiceDAO().selectUserChoiceByUserExamID(137);
        for (UserChoice userChoice : listUserChoice) {
            System.out.println(userChoice.toString());
        }
    }
}
