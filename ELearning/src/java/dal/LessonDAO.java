/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Course;
import model.Lesson;
import model.Mooc;
import model.Role;

/**
 *
 * @author ADMIN
 */
public class LessonDAO extends DBContext implements DAO {

    public int getLessonIdByNumberAndMoocID(String number, String moocid) {
        try {
            String sql = "  select * from [Lessons] where [MoocID] = " + moocid + " and [LessonNumber] = " + number;
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                return lessonId;
            }
        } catch (Exception e) {
        }
        return -1;
    }

    public boolean updateLesson(String lessonid, String lessonName, String lessonUrl, String Description) {
        try {
            String sql = "update [Lessons] set [LessonName] = '" + lessonName + "', [LessonUrl] = '" + lessonUrl + "', [Description]= '" + Description + "'\n"
                    + "where [LessonID] = " + lessonid;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public static void main(String[] args) {
//        LessonDAO d = new LessonDAO();
//        System.out.println(d.updateLesson("21", "sang", "sang", "sang"));
//    }
//    public boolean deleteLesson(String id, String moocid) {
//        try {
//            CallableStatement callableStatement = connection.prepareCall("{call usp_deleteLessonByID(?, ?)}");
//
//            // Thiết lập các tham số cho stored procedure
//            callableStatement.setInt(1, Integer.parseInt(id));
//            callableStatement.setInt(2, Integer.parseInt(moocid));
//
//            // Thực thi stored procedure
//            callableStatement.execute();
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
    public boolean AddAnewLesson(String lessonNumber, String lessonName, String moocid, String lessonUrl, String Description) {
        try {
            String sql = " insert into [Lessons] ([LessonNumber], [LessonName], \n"
                    + " [MoocID], [LessonUrl], [Description]) values\n"
                    + " ('" + lessonNumber + "', '" + lessonName + "', '" + moocid + "', '" + lessonUrl + "', '" + Description + "')";
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public ArrayList selectLessonByMoocId(int moocId) {
        final ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
        Lesson lesson = null;
        try {
            String sql = "SELECT * FROM Lessons WHERE MoocID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, moocId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                int lessonNumber = rs.getInt("LessonNumber");
                String lessonName = rs.getString("LessonName");
                String lessonUrl = rs.getString("LessonUrl");
                String description = rs.getString("Description");
                Mooc mooc = new MoocDAO().selectById(moocId);
                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description);
                listLesson.add(lesson);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listLesson;
    }

//    public boolean checkLessonIsLearn(int userId, int lessonId){
//        try {
//            
//            String sql = "SELECT * FROM Lessons WHERE LessonID=?";
//            PreparedStatement st = connection.prepareStatement(sql);
//            st.setInt(1, id);
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                int lessonId = rs.getInt("LessonID");
//                int lessonNumber = rs.getInt("LessonNumber");
//                String lessonName = rs.getString("LessonName");
//                String lessonUrl = rs.getString("LessonUrl");
//                String description = rs.getString("Description");
//                int moocId = rs.getInt("MoocID");
//                Mooc mooc = new MoocDAO().selectById(moocId);
//                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description);
//            }
//        } catch (Exception e) {
//        }
//    }
    public int countLessonByCourseId(int courseId) {

        try {
            String sql = "select  COUNT(*) from Mooc as m inner join Lessons as l\n"
                    + "on m.MoocID=l.MoocID\n"
                    + "where m.CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
         
        }
        return 0;
    }

    @Override
    public ArrayList selectAll() {

        ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
        Lesson lesson = null;
        try {
            String sql = "select  c.CourseName, l.LessonName,m.MoocName,l.LessonUrl,l.Description,l.MoocID, l.LessonNumber,c.CourseID, l.LessonID from Mooc as m inner join Course as c\n"
                    + " on m.CourseID=c.CourseID\n"
                    + " inner join Lessons as l \n"
                    + " on l.MoocID=m.MoocID\n"
                    + " order by c.CourseID";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                int lessonNumber = rs.getInt("LessonNumber");
                String lessonName = rs.getString("LessonName");
                String lessonUrl = rs.getString("LessonUrl");
                String description = rs.getString("Description");
                int moocId = rs.getInt("MoocID");
                Mooc mooc = new MoocDAO().selectById(moocId);
                int courseId = rs.getInt("CourseID");
                Course course = new CourseDAO().selectById(courseId);

                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description, course);
                listLesson.add(lesson);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listLesson;

    }

    @Override
    public Lesson selectById(int id) {
        Lesson lesson = null;
        try {
            String sql = "SELECT * FROM Lessons WHERE LessonID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                int lessonNumber = rs.getInt("LessonNumber");
                String lessonName = rs.getString("LessonName");
                String lessonUrl = rs.getString("LessonUrl");
                String description = rs.getString("Description");
                int moocId = rs.getInt("MoocID");
                Mooc mooc = new MoocDAO().selectById(moocId);
                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lesson;
    }

    public Lesson selectLessonLearning(int userId, int courseId) {
        Lesson lesson = null;
        try {
            String sql = "SELECT *\n"
                    + "	FROM (\n"
                    + "		SELECT l.LessonID, l.LessonName,l.LessonNumber,l.LessonUrl,l.MoocID,l.Description\n"
                    + "		FROM Mooc AS m\n"
                    + "		INNER JOIN Lessons AS l ON m.MoocID = l.MoocID\n"
                    + "		INNER JOIN Course AS c ON m.CourseID = c.CourseID\n"
                    + "		WHERE c.CourseID = 0\n"
                    + "	) AS temp\n"
                    + "	INNER JOIN Progress2 AS p ON temp.LessonNumber=p.NumberLearned+1\n"
                    + "	where  p.UserID=? and p.CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                int lessonNumber = rs.getInt("LessonNumber");
                String lessonName = rs.getString("LessonName");
                String lessonUrl = rs.getString("LessonUrl");
                String description = rs.getString("Description");
                int moocId = rs.getInt("MoocID");
                Mooc mooc = new MoocDAO().selectById(moocId);
                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lesson;
    }

    public Lesson selectFirstLesson(int courseId) {
        Lesson lesson = null;
        try {
            String sql = "select top 1 * FROM  Mooc as m inner join Lessons as l \n"
                    + "                 on m.MoocID= l.MoocID inner join Course as c \n"
                    + "                 on m.CourseID=c.CourseID \n"
                    + "			where c.CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                int lessonNumber = rs.getInt("LessonNumber");
                String lessonName = rs.getString("LessonName");
                String lessonUrl = rs.getString("LessonUrl");
                String description = rs.getString("Description");
                int moocId = rs.getInt("MoocID");
                Mooc mooc = new MoocDAO().selectById(moocId);
                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lesson;
    }

    public ArrayList selectAllByCourseID(int courseId) {

        ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
        Lesson lesson = null;
        try {
            String sql = "select  c.CourseName, l.LessonName,m.MoocName,l.LessonUrl,l.Description,l.MoocID, l.LessonNumber,\n"
                    + "c.CourseID, l.LessonID from Mooc as m inner join Course as c\n"
                    + "                     on m.CourseID=c.CourseID\n"
                    + "                     inner join Lessons as l \n"
                    + "                     on l.MoocID=m.MoocID\n"
                    + "					 where c.CourseID=?\n"
                    + "                    order by l.LessonNumber ASC";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int lessonId = rs.getInt("LessonID");
                int lessonNumber = rs.getInt("LessonNumber");
                String lessonName = rs.getString("LessonName");
                String lessonUrl = rs.getString("LessonUrl");
                String description = rs.getString("Description");
                int moocId = rs.getInt("MoocID");
                Mooc mooc = new MoocDAO().selectById(moocId);
                Course course = new CourseDAO().selectById(courseId);

                lesson = new Lesson(lessonId, lessonNumber, lessonName, mooc, lessonUrl, description, course);
                listLesson.add(lesson);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listLesson;

    }

    public int GetMaxNumberLessonByCourseId(int courseID) {

        try {
            String sql = "select MAX(l.LessonNumber)  from Course as c inner join Mooc as m \n"
                    + "on c.CourseID= m.CourseID inner join Lessons as l \n"
                    + "on l.MoocID = m.MoocID where c.CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(new LessonDAO().GetMaxNumberLessonByCourseId(1121222110));
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public boolean deleteLesson(String id, String moocid) {
        try {
            CallableStatement callableStatement = connection.prepareCall("{call usp_deleteLessonByID(?, ?)}");

            // Thiết lập các tham số cho stored procedure
            callableStatement.setInt(1, Integer.parseInt(id));
            callableStatement.setInt(2, Integer.parseInt(moocid));

            // Thực thi stored procedure
            callableStatement.execute();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
