/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Course;
import model.Problem;
import model.Review;
import model.ReviewReport;
import model.User;

/**
 *
 * @author NhatAnh
 */
public class ReviewReportDAO extends DBContext implements DAO {

    @Override
    public ArrayList selectAll() {
        ArrayList<ReviewReport> listReviewReport = new ArrayList<ReviewReport>();
        try {
            final String sql = "SELECT * FROM ReviewReport";
            PreparedStatement st = connection.prepareStatement(sql);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int reviewReportId = rs.getInt("ReviewReportID");
                int userId = rs.getInt("UserID");
                int reviewId = rs.getInt("ReviewID");
                int problemId = rs.getInt("ProblemID");
                String reason = rs.getString("Reason");
                Date reportDate = rs.getDate("ReportDate");
                Time reportTime = rs.getTime("ReportDate");
                User user = new UserDAO().selectById(userId);
                Review review = new ReviewDAO().selectById(reviewId);
                Problem problem = new ProblemDAO().selectById(problemId);
                ReviewReport reviewReport = new ReviewReport(reviewReportId, user, review, problem, reason, reportDate, reportTime);
                listReviewReport.add(reviewReport);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listReviewReport;
    }

    public int addReviewReport(int userId, int reviewId, int problemId, String reason) {
        try {
            String sql = "INSERT INTO ReviewReport\n"
                    + "( UserID, ReviewID, ProblemID, Reason, ReportDate )\n"
                    + "VALUES(?, ?, ?, ?,GETDATE());";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, reviewId);
            st.setInt(3, problemId);
            st.setString(4, reason);
            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }
    
    public int deleteReport(int reportId) {
        try {
            String sql = "DELETE FROM [ReviewReport] WHERE ReviewReportID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, reportId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }
    
    public static void main(String[] args) {
        ReviewReportDAO dao = new ReviewReportDAO();
        ArrayList<ReviewReport> listReviewReport = new ArrayList<ReviewReport>();
        listReviewReport = dao.selectAll();
        for(ReviewReport rvrp : listReviewReport){
            System.out.println(rvrp.toString());
        }

    }

    @Override
    public Object selectById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
