/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import com.oracle.wls.shaded.org.apache.bcel.generic.AALOAD;
import static dal.DBContext.connection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Course;
import model.Mooc;

/**
 *
 * @author ADMIN
 */
public class MoocDAO extends DBContext implements DAO {
    
    public int getFisrtMoocID(int courseid){
        try {
            String sql = "select MoocID from Mooc where CourseID = "+courseid+" order by MoocNumber asc";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return -1;
    }

    public static ArrayList getMoocByCourseId(int CourseId) {
        ArrayList<Mooc> listMooc = new ArrayList<Mooc>();

        try {
             String sql = "SELECT MoocID, MoocNumber, MoocName FROM Mooc WHERE CourseID=? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, CourseId);
             ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int moocId = rs.getInt("MoocID");
                int moocNumber = rs.getInt("MoocNumber");
                String moocName = rs.getString("MoocName");
                Mooc mooc = new Mooc(moocId, moocNumber, moocName);
                listMooc.add(mooc);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listMooc;
    }
    
    

    @Override
    public ArrayList selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Mooc selectById(int id) {
        
        Mooc mooc=null;
        try {
             String sql = "SELECT * from Mooc WHERE MoocID=? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
             ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int moocId = rs.getInt("MoocID");
                int moocNumber = rs.getInt("MoocNumber");
                String moocName = rs.getString("MoocName");
                Course course = new CourseDAO().selectById(rs.getInt("CourseID"));
                 mooc = new Mooc(moocId, moocNumber, moocName,course);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return mooc;
        
    }
  
    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
public List<Mooc> selectByCourseID(int id) {
    List<Mooc> moocs = new ArrayList<>();
    try {
        String sql = "SELECT * FROM Mooc WHERE CourseID = ?";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, id);
        ResultSet rs = st.executeQuery();
        
        while (rs.next()) {
            int moocId = rs.getInt("MoocID");
            int moocNumber = rs.getInt("MoocNumber");
            String moocName = rs.getString("MoocName");
            Course course = new CourseDAO().selectById(rs.getInt("CourseID"));
            Mooc mooc = new Mooc(moocId, moocNumber, moocName, course);
            moocs.add(mooc);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    return moocs;
}

 


     public boolean addANewMooc(String number, String MoocName, String courseid) {
        try {
            String sql = "insert into Mooc ([MoocNumber], [MoocName], [CourseID]) values\n"
                    + "  (" + number + " , '" + MoocName + "' , " + courseid + ")";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

      public int GetIdMooc(int number, int courseid) {
        try {
            String sql = "  select * from [Mooc] where [MoocNumber] = " + number + " and [CourseID] = " + courseid + ";";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            System.out.println(sql);
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

     public boolean deleteMooc(String moocid, String courseid) {
        try {
            CallableStatement callableStatement = connection.prepareCall("{call usp_deleteMoocByID(?, ?)}");
            
            // Thiết lập các tham số cho stored procedure
            callableStatement.setInt(1, Integer.parseInt(moocid));
            callableStatement.setInt(2, Integer.parseInt(courseid));
            
            // Thực thi stored procedure
            callableStatement.execute();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateMooc(String moocid, String moocname) {
        try {
            String sql = "update [Mooc] set [MoocName] = '" + moocname + "' where [MoocID] = " + moocid;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(MoocDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    public static void main(String[] args) {
        MoocDAO md = new MoocDAO();
        List<Mooc> m = md.selectByCourseID(0)  ;
        for(Mooc mm :m){
            System.out.println(mm.getMoocName());
        }
        
    
    }
}
