/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Certificate;
import model.Course;
import model.Exam;
import model.User;

public class CertificateDao extends DBContext implements DAO {

    public ArrayList<Certificate> getCertificateList(int aid) {
        String sql = "SELECT c.CertificateID , s.CourseName, ca.CategoryName, s.CourseImg "
                + "FROM [dbo].[Certificate] c "
                + "INNER JOIN Course s ON c.CourseID = s.CourseID "
                + "INNER JOIN Category ca ON ca.CategoryID = s.CategoryID "
                + "WHERE c.UserID = ? AND c.Satus = '1'";
        ArrayList<Certificate> certificates = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, aid);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Certificate c = new Certificate();
                c.setCertificateId(rs.getInt("CertificateID"));
                Category ca = new Category();
                ca.setCategoryName(rs.getString("CategoryName"));

                Course co = new Course();
                co.setCourseName(rs.getString("CourseName"));
                co.setCourseImg(rs.getString("CourseImg"));

                c.setCourseId(co);
                c.setCategory(ca);

                certificates.add(c);
            }

            return certificates;
        } catch (SQLException e) {
            e.printStackTrace(); // Xử lý lỗi nên được thực hiện ở đây.
        } finally {
            // Đảm bảo rằng bạn luôn đóng kết nối sau khi sử dụng xong.
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null; // Trả về null nếu có lỗi.
    }

    public Certificate getCertificateListID(String id) {
        String sql = " SELECT  c.Date, u.FullName ,c.CertificateID ,s.CourseName , ca.CategoryName , s.CourseImg\n"
                + "  FROM [dbo].[Certificate]  c INNER JOIN Course s  ON c.CourseID = s.CourseID\n"
                + "  INNER JOIN Category ca on ca.CategoryID = s.CategoryID INNER JOIN [User] u ON u.UserID = c.UserID  where  c.Satus = '1' and c.CertificateID = ? ";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Certificate c = new Certificate();
                c.setCertificateId(rs.getInt("CertificateID"));
                c.setDate(rs.getDate("Date"));
                Category ca = new Category();
                ca.setCategoryName(rs.getString("CategoryName"));

                Course co = new Course();
                co.setCourseName(rs.getString("CourseName"));
                co.setCourseImg(rs.getString("CourseImg"));

                User u = new User();
                u.setFullName(rs.getString("FullName"));
                c.setCourseId(co);
                c.setCategory(ca);
                c.setUserId(u);
                return c;
            }

        } catch (SQLException e) {
            e.printStackTrace(); // Xử lý lỗi nên được thực hiện ở đây.
        } finally {
            // Đảm bảo rằng bạn luôn đóng kết nối sau khi sử dụng xong.
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null; // Trả về null nếu có lỗi.
    }

    public Certificate getCertificateByUserCourseId(int userId, int courseId) {
        Certificate certificate = null;
        String sql = "select * from Certificate where UserID=? and CourseID=?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, courseId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int certificateId = rs.getInt("CertificateID");
                User user = new UserDAO().selectById(userId);
                Course course = new CourseDAO().selectById(rs.getInt("CourseID"));
                Date date = rs.getDate("Date");
                Boolean status = rs.getBoolean("Satus");
                certificate = new Certificate(certificateId, user, course, date, status);
            }

        } catch (SQLException e) {
            e.printStackTrace(); // Xử lý lỗi nên được thực hiện ở đây.
        } finally {
            // Đảm bảo rằng bạn luôn đóng kết nối sau khi sử dụng xong.
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return certificate;
    }

    public int addCertificate(int userId, int courseId) {
        String sql = "INSERT INTO Certificate\n"
                + "(UserID, CourseID, [Date], Satus)\n"
                + "VALUES(?, ?, GETDATE(),1);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            ps.setInt(2, courseId);
            int check = ps.executeUpdate();
            return check;

        } catch (SQLException e) {
            e.printStackTrace(); // Xử lý lỗi nên được thực hiện ở đây.
        } finally {
            // Đảm bảo rằng bạn luôn đóng kết nối sau khi sử dụng xong.
            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return -1; 
    }

    public static void main(String[] args) {
        CertificateDao adao = new CertificateDao();
        Certificate c =  adao.getCertificateByUserCourseId(1,9);
        System.out.println(c);
    }

    @Override
    public ArrayList selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object selectById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
