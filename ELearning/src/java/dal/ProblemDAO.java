/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Problem;
import model.Problem;

/**
 *
 * @author NhatAnh
 */
public class ProblemDAO extends DBContext implements DAO {

    @Override
    public ArrayList selectAll() {
        ArrayList<Problem> listProblem = new ArrayList<Problem>();

        try {
             String sql = "SELECT * FROM Problem";
            PreparedStatement st = connection.prepareStatement(sql);
             ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int problemId = rs.getInt("ProblemID");
                String problem = rs.getString("Problem");
                Problem problem1 = new Problem(problemId, problem);
                listProblem.add(problem1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProblemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listProblem;
    }
    public static void main(String[] args) {
        ArrayList<Problem> listProblem = new ArrayList<Problem>();
        ProblemDAO dao = new ProblemDAO();
        
        listProblem = dao.selectAll();
        for(Problem problem : listProblem){
            System.out.println(problem.toString());
        }
    }
    @Override
    public Problem selectById(int id) {
        Problem problem = null;
        try {
             String sql = "SELECT * FROM Problem WHERE ProblemID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int roleId = rs.getInt("ProblemID");
                String roleName = rs.getString("Problem");
                problem = new Problem(roleId, roleName);

            }
          //  DBContext.closeConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProblemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return problem;
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
