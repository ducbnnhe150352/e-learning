/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Teacher;
import model.User;

/**
 *
 * @author ADMIN
 */
public class TeacherDAO extends DBContext implements DAO {

    @Override
    public ArrayList selectAll() {
        ArrayList<Teacher> teachers = new ArrayList<>();
        try {
            String sql = " select t.* from dbo.Teacher t join dbo.[User] u on t.UserID=u.UserID;";
            PreparedStatement stm = connection.prepareStatement(sql);
            final ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Teacher teacher = new Teacher();
                int teacherid = rs.getInt("TeacherID");
                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                String position = rs.getString("Position");
                String workplace = rs.getString("Workplace");
                Timestamp dateJoin = rs.getTimestamp("dateJoin");
                Timestamp dateRequest = rs.getTimestamp("DateRequest");
                String about = rs.getString("About");
                Boolean status = rs.getBoolean("Status");
                teacher.setTeacherId(teacherid);
                teacher.setUser(user);
                teacher.setPosition(position);
                teacher.setWorkPlace(workplace);
                teacher.setDateJoin(dateJoin);
                teacher.setDateRequest(dateRequest);
                teacher.setAbout(about);
                teacher.setStatus(status);
                teachers.add(teacher);
            }
        } catch (Exception e) {
        }
        return teachers;
    }

    public List<Teacher> selectwithsearch(String keyword) {

        List<Teacher> teachers = new ArrayList<>();
        try {
            String sql = " select t.* from dbo.Teacher t join dbo.[User] u on t.UserID=u.UserID where u.FullName like '%" + keyword + "%';";
            PreparedStatement stm = connection.prepareStatement(sql);
            final ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Teacher teacher = new Teacher();
                int teacherid = rs.getInt("TeacherID");
                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                String position = rs.getString("Position");
                String workplace = rs.getString("Workplace");
                Timestamp dateJoin = rs.getTimestamp("dateJoin");
                String about = rs.getString("About");
                Boolean status = rs.getBoolean("Status");
                teacher.setTeacherId(teacherid);
                teacher.setUser(user);
                teacher.setPosition(position);
                teacher.setWorkPlace(workplace);
                teacher.setDateJoin(dateJoin);
                teacher.setAbout(about);
                teacher.setStatus(status);
                teachers.add(teacher);
            }
        } catch (Exception e) {
        }
        return teachers;
    }

    @Override
    public Teacher selectById(int id) {
        Teacher teacher = null;
        try {
            String sql = "SELECT * FROM Teacher WHERE TeacherID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                String position = rs.getString("Position");
                String workplace = rs.getString("Workplace");
                String personalWebsite = rs.getString("PersonalWebsite");
                String facebook = rs.getString("Facebook");
                String linkedin = rs.getString("Linkedin");
                Timestamp dateJoin = rs.getTimestamp("dateJoin");
                String about = rs.getString("About");
                String cv = rs.getString("CV");
                Boolean status = rs.getBoolean("Status");
                Timestamp dateRequest = rs.getTimestamp("DateRequest");

                teacher = new Teacher(id, user, position, workplace, personalWebsite, facebook, linkedin, dateJoin, about, cv, status, dateRequest);
                teacher.getUser().getAvatar();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return teacher;
    }

    public int changestatusteacher(int teacherid, int status) {
        try {
            String sql = "";
            if (status == 1) {
                sql = "update Teacher set Status = " + status + ", DateJoin = GETDATE() where TeacherID =  " + teacherid + ";";
            } else {
                sql = "update Teacher set Status = " + status + ", DateJoin = null where TeacherID =  " + teacherid + ";";

            }
            PreparedStatement stm = connection.prepareStatement(sql);
            return stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public boolean checkSentRequest(int userId) {
        try {
            String sql = "select * from teacher as t inner join \n"
                    + "[User] as u on t.UserID =u.UserID\n"
                    + "where t.Status IS NULL and u.UserID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }
    public static void main(String[] args) {
        
    }

    public int insertTeacherRequest(int userId, String position, String place, String website, String facebook, String linkedin, String about, String cv, Timestamp date) {
        try {
            String sql = "INSERT INTO Teacher\n"
                    + "(UserID, [Position], Workplace, PersonalWebsite, Facebook, Linkedin, About, CV, DateRequest)\n"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setString(2, position);
            st.setString(3, place);
            st.setString(4, website);
            st.setString(5, facebook);
            st.setString(6, linkedin);
            st.setString(7, about);
            st.setString(8, cv);
            st.setTimestamp(9, date);

            return st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
           
        }
        return -1;

    }

    public int updateBecomeTeacher(int userId) {
        try {
            String sql = "UPDATE [User]\n"
                    + "SET  RoleID=1\n"
                    + "WHERE UserID=?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            int check = st.executeUpdate();
            return check;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

    public Teacher selectByUserId(int id) {
        Teacher teacher = null;
        try {
            String sql = "SELECT * FROM Teacher WHERE UserID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                String position = rs.getString("Position");
                String workplace = rs.getString("Workplace");
                String personalWebsite = rs.getString("PersonalWebsite");
                String facebook = rs.getString("Facebook");
                String linkedin = rs.getString("Linkedin");
                Timestamp dateJoin = rs.getTimestamp("dateJoin");
                String about = rs.getString("About");
                String cv = rs.getString("CV");
                Boolean status = rs.getBoolean("Status");
                Timestamp dateRequest = rs.getTimestamp("DateRequest");

                teacher = new Teacher(id, user, position, workplace, personalWebsite, facebook, linkedin, dateJoin, about, cv, status, dateRequest);
                teacher.getUser().getAvatar();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return teacher;
    }


    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
