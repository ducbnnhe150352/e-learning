/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Course;
import model.Review;
import model.User;

/**
 *
 * @author ADMIN
 */
public class ReviewDAO extends DBContext implements DAO {

    public ArrayList selectReviewByCourseId(int courseId) {
        ArrayList<Review> listReview = new ArrayList<Review>();

        try {
            String sql = "SELECT * FROM Review WHERE CourseID=? ORDER BY Time DESC ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int reviewId = rs.getInt("ReviewID");
                int rating = rs.getInt("Rating");
                Date dateReview = rs.getDate("Time");
                String reviewContent = rs.getString("ReviewContent");
                Boolean isReport = rs.getBoolean("isReport");

                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                Course course = new CourseDAO().selectById(courseId);
                Review review = new Review(reviewId, course, user, rating, dateReview, reviewContent, isReport);
                listReview.add(review);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ReviewDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listReview;
    }

    @Override
    public ArrayList selectAll() {
        ArrayList<Review> listReview = new ArrayList<Review>();

        try {
            String sql = "  select * from Review order by CourseID ASC";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int reviewId = rs.getInt("ReviewID");
                int rating = rs.getInt("Rating");
                Date dateReview = rs.getDate("Time");
                String reviewContent = rs.getString("ReviewContent");
                Boolean isReport = rs.getBoolean("isReport");

                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                int courseId = rs.getInt("CourseID");
                Course course = new CourseDAO().selectById(courseId);
                Review review = new Review(reviewId, course, user, rating, dateReview, reviewContent, isReport);
                listReview.add(review);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ReviewDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listReview;
    }

    @Override
    public Review selectById(int id) {
        Review review = null;
        try {
            String sql = "SELECT * FROM Review WHERE ReviewID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int reviewId = rs.getInt("ReviewID");
                int rating = rs.getInt("Rating");
                Date dateReview = rs.getDate("Time");
                String reviewContent = rs.getString("ReviewContent");
                Boolean isReport = rs.getBoolean("isReport");
                int courseId = rs.getInt("CourseID");
                int userId = rs.getInt("UserID");
                User user = new UserDAO().selectById(userId);
                Course course = new CourseDAO().selectById(courseId);
                review = new Review(reviewId, course, user, rating, dateReview, reviewContent, isReport);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return review;
    }

    public double ratingAverageByUserId(int userId) {

        try {
            final String sql = "select SUM(r.Rating) as sum, Count(*) as count from Review as r inner join Course as c\n"
                    + "on r.CourseID = c.CourseID\n"
                    + "where c.UserID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                float sum = rs.getInt("sum");
                float count = rs.getInt("count");
                if (sum == 0) {
                    return 0;
                }
                double average = sum / count;
                DecimalFormat decimalFormat = new DecimalFormat("#.#");
                double roundedNumber1 = Double.parseDouble(decimalFormat.format(average));
                return roundedNumber1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int addReview(int userId, int courseId, int rating, String reviewContent) {
        try {
            String sql = "INSERT INTO [Review]\n"
                    + "(UserID, CourseID, Rating, Time, ReviewContent, isReport)\n"
                    + "VALUES(?, ?, ?,GETDATE(), ?, 0);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            st.setInt(3, rating);
            st.setString(4, reviewContent);
            int rs = st.executeUpdate();
            return rs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public int deleteReview(int reviewId) {
        try {
            String sql = "DELETE FROM [ReviewReport] WHERE ReviewID = ?; DELETE FROM [Review] WHERE ReviewID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, reviewId);
            st.setInt(2, reviewId);
            int rs = st.executeUpdate();
            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public boolean checkReview(int userId, int courseId) {

        try {
            String sql = "Select * from Review where UserID = ? and CourseID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
            return false;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(EnrollDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ReviewDAO dao = new ReviewDAO();
        System.out.println(dao.checkReview(0, 0));
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
