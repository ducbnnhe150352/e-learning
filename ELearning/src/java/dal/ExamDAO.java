/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Course;
import model.Exam;
import model.ExamDetail;
import model.User;

/**
 *
 * @author ADMIN
 */
public class ExamDAO extends DBContext implements DAO {

    public ArrayList selectExamByCourseId(int courseId) {
        ArrayList<Exam> listExam = new ArrayList<Exam>();
        Exam exam = null;
        try {
            String sql = "SELECT * FROM Exams WHERE CourseID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int examId = rs.getInt("ExamID");
                String examName = rs.getString("ExamName");
                Course course = new CourseDAO().selectById(courseId);
                int duration = rs.getInt("Duration");
                int quantity = rs.getInt("Quantity");
                exam = new Exam(examId, examName, course, duration, quantity);
                listExam.add(exam);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listExam;
    }

    public List<Exam> selectExamByCourseIdAndName(int courseId, String examName, int pageNumber, int pageSize) {
        List<Exam> listExam = new ArrayList<>();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT E.*, C.* FROM (");
            sql.append(" SELECT ROW_NUMBER() OVER (ORDER BY ExamID) AS RowNum, * FROM Exams WHERE CourseID = ?");
            if (examName != null && !examName.isEmpty()) {
                sql.append(" AND ExamName LIKE ?");
            }
            sql.append(") AS E ");
            sql.append("JOIN Course C ON E.CourseID = C.CourseID ");
            sql.append("WHERE E.RowNum BETWEEN ? AND ?");

            PreparedStatement st = connection.prepareStatement(sql.toString());
            st.setInt(1, courseId);

            int paramIndex = 2;
            if (examName != null && !examName.isEmpty()) {
                st.setString(paramIndex++, "%" + examName + "%");
            }

            int startRow = (pageNumber - 1) * pageSize + 1;
            int endRow = pageNumber * pageSize;

            st.setInt(paramIndex++, startRow);
            st.setInt(paramIndex, endRow);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int examId = rs.getInt("ExamID");
                String examNameResult = rs.getString("ExamName");

                // Create course object from the same result set
                Course course = new Course(); // add additional fields if needed
                course.setCourseId(rs.getInt("CourseID"));
                course.setCourseName(rs.getString("CourseName"));

                int duration = rs.getInt("Duration");
                int quantity = rs.getInt("Quantity");
                Exam exam = new Exam(examId, examNameResult, course, duration, quantity);
                listExam.add(exam);
            }
        } catch (SQLException e) {
            Logger.getLogger(ExamDAO.class.getName()).log(Level.SEVERE, "Error fetching exams", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ExamDAO.class.getName()).log(Level.SEVERE, "Error closing connection", ex);
            }
        }
        return listExam;
    }

    public int getTotalPages(int courseId, String examName, int pageSize) {
        int totalExams = 0;
        try {
            String sql = "SELECT COUNT(*)\n" +
                    "FROM Exams \n" +
                    "WHERE CourseID = ?";

            if (examName != null && !examName.isEmpty()) {
                sql += " AND ExamName LIKE ?";
            }

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, courseId);

            if (examName != null && !examName.isEmpty()) {
                statement.setString(2, "%" + examName + "%");
            }

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                totalExams = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return (int) Math.ceil((double) totalExams / pageSize);
    }


    public Exam selectExamByExamId(int examId) {
        Exam exam = null;
        try {
            String sql = "SELECT * FROM Exams WHERE ExamID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, examId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String examName = rs.getString("ExamName");
                int courseId = rs.getInt("CourseID");
                Course course = new CourseDAO().selectById(courseId);
                int duration = rs.getInt("Duration");
                int quantity = rs.getInt("Quantity");
                exam = new Exam(examId, examName, course, duration, quantity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return exam;
    }

    @Override
    public ArrayList selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Exam selectById(int id) {
        Exam exam = null;
        try {
            String sql = "SELECT * FROM Exams WHERE ExamID=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            final ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int examId = rs.getInt("ExamID");
                String examName = rs.getString("ExamName");
                int courseId = rs.getInt("CourseID");
                Course course = new CourseDAO().selectById(courseId);
                int duration = rs.getInt("Duration");
                int quantity = rs.getInt("Quantity");
                exam = new Exam(examId, examName, course, duration, quantity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return exam;
    }

    public boolean deleteExam(int examId) {
        String sqlQuery = "DELETE FROM [Exams] WHERE [ExamID] = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, examId);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public boolean addExam(Exam exam) {
        String sqlQuery = "INSERT INTO [dbo].[Exams]\n"
                + "           ([ExamName]\n"
                + "           ,[CourseID]\n"
                + "           ,[Duration]\n"
                + "           ,[Quantity])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, exam.getExamName());
            statement.setInt(2, exam.getCourseId().getCourseId());
            statement.setInt(3, exam.getDuration());
            statement.setInt(4, exam.getQuantity());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean updateExam(Exam exam) {
        String sqlQuery = "UPDATE [dbo].[Exams]\n"
                + "   SET [ExamName] = ?\n"
                + "      ,[Duration] = ?\n"
                + "      ,[Quantity] = ?\n"
                + " WHERE [ExamID] = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, exam.getExamName());
            statement.setInt(2, exam.getDuration());
            statement.setInt(3, exam.getQuantity());
            statement.setInt(4, exam.getExamId());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            
        }
    }

    public int countNumberExam(int courseId) {
        String sqlQuery = "select count(*) FROM [Exams] WHERE [CourseID] = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setInt(1, courseId);
            ResultSet rs= statement.executeQuery();
            while(rs.next()){
                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
           
        }
        return 0;
    }

    public static void main(String[] args) {
        ExamDAO d = new ExamDAO();
        Exam ex = new Exam();
        ArrayList<Exam> listExam = new ArrayList<Exam>();
        ex = d.selectById(1);
        System.out.println(ex);
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Exam selectLastExam() {
        Exam exam = null;
        try {
            String sql = "SELECT TOP (1) [ExamID]\n"
                    + "      ,[ExamName]\n"
                    + "      ,[CourseID]\n"
                    + "      ,[Duration]\n"
                    + "      ,[Quantity]\n"
                    + "  FROM [dbo].[Exams]\n"
                    + "  ORDER BY [ExamID] DESC";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int examId = rs.getInt("ExamID");
                String examName = rs.getString("ExamName");
                int courseId = rs.getInt("CourseID");
                Course course = new Course();
                course.setCourseId(courseId);
                int duration = rs.getInt("Duration");
                int quantity = rs.getInt("Quantity");
                exam = new Exam(examId, examName, course, duration, quantity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LessonDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return exam;
    }
}
