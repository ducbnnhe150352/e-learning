/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import static dal.DBContext.connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Notification;
import model.Problem;
import model.User;

/**
 *
 * @author ADMIN
 */
public class NotificationDAO extends DBContext implements DAO {

    public ArrayList selectAllByUserId(int userId) {
        ArrayList<Notification> listNofication = new ArrayList<Notification>();
        try {
            String sql = "SELECT * FROM Notification where ToUserID=? ORDER BY NotificationID DESC   ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int notificationId = rs.getInt("NotificationID");
                User fromUserId = new UserDAO().selectById(rs.getInt("FromUserID"));
                User toUserId = new UserDAO().selectById(rs.getInt("ToUserID"));
                String content = rs.getString("Content");
                String link = rs.getString("Link");
                Timestamp date = rs.getTimestamp("Date");
                boolean isRead = rs.getBoolean("IsRead");

                Notification notification = new Notification(notificationId, fromUserId, toUserId, content, link, date, isRead);
                listNofication.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
           
        }
        return listNofication;
    }

    public int countNotificationNotRead(int userId) {
        try {
            String sql = "SELECT Count(*) FROM Notification where ToUserID=? and isRead=0";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProblemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int updateReadNotification(int notificationId) {
        int check = 0;
        try {
            String sql = "UPDATE Notification\n"
                    + "SET  IsRead=1\n"
                    + "WHERE NotificationID=?;";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, notificationId);
            check = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProblemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return check;
    }

    public int insertNotification(int fromUserId, String content, String link, Timestamp date, int toUserId) {
        int check = 0;
        try {
            String sql = "INSERT INTO Notification\n"
                    + "(FromUserID, Content, Link, [Date], IsRead, ToUserID)\n"
                    + "VALUES(?, ?, ?, ?, 0, ?);";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, fromUserId);
            st.setString(2, content);
            st.setString(3, link);
            st.setTimestamp(4, date);
            st.setInt(5, toUserId);
            check = st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProblemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return check;
    }

    public static void main(String[] args) {
        int a = new NotificationDAO().countNotificationNotRead(122);
        System.out.println(a);
    }

    @Override
    public ArrayList selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

    }

    @Override
    public Object selectById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insert(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deleteAll(ArrayList arr) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int update(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int updateById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
