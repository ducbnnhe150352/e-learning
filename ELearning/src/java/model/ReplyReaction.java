/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
public class ReplyReaction {

    private User userId;
    private Reply replyId;
    private boolean isLike;
    private boolean isDisLike;

    public ReplyReaction() {
    }

    public ReplyReaction(User userId, Reply replyId, boolean isLike, boolean isDisLike) {
        this.userId = userId;
        this.replyId = replyId;
        this.isLike = isLike;
        this.isDisLike = isDisLike;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Reply getReplyId() {
        return replyId;
    }

    public void setReplyId(Reply replyId) {
        this.replyId = replyId;
    }

    public boolean isIsLike() {
        return isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public boolean isIsDisLike() {
        return isDisLike;
    }

    public void setIsDisLike(boolean isDisLike) {
        this.isDisLike = isDisLike;
    }

    @Override
    public String toString() {
        return "ReplyReaction{" + "userId=" + userId + ", replyId=" + replyId + ", isLike=" + isLike + ", isDisLike=" + isDisLike + '}';
    }

}
