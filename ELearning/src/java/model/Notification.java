/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author ADMIN
 */
public class Notification {

    private int notificationId;
    private User fromUser;
    private User toUser;
    private String content;
    private String link;
    private Timestamp date;
    private boolean isRead;

    public Notification() {
    }

    public Notification(int notificationId, User fromUser, User toUser, String content, String link, Timestamp date, boolean idRead) {
        this.notificationId = notificationId;
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.content = content;
        this.link = link;
        this.date = date;
        this.isRead = idRead;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean idRead) {
        this.isRead = idRead;
    }

    @Override
    public String toString() {
        return "Notification{" + "notificationId=" + notificationId + ", fromUser=" + fromUser + ", toUser=" + toUser + ", content=" + content + ", link=" + link + ", date=" + date + ", idRead=" + isRead + '}';
    }

}
