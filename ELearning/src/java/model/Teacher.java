/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author ADMIN
 */
public class Teacher extends User {

    private int teacherId;
    private User user;
    private String position;
    private String workPlace;
    private String personalWebsite;
    private String facebook;
    private String linkedin;
    private Timestamp dateJoin;
    private String about;
    private String cv;
    private boolean status;
    private Timestamp dateRequest;

    public Teacher() {
    }

    public Teacher(int teacherId, User user, String position, String workPlace, String personalWebsite, String facebook, String linkedin, Timestamp dateJoin, String about, String cv, boolean status, Timestamp dateRequest) {
        this.teacherId = teacherId;
        this.user = user;
        this.position = position;
        this.workPlace = workPlace;
        this.personalWebsite = personalWebsite;
        this.facebook = facebook;
        this.linkedin = linkedin;
        this.dateJoin = dateJoin;
        this.about = about;
        this.cv = cv;
        this.status = status;
        this.dateRequest = dateRequest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public void setUserId(User user) {
        this.user = user;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public String getPersonalWebsite() {
        return personalWebsite;
    }

    public void setPersonalWebsite(String personalWebsite) {
        this.personalWebsite = personalWebsite;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public Timestamp getDateJoin() {
        return dateJoin;
    }

    public void setDateJoin(Timestamp dateJoin) {
        this.dateJoin = dateJoin;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Timestamp getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Timestamp dateRequest) {
        this.dateRequest = dateRequest;
    }

    @Override
    public String toString() {
        return "Teacher{" + "teacherId=" + teacherId + ", user=" + user + ", position=" + position + ", workPlace=" + workPlace + ", personalWebsite=" + personalWebsite + ", facebook=" + facebook + ", linkedin=" + linkedin + ", dateJoin=" + dateJoin + ", about=" + about + ", cv=" + cv + ", status=" + status + ", dateRequest=" + dateRequest + '}';
    }

}
