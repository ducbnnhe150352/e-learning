/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author ADMIN
 */
public class Certificate {
   private int certificateId;
    private User userId;
    private Course courseId;
    private Date date;
    private boolean status;
    private Category category;
    

    public Certificate() {
    }
    

    public Certificate(int certificateId, User userId, Course courseId, Date date, boolean status) {
        this.certificateId = certificateId;
        this.userId = userId;
        this.courseId = courseId;
        this.date = date;
        this.status = status;
    }

    public int getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(int certificateId) {
        this.certificateId = certificateId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Course getCourseId() {
        return courseId;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Certificate{" + "certificateId=" + certificateId + ", userId=" + userId + ", courseId=" + courseId + ", date=" + date + ", status=" + status + ", category=" + category + '}';
    }
    
}
