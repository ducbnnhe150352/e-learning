/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.sql.Time;
import java.time.*;

/**
 *
 * @author NhatAnh
 */
public class ReviewReport {
    private int reviewReportId;
    private User userId;
    private Review reviewId;
    private Problem problemId;
    private String reason;
    private Date reportDate;
    private Time reportTime;

    public ReviewReport() {
    }

    public ReviewReport(int reviewReportId, User userId, Review reviewId, Problem problemId, String reason, Date reportDate, Time reportTime) {
        this.reviewReportId = reviewReportId;
        this.userId = userId;
        this.reviewId = reviewId;
        this.problemId = problemId;
        this.reason = reason;
        this.reportDate = reportDate;
        this.reportTime = reportTime;
    }

    public int getReviewReportId() {
        return reviewReportId;
    }

    public void setReviewReportId(int reviewReportId) {
        this.reviewReportId = reviewReportId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Review getReviewId() {
        return reviewId;
    }

    public void setReviewId(Review reviewId) {
        this.reviewId = reviewId;
    }

    public Problem getProblemId() {
        return problemId;
    }

    public void setProblemId(Problem problemId) {
        this.problemId = problemId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public Time getReportTime() {
        return reportTime;
    }

    public void setReportTime(Time reportTime) {
        this.reportTime = reportTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ReviewReport{");
        sb.append("reviewReportId=").append(reviewReportId);
        sb.append(", userId=").append(userId);
        sb.append(", reviewId=").append(reviewId);
        sb.append(", problemId=").append(problemId);
        sb.append(", reason=").append(reason);
        sb.append(", reportDate=").append(reportDate);
        sb.append(", reportTime=").append(reportTime);
        sb.append('}');
        return sb.toString();
    }

    

}

    