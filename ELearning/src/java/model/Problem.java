/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author NhatAnh
 */
public class Problem {
     private int problemId;
     private String problem;

    public Problem(int problemId, String problem) {
        this.problemId = problemId;
        this.problem = problem;
    }

    public int getProblemId() {
        return problemId;
    }

    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    @Override
    public String toString() {
        return "Problem{" + "problemId=" + problemId + ", problem=" + problem + '}';
    }
     
     
}
