/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package dal;

import java.util.ArrayList;
import model.Course;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author NhatAnh
 */
public class CourseDAOTest {

    public CourseDAOTest() {
    }

    @Test
    public void testGetNumberMax() {
    }

    @Test
    public void testSelectAll() {
    }

    @Test
    public void testSelectById() {
    }

    @Test
    public void testMain() {
    }

    @Test
    public void testPopularCourses() {
    }

    @Test
    public void testSelectByCategoryID() {
        CourseDAO dao = new CourseDAO();
        ArrayList<Course> listCourse = new ArrayList<Course>();
        listCourse = dao.selectByCategoryID(1); // Lấy danh sách khóa học với CategoryID = 0
        try {
            assertEquals(2, listCourse.size()); // Kiểm tra số lượng khóa học trả về
            assertEquals("Cooking begin", listCourse.get(0).getCourseName()); // Kiểm tra tên khóa học
            assertEquals("Cooking Master", listCourse.get(1).getCourseName()); // Kiểm tra tên khóa học 
//            assertEquals("Head First Java by Kathy Sierra &", listCourse.get(0).getCourseName()); // Kiểm tra tên khóa học 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetCourse() {
    }

    @Test
    public void testSelectByDuration() {
        CourseDAO dao = new CourseDAO();
        ArrayList<Course> listCourse = new ArrayList<Course>();
        int a = 0, b = 4;
        listCourse = dao.selectByDuration(a, b);
        System.out.println(listCourse.size());
    }

    @Test
    public void testSelectTop3Course() {
    }

    @Test
    public void testInsert() {
    }

    @Test
    public void testInsertAll() {
    }

    @Test
    public void testDelete() {
    }

    @Test
    public void testDeleteAll() {
    }

    @Test
    public void testUpdate() {
    }

    @Test
    public void testUpdateById() {
    }

    @Test
    public void testListTop4Course() {
    }

    @Test
    public void testSelectByUserID() {
    }

    @Test
    public void testCountTotalCourses() {
    }

}
