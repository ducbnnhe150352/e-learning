<%-- 
    Document   : chat.jsp
    Created on : Oct 8, 2023, 1:41:01 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <title>JSP Page</title>
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.2/css/bootstrap.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
            />
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
            integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
            />

        <style>
            a.nav-link {
                color: gray;
                font-size: 18px;
                padding: 0;
            }

            .avatar {
                width: 35px;
                height: 35px;
                border-radius: 50%;
                border: 2px solid #e84118;
                padding: 2px;
                flex: none;
            }

            input:focus {
                outline: 0px !important;
                box-shadow: none !important;
            }

            .card-text {
                border: 2px solid #ddd;
                border-radius: 8px;
            }
        </style>
        <script type="text/javascript">
            var socket;

            function connect() {
                socket = new WebSocket("ws:localhost:9999/ELearning/chat");

                socket.onopen = function () {
                    console.log("WebSocket connection opened");
                };

                socket.onclose = function () {
                    console.log("WebSocket connection closed");
                };

                socket.onmessage = function (event) {
                    var message = event.data;
                    console.log("Received message: " + message);

                    var chatBox = document.getElementById("messageContent");
                    // chatBox.innerHTML += message + "<br>";


                    var divElement = document.createElement("div");
                    divElement.className = "card card-text d-inline-block p-2 px-3 m-1";
                    divElement.style.backgroundColor = "#0084ff";
                    divElement.style.color = "white";
                    divElement.textContent += message;
                    var lineBreak = document.createElement("br");

                    chatBox.appendChild(lineBreak);
                    chatBox.appendChild(divElement);

                };
            }

            function sendMessage() {
                //  var recipient = document.getElementById("recipientInput").value;
                var message = document.getElementById("messageInput").value;

                var data = " " + message;
                socket.send(data);

                // Xóa n?i dung tin nh?n sau khi g?i
                document.getElementById("messageInput").value = "";
            }
        </script>
    </head>
    <body onload="connect()">
        <h1>Real-time Chat</h1>

        <div id="chatBox" style="height: 200px; border: 1px solid black; overflow-y: scroll;"></div>

        <!--        <input type="text" id="recipientInput" placeholder="Recipient">-->
        <!--        <input type="text" id="messageInput" placeholder="Message">-->

        <div class="card mx-auto" style="max-width: 400px">
            <div class="card-header bg-transparent">
                <div class="navbar navbar-expand p-0">
                    <ul class="navbar-nav me-auto align-items-center">
                        <li class="nav-item">
                            <a href="#!" class="nav-link">
                                <div
                                    class="position-relative"
                                    style="
                                    width: 40px;
                                    height: 40px;
                                    border-radius: 50%;
                                    border: 2px solid #e84118;
                                    padding: 2px;
                                    "
                                    >
                                    <img
                                        src="https://nextbootstrap.netlify.app/assets/images/profiles/1.jpg"
                                        class="img-fluid rounded-circle"
                                        alt=""
                                        />
                                    <span
                                        class="position-absolute bottom-0 start-100 translate-middle p-1 bg-success border border-light rounded-circle"
                                        >
                                        <span class="visually-hidden">New alerts</span>
                                    </span>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a
                                href="#!"
                                class="nav-link"
                                style="color: black; font-weight: 500"
                                >Admin</a
                            >
                        </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item">
                            <a href="#!" class="nav-link">
                                <i class="fa-solid fa-xmark" style="font-size: 21px"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div
                class="card-body p-4"
                style="height: 500px; overflow: auto; background-color: #eeeeee"
                >
                <!--                //////////////////////// -->
                <div
                    class="d-flex align-items-baseline mb-4"
                    style="margin-bottom: 0rem !important"
                    >
                    <div class="position-relative avatar">
                        <img
                            src="https://nextbootstrap.netlify.app/assets/images/profiles/1.jpg"
                            class="img-fluid rounded-circle"
                            alt=""
                            />
                        <span
                            class="position-absolute bottom-0 start-100 translate-middle p-1 bg-success border border-light rounded-circle"
                            >
                            <span class="visually-hidden">New alerts</span>
                        </span>
                    </div>
                    <div class="pe-2">
                        <div>
                            <div
                                class="card card-text d-inline-block p-2 px-3 m-1"
                                style="background-color: #babbbf; color: black"
                                >
                                Hi helh, are you available to chat?
                            </div>
                        </div>
                        <div>
                            <div class="small">01:10PM</div>
                        </div>
                    </div>
                </div>
                <!--                /////////////////////////////////// -->
                <div
                    class="d-flex align-items-baseline text-end justify-content-end mb-4"
                    >
                    <div class="pe-2">
                        <div id="messageContent">
                            <div
                                class="card card-text d-inline-block p-2 px-3 m-1"
                                style="background-color: #0084ff; color: white"
                                >
                                Let me know when you're available?
                            </div>
                            <div
                                class="card card-text d-inline-block p-2 px-3 m-1"
                                style="background-color: #0084ff; color: white"
                                >
                                Let me know when you're available?
                            </div>
                        </div>

                        <div>
                            <div class="small">01:13PM</div>
                        </div>
                    </div>
                    <!--                    <div class="position-relative avatar">
                                            <img
                                                src="https://nextbootstrap.netlify.app/assets/images/profiles/2.jpg"
                                                class="img-fluid rounded-circle"
                                                alt=""
                                                />
                                            <span
                                                class="position-absolute bottom-0 start-100 translate-middle p-1 bg-success border border-light rounded-circle"
                                                >
                                                <span class="visually-hidden">New alerts</span>
                                            </span>
                                        </div>-->
                </div>

                <!--                ///////////////////////////// -->
            </div>
            <div
                class="card-footer bg-white position-absolute w-100 bottom-0 m-0 p-1"
                >
                <div class="input-group">
                    <input
                        type="text"
                        id="messageInput"
                        style="padding-right: 0px"
                        class="form-control border-0"
                        placeholder="Write a message..."
                        />
                    <div
                        class="input-group-text bg-transparent border-0"
                        style="padding: 5px"
                        >
                        <button onclick="sendMessage()" class="btn btn-light text-secondary" style="padding: 5px">
                            <i
                                class="fa-solid fa-paper-plane"
                                style="color: blue; font-size: 20px"
                                ></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
