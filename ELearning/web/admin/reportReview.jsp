<%-- 
    Document   : reportReview
    Created on : Oct 25, 2023, 10:08:48 AM
    Author     : NhatAnh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="dal.CourseDAO"%>
<%@page import="dal.RoleDAO"%>
<%@page import="dal.AccountDAO"%>
<%@page import="dal.ReviewReportDAO"%>
<%@page import="dal.ReviewDAO"%>
<%@page import="dal.ProblemDAO"%>
<%@page import="model.Problem"%>
<%@page import="model.ReviewReport"%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <style>
            .popup-menu {
                display: none;
                background-color: #f4f4f4;
                border-radius: 8px;
            }
            .popup-menu-hiding ul{
                list-style: none;
            }
            .popup-menu-hiding {

                display: flex;
                flex-direction: column;
                align-items: center;
                width: 120px;
                z-index: 10;
            }
            .popup-menu-item:hover{
                background-color: #dbdbdb;
                cursor: pointer;
                transition: 1s;
                border-radius: 8px
            }
            .popup-menu-item {
                padding: 16px;
                width: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .delete-reserve{
                display: none;
            }
            .delete-hiding{
                display: block;
                position: fixed;
                top: 0;
                background-color: rgba(180, 180, 180, 0.5);
                width: 100%;
                z-index: 100;
            }
            .delete-report-reserve{
                display: none;
            }
            .delete-report-hiding{
                display: block;
                position: fixed;
                top: 0;
                background-color: rgba(180, 180, 180, 0.5);
                width: 100%;
                z-index: 100;
            }
        </style>
        <div class="page d-flex">
            <jsp:include page="sidebar.jsp"></jsp:include>
                <div class="content w-full">
                    <!-- Start Head -->

                    <div>
                    <%@include file= "../component/header.jsp" %>
                </div>

                <!-- End Head -->
                <h1 class="p-relative">Report reviews</h1>


                <!-- Start Projects Table -->
                <div class="projects p-20 bg-white rad-10 m-20">

                    <div class="head bg-white p-15 between-flex">
                        <h2 class="mt-0 mb-20">List Report</h2>

                        <form action="user" method="post">
                            <div class="search p-relative" type="submit">

                                <input class="p-10" type="search" name="txtSearch" placeholder="Type A Keyword" />

                            </div>     
                        </form>

                    </div>
                    <div class="responsive-table">
                        <table class="fs-14 w-full">
                            <thead>
                                <tr>
                                    <td class="w-10">STT</td>
                                    <td>User report</td>
                                    <td>Review</td>
                                    <td>Problem</td>                                    
                                    <td>Reason</td>                                    
                                    <td>Report Date</td>
                                    <td>Action</td>


                                </tr>
                            </thead>
                            <% ReviewReportDAO daoReview = new ReviewReportDAO();
                            ProblemDAO daoProblem = new ProblemDAO();
                            ArrayList<ReviewReport> listReviewReport = new ArrayList<ReviewReport>();
                            listReviewReport = daoReview.selectAll();
                            %> 
                            <tbody>
                                <%for(ReviewReport review : listReviewReport ){%>
                                <tr>
                                    <td><%=review.getReviewReportId()%></td>
                                    <td><%=review.getUserId().getFullName()%></td>
                                    <td><%=review.getReviewId().getReviewContent()%> </td>
                                    <td><%=review.getProblemId().getProblem()%></td>
                                    <td><%=review.getReason()%></td>
                                    <td><%=review.getReportDate()%> | <%=review.getReportTime()%> </td>
                                    <td> <div> <a class="fa-regular fa-edit delete" style="cursor: pointer; color: black;" onmouseover="this.style.color = 'red'" onmouseout="this.style.color = 'black'" onclick="menuReview(<%=review.getReviewReportId()%>)"></a>
                                            <div class="popup-menu" style="position: absolute" id="popup<%=review.getReviewReportId()%>">
                                                <div class="popup-menu-hiding">
                                                    <div class="popup-menu-item" onclick="deleteReview(<%=review.getReviewId().getReviewId()%>)"> Delete review </div>
                                                    <div class="popup-menu-item" onclick="deleteReport(<%=review.getReviewReportId()%>)"> Delete Report</div>
                                                </div>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Projects Table -->

                <!-- Delete review start-->

                <div class="delete-reserve">
                    <div class="delete-hiding" >
                        <div class="d-flex align-items-center justify-content-center" style="min-height: 100vh">
                            <div class="col-12 col-sm-4" style="border-radius: 8px;">
                                <div class="card">
                                    <div class="d-flex align-items-center justify-content-between card-headertext-white"
                                         style="background-color: #06BBCC; padding:16px">
                                        <div style="color: #FFFFFF; font-size: 24px; font-weight: 700">Delete review</div>
                                        <div style="cursor: pointer" onclick="deleteReview()">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0,0,256,256">
                                            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(8.53333,8.53333)"><path d="M7,4c-0.25587,0 -0.51203,0.09747 -0.70703,0.29297l-2,2c-0.391,0.391 -0.391,1.02406 0,1.41406l7.29297,7.29297l-7.29297,7.29297c-0.391,0.391 -0.391,1.02406 0,1.41406l2,2c0.391,0.391 1.02406,0.391 1.41406,0l7.29297,-7.29297l7.29297,7.29297c0.39,0.391 1.02406,0.391 1.41406,0l2,-2c0.391,-0.391 0.391,-1.02406 0,-1.41406l-7.29297,-7.29297l7.29297,-7.29297c0.391,-0.39 0.391,-1.02406 0,-1.41406l-2,-2c-0.391,-0.391 -1.02406,-0.391 -1.41406,0l-7.29297,7.29297l-7.29297,-7.29297c-0.1955,-0.1955 -0.45116,-0.29297 -0.70703,-0.29297z"></path></g></g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="card-body" style="background-color:#f4f4f4; padding: 16px">
                                        <div>Do you want to delete this review?</div>  

                                        <form action="../AdminDeleteReview" method="POST"> 
                                            <input type="hidden" name="reviewId" id="review-hiden-id" value="" />
                                            <input type="hidden" name="courseId" id="course-hiden-id" value="" />
                                            <div class="d-flex align-items-center justify-content-center mt-2">
                                                <div>
                                                    <button type="submit" class="btn btn-primary" style="font-size: 18px">
                                                        Yes
                                                    </button> 
                                                    <button type="button" onclick="deleteReport()" class="btn btn-secondary" style="font-size: 18px">
                                                        No
                                                    </button> 

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Delete review end-->

                <!-- Delete report start-->
                <div class="delete-report-reserve">
                    <div class="delete-report-hiding" >
                        <div class="d-flex align-items-center justify-content-center" style="min-height: 100vh">
                            <div class="col-12 col-sm-4" style="border-radius: 8px;">
                                <div class="card">
                                    <div class="d-flex align-items-center justify-content-between card-headertext-white"
                                         style="background-color: #06BBCC; padding:16px">
                                        <div style="color: #FFFFFF; font-size: 24px; font-weight: 700">Delete report</div>
                                        <div style="cursor: pointer" onclick="deleteReport()">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0,0,256,256">
                                            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(8.53333,8.53333)"><path d="M7,4c-0.25587,0 -0.51203,0.09747 -0.70703,0.29297l-2,2c-0.391,0.391 -0.391,1.02406 0,1.41406l7.29297,7.29297l-7.29297,7.29297c-0.391,0.391 -0.391,1.02406 0,1.41406l2,2c0.391,0.391 1.02406,0.391 1.41406,0l7.29297,-7.29297l7.29297,7.29297c0.39,0.391 1.02406,0.391 1.41406,0l2,-2c0.391,-0.391 0.391,-1.02406 0,-1.41406l-7.29297,-7.29297l7.29297,-7.29297c0.391,-0.39 0.391,-1.02406 0,-1.41406l-2,-2c-0.391,-0.391 -1.02406,-0.391 -1.41406,0l-7.29297,7.29297l-7.29297,-7.29297c-0.1955,-0.1955 -0.45116,-0.29297 -0.70703,-0.29297z"></path></g></g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="card-body" style="background-color:#f4f4f4; padding: 16px">
                                        <div>Do you want to delete this report?</div>  

                                        <form action="../DeleteReport" method="POST"> 
                                            <input type="hidden" name="reviewReportId" id="report-hiden-id" value="" />
                                            <div class="d-flex align-items-center justify-content-center mt-2">
                                                <div>
                                                    <button type="submit" class="btn btn-primary" style="font-size: 18px">
                                                        Yes
                                                    </button> 
                                                    <button type="button" onclick="deleteReport()" class="btn btn-secondary" style="font-size: 18px">
                                                        No
                                                    </button> 

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Delete report end-->


                <!-- Thêm phân trang -->
                <!-- Thêm một lớp CSS cho phân trang -->
                <div class="pagination-sm" style="" >
                    <c:forEach begin="1" end="${endPage}" var="i">
                        <a href="#">${i}</a>

                    </c:forEach>
                </div>
                <input type="hidden" name="reviewId" id="reviewId" value=""></input>
                <script>
                    function menuReview(reviewID) {
                        var modal = document.getElementById("popup" + reviewID);
                        modal.classList.toggle("popup-menu-hiding");
                    }
                    function deleteReview(id) {
                        var modal = document.querySelector(".delete-reserve");
                        modal.classList.toggle("delete-hiding");
                        var reviewHiden = document.getElementById("review-hiden-id");
                        reviewHiden.value = id;

                    }
                    function deleteReport(id) {
                        var modal = document.querySelector(".delete-report-reserve");
                        modal.classList.toggle("delete-report-hiding");
                        var reportHiden = document.getElementById("report-hiden-id");
                        reportHiden.value = id;

                    }
                </script>

                </body>
                </html>
