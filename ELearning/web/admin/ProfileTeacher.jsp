
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="dal.CourseDAO"%>
<%@page import="dal.RoleDAO"%>
<%@page import="dal.AccountDAO"%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Profile</title>
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/framework.css" />
        <link rel="stylesheet" href="css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />
    </head>
    <body>
        <div class="page d-flex">
            <jsp:include page="sidebar.jsp"></jsp:include>
                <div class="content w-full">
                    <!-- Start Head -->

                    <div>
                    <%@include file= "../component/header.jsp" %>
                </div>

                <!-- End Head -->
                <h1 class="p-relative">Profile of Teacher ${teacher.getUser().getFullName()}</h1>
                <div class="profile-page m-20">
                    <!-- Start Overview -->
                    <div class="overview bg-white rad-10 d-flex align-center">
                        <div class="avatar-box txt-c p-20">
                            <img class="rad-half mb-10" src="<%=url%>/imageStorage/user/${teacher.getUser().getAvatar()}" alt="" />
                            <h3 class="m-0">${teacher.getUser().getFullName()}</h3>
                            <p class="c-grey mt-10">${teacher.getRoleid().getRoleName()}</p>
                            <div class="level rad-6 bg-eee p-relative">
                                <span style="width: 70%"></span>
                            </div>
                            <div class="rating mt-10 mb-10">
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                            </div>
                            <p class="c-grey m-0 fs-13">
                                <script type="text/javascript">
                                    function checkBanStatus(status) {
                                        if (status == true) {
                                            return '<span class="label btn-shape bg-green c-white">Active</span>';
                                        } else {
                                            return '<span class="label btn-shape bg-red c-white">Inactive</span>';
                                        }
                                    }
                                    document.write(checkBanStatus(${teacher.isStatus()}));
                                </script>
                            </p>
                        </div>
                        <div class="info-box w-full txt-c-mobile">
                            <!-- Start Information Row -->
                            <div class="box p-20 d-flex align-center">
                                <h2 style="color: black" class="c-grey fs-15 m-0 w-full">General Information</h2>
                                <div class="fs-14">
                                    <span style="color: black" class="c-grey">Full Name:</span>
                                    <span>${teacher.getUser().getFullName()}</span>
                                </div>
                                <div class="fs-14">
                                    <span style="color: black" class="c-grey">Gender:</span>
                                    <span>
                                        ${teacher.getUser().getGender() ? 'Male' : 'Female'}
                                    </span>

                                </div>
                                <div class="fs-14">
                                    <span  style="color: black" class="c-grey">Country:</span>
                                    <span><span>${teacher.getUser().getAddress()}</span></span>
                                </div>                                
                            </div>
                            <!-- End Information Row -->
                            <!-- Start Information Row -->
                            <div class="box p-20  align-center">
                                <h2 style="color: black" class="c-grey w-full fs-15 m-0">Personal Information</h2>
                                <div class="fs-14">
                                    <strong style="color: black; display: block" class="c-grey">Email:</strong>
                                    <span>${teacher.getUser().getEmail()}</span>
                                </div>
                                <div class="fs-14">
                                    <strong style="color: black; display: block" class="c-grey">Phone: </strong>
                                    <span>${teacher.getUser().getPhone()}</span>
                                </div>
                                <div class="fs-14">
                                     <strong style="color: black; display: block" class="c-grey">Date Of Birth:</strong>
                                    <span>${teacher.getUser().getDateOfBirth()}</span>
                                </div>
                                <div class="fs-15" style="margin-right: 40px; display: block">
                                    <strong style="color: black" class="c-grey">Personal Website: </strong>
                                    <span><a target="_blank" href="${teacher.getPersonalWebsite()}">${teacher.getPersonalWebsite()}</a></span>
                                </div>
                                <br>
                                <div class="fs-15" style="display: block;">
                                    <strong style="color: black" class="c-grey">Facebook: </strong>
                                    <span><a target="_blank" href="${teacher.getFacebook()}">${teacher.getFacebook()}</a></span>
                                </div>
                                <br>
                                <div style="" class="fs-15" style="display: block;">
                                    <strong style="color: black" class="c-grey">Linked in: </strong>
                                    <span><a target="_blank" href="${teacher.getLinkedin()}">${teacher.getLinkedin()}</a></span>
                                </div>
                            </div>
                            <!-- End Information Row -->
                            <!-- Start Information Row -->
                            <div class="box p-20 d-flex align-center">
                                <h4 class="c-grey w-full fs-15 m-0">Job Information</h4>
                                <div class="fs-14">
                                    <span class="c-grey">Work Place: </span>
                                    <span>${teacher.getWorkPlace()}</span>
                                </div>
                                <div class="fs-14">
                                    <span class="c-grey">Position: </span>
                                    <span>${teacher.getPosition()}</span>
                                </div>
                                <div class="fs-14">
                                    <span class="c-grey">CV:</span>
                                    <span><a target="_blank" href="${teacher.getCv()}">${teacher.getCv()}</a></span>
                                </div>

                            </div>
                            <!-- End Information Row -->
                            <!-- Start Information Row -->


                            <form action="ban" method="post">
                                <div class="box p-20 d-flex align-center">
                                    <h4 class="c-grey w-full fs-15 m-0">Restrict user: ${teacher.getUser().getUserId()}</h4>
                                    <input value="${teacher.getUser().getUserId()}" name="ID" type="hidden">
                                    <div class="fs-14">
                                        <span class="c-grey" >Reason : </span>
                                        <input type="text" name="reasonBan" value="${teacher.getUser().getReason()}">

                                    </div>
                                    <div class="fs-14">
                                        <span class="c-grey" >Time  : </span>
                                        <input type="date" name="timeBan" value="${teacher.getUser().getTimeBan()}">

                                    </div>

                                  

                                    <script>
                                        function confirmBan() {
                                            if (confirm("Are you sure with this change?")) {
                                                // Thêm tên form vào hàm submit nếu bạn muốn gửi form đầu tiên trên trang
                                                document.forms[0].submit();
                                                // Hoặc thêm ID cho form của bạn và sử dụng nó để gửi form
                                                // document.getElementById("yourFormId").submit();
                                                // Thêm thông báo thành công hoặc thay đổi tùy ý
                                            } else {
                                                alert("Changes not saved.");
                                            }
                                        }
                                    </script>


                                </div>
                            </form> 

                            <form action="ChangeStatusTeacher" method="post">
                                <div class="box p-20 d-flex align-center">
                                    <input value="${teacher.getTeacherId()}" name="teacherid" hidden="">
                                    <input value="${teacher.isStatus()}" name="status" hidden="">
                                    <c:if test="${teacher.isStatus()==true}">
                                        <div class="fs-14">
                                            <button class="label btn-shape bg-red c-white" type="button" onclick="confirmBan()">Reject</button>
                                        </div>
                                    </c:if>
                                    <c:if test="${teacher.isStatus()==false}">
                                        <div class="fs-14">
                                            <button class="label btn-shape bg-green c-white" type="button" onclick="confirmBan()">Approve</button>
                                        </div> 
                                    </c:if>
                                    <script>
                                        function confirmBan() {
                                            if (confirm("Are you sure with this change?")) {
                                                // Thêm tên form vào hàm submit nếu bạn muốn gửi form đầu tiên trên trang
                                                document.forms[1].submit();
                                                // Hoặc thêm ID cho form của bạn và sử dụng nó để gửi form
                                                // document.getElementById("yourFormId").submit();
                                                // Thêm thông báo thành công hoặc thay đổi tùy ý
                                            } else {
                                                alert("Changes not saved.");
                                            }
                                        }
                                    </script>
                                </div>
                            </form> 
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </body>
</html>
