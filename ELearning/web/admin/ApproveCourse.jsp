
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="dal.CourseDAO"%>
<%@page import="dal.RoleDAO"%>
<%@page import="dal.AccountDAO"%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Course Details</title>
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/framework.css" />
        <link rel="stylesheet" href="css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />
    </head>
    <body>
        <div class="page d-flex">
            <jsp:include page="sidebar.jsp"></jsp:include>
                <div class="content w-full">
                    <!-- Start Head -->

                    <div>
                    <%@include file= "../component/header.jsp" %>
                </div>

                <!-- End Head -->
                <h1 class="p-relative">Details of Course ${course.getCourseName()}</h1>
                <div class="profile-page m-20">
                    <!-- Start Overview -->
                    <div class="overview bg-white rad-10 d-flex align-center">
                        <div class="avatar-box txt-c p-20">
                            <img style="width: 100px; /* Đặt chiều rộng của hình ảnh */
                                 height: 100px; /* Đặt chiều cao của hình ảnh */
                                 object-fit: contain; /* Để tạo hình vuông */" src="<%=url%>/imageStorage/course/${course.getCourseImg()}" alt="" />
                            <h3 class="m-0">${course.getCourseName()}</h3>
                            <p class="c-grey mt-10">${course.getCategoryId().getCategoryName()}<p>
                            <div class="level rad-6 bg-eee p-relative">
                                <span style="width: 70%"></span>
                            </div>
                            <div class="rating mt-10 mb-10">
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                                <i class="fa-solid fa-star c-orange fs-13"></i>
                            </div>
                            <p class="c-grey m-0 fs-13">
                                <script type="text/javascript">
                                    function checkBanStatus(status) {
                                        if (status == true) {
                                            return '<span class="label btn-shape bg-green c-white">Active</span>';
                                        } else {
                                            return '<span class="label btn-shape bg-red c-white">Inactive</span>';
                                        }
                                    }
                                    document.write(checkBanStatus(${course.isIdContinued()}));
                                </script>
                            </p>
                        </div>
                        <div class="info-box w-full txt-c-mobile">
                            <div class="box p-20 d-flex align-center">
                                <h4 class="c-grey fs-15 m-0 w-full">General Information</h4>
                                <div class="fs-14" style="margin-right: 10px">
                                    <span class="c-grey">Course Name</span>
                                    <span>${course.getCourseName()}</span>
                                </div>
                                <div class="fs-14">
                                    <span class="c-grey">Category: </span>
                                    <span>
                                        ${course.getCategoryId().getCategoryName()}
                                    </span>

                                </div>
                                <div class="fs-14">
                                    <span class="c-grey">Public: </span>
                                    <span><span>${course.getPublish()}</span></span>
                                </div>  
                                <div class="fs-14">
                                    <span class="c-grey">Duration: </span>
                                    <span>${course.getDuration()}</span>
                                </div>
                            </div>
                            <!-- End Information Row -->
                            <!-- Start Information Row -->
                            <div class="box p-20 d-flex align-center">
                                <h4 class="c-grey w-full fs-15 m-0">Details Information</h4>
                                <div class="fs-14">
                                    <span class="c-grey">Report: </span>
                                    <span>${course.getReport()}</span>
                                </div>
                                <div class="fs-14">
                                    <span class="c-grey">New Version Id: </span>
                                    <span>${course.getNewVersion()}</span>
                                </div>
                                <div class="fs-14">
                                    <span class="c-grey">Description: </span>
                                    <span>${course.getDescription()}</span>
                                </div>
                            </div> 

                            <form action="ChangeStatusCourse" method="post">
                                <div class="box p-20 d-flex align-center">
                                    <input value="${course.getCourseId()}" name="courseid" hidden="">
                                    <input value="${course.isIdContinued()}" name="status" hidden="">
                                    <c:if test="${course.isIdContinued()==true}">
                                        <div class="fs-14">
                                            <button class="label btn-shape bg-red c-white" type="button" onclick="confirmBan()">Reject</button>
                                        </div>
                                    </c:if>
                                    <c:if test="${course.isIdContinued()==false}">
                                        <div class="fs-14">
                                            <button class="label btn-shape bg-green c-white" type="button" onclick="confirmBan()">Approve</button>
                                        </div> 
                                    </c:if>
                                    <script>
                                        function confirmBan() {
                                            if (confirm("Are you sure with this change?")) {
                                                // Thêm tên form vào hàm submit nếu bạn muốn gửi form đầu tiên trên trang
                                                document.forms[0].submit();
                                                // Hoặc thêm ID cho form của bạn và sử dụng nó để gửi form
                                                // document.getElementById("yourFormId").submit();
                                                // Thêm thông báo thành công hoặc thay đổi tùy ý
                                            } else {
                                                alert("Changes not saved.");
                                            }
                                        }
                                    </script>
                                </div>
                            </form> 
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </body>
</html>