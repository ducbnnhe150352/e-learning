<%-- 
    Document   : learning
    Created on : Sep 23, 2023, 10:55:13 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dal.CourseDAO"%>
<%@page import="dal.ProgressDAO"%>
<%@page import="dal.ExamDetailDAO"%>
<%@page import="dal.ExamDAO"%>
<%@page import="dal.MoocDAO"%>
<%@page import="dal.LessonDAO"%>
<%@page import="dal.ExamDetailDAO"%>

<%@page import="model.Lesson"%>
<%@page import="model.Exam"%>
<%@page import="model.Mooc"%>
<%@page import="model.Course"%>
<%@page import="model.ExamDetail"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="java.io.*, java.util.*" %>

<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<%
    int courseID = (Integer)request.getAttribute("courseid");
   
%>
<%
                Course courseStart= new CourseDAO().selectById(0);
                
                ArrayList<Mooc> listMooc = new ArrayList<Mooc>();
                MoocDAO moocDao= new MoocDAO();
                listMooc = moocDao.getMoocByCourseId(courseID);
%>
<!DOCTYPE html>
<html>
    <head>  
        <title>Learning</title>
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
            />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


        <style>
            .video-container {
                display: flex;
                justify-content: center;
                align-items: center;
            }
            body {
                overflow: hidden;
            }
            .scrollable-div {
                overflow: auto;
                height: 95vh;
            }
            /* Tùy chỉnh thanh cuộn */
            .scrollable-div::-webkit-scrollbar {
                width: 7px; /* Đặt chiều rộng của thanh cuộn */
            }

            .scrollable-div::-webkit-scrollbar-thumb {
                background-color: #d9d9d9; /* Đặt màu sắc của thanh cuộn */
                border-radius: 4px; /* Đặt viền cong của thanh cuộn */
            }

            .scrollable-div::-webkit-scrollbar-thumb:hover {
                background-color: #ffffff; /* Đặt màu sắc khi hover lên thanh cuộn */
            }
            .accordion {
                padding-left: 0;
            }
            .accordion li {
                list-style: none;
                width: 100%;
                background-color: #edeff1;
                box-shadow: 6px 6px 10px -1px rgba(0, 0, 0, 0.15);
            }
            .accordion li label {
                display: flex;
                align-items: center;
                padding: 10px;
                padding-bottom: 5px;
                font-size: 17px;
                cursor: pointer;
            }
            label::before {
                content: "+";
                margin-right: 10px;
                align-items: center;
                font-size: 18px;
                margin-bottom: 4px;
                font-weight: bold;
            }

            input[type="checkbox"] {
                display: none;
            }
            .accordion .content {
                background-color: white;
                line-height: 26px;
                max-height: 0;
                overflow: hidden;
                padding-bottom: 0px;
                transition: max-height 0.5s, padding 0.5s;
            }
            .accordion .content a {
                text-decoration: none;
                margin-left: 2rem;
                color: black;
                cursor: pointer;
            }

            .content a:focus p {
                font-weight: bold;
                color: #2bc5d4
            }
            .accordion .content :hover {
                color: #2bc5d4;
            }
            .accordion input[type="checkbox"]:checked + label + .content {
                max-height: 400px;
                padding: 0px 10px 0px 40px;
            }

            .accordion input[type="checkbox"]:checked + label::before {
                content: "-";
                margin-right: 10px;
                font-size: 18px;
            }
            hr {
                border: 0.8;
                margin-bottom: 0px;
            }
            label {
                margin-bottom: 0px;
                padding-bottom: 0px;
            }
            .vertical {
                border-left: 1px solid #d8d8d8;
                height: 75px;
                font-weight: 300px;
                margin: auto;
                margin-right: 30px;
            }
            /*  //////////////////////////////  //////////////////////////////////////////////////////////// //////////////////////////////////////*/

            .img-sm {
                width: 46px;
                height: 46px;
            }

            .panel {
                box-shadow: 0 2px 0 rgba(0, 0, 0, 0.075);
                border-radius: 0;
                border: 0;
                margin-bottom: 15px;
            }

            .panel .panel-footer,
            .panel > :last-child {
                border-bottom-left-radius: 0;
                border-bottom-right-radius: 0;
            }

            .panel .panel-heading,
            .panel > :first-child {
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }

            .panel-body {
                padding: 25px 20px;
            }

            .media-block .media-left {
                display: block;
                float: left;
            }

            .media-block .media-right {
                float: right;
            }

            .media-block .media-body {
                display: block;
                overflow: hidden;
                width: auto;
            }

            .middle .media-left,
            .middle .media-right,
            .middle .media-body {
                vertical-align: middle;
            }

            .thumbnail {
                border-radius: 0;
                border-color: #e9e9e9;
            }

            .tag.tag-sm,
            .btn-group-sm > .tag {
                padding: 5px 10px;
            }

            .tag:not(.label) {
                background-color: #fff;
                padding: 6px 12px;
                border-radius: 2px;
                border: 1px solid #cdd6e1;
                font-size: 12px;
                line-height: 1.42857;
                vertical-align: middle;
                -webkit-transition: all 0.15s;
                transition: all 0.15s;
            }
            .text-muted,
            a.text-muted:hover,
            a.text-muted:focus {
                color: #acacac;
            }
            .text-sm {
                font-size: 0.9em;
            }
            .text-5x,
            .text-4x,
            .text-5x,
            .text-2x,
            .text-lg,
            .text-sm,
            .text-xs {
                line-height: 1.25;
            }

            .btn-trans {
                background-color: transparent;
                border-color: transparent;
                color: #929292;
            }

            .btn-icon {
                padding-left: 9px;
                padding-right: 9px;
            }

            .btn-sm,
            .btn-group-sm > .btn,
            .btn-icon.btn-sm {
                padding: 5px 10px !important;
            }

            .mar-top {
                margin-top: 15px;
            }
            .popup-menu {
                display: none;
                position: absolute;
                background-color: #fff;
                border: 1px solid #ccc;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
            }

            /* Định dạng dấu ba chấm */
            .ellipsis {
                cursor: pointer;
            }
            .option li:hover {
                background-color: #4267b2;
                color: white;
                cursor: pointer;
            }
            ul {
                list-style: none;
                padding-inline-start: 00px;
                margin: 0px;
            }
            .activeLesson p{
                color:#2a6ced;
                font-size: 18px;
            }
            .button {
                display: inline-block;
                padding: 10px 20px;
                background-color: #3498db;
                color: #fff;
                text-decoration: none;
                border: 1px solid #2980b9;
                border-radius: 5px;
                cursor: pointer;
                text-align: center;
            }

            .button:hover {
                background-color: #2980b9;
            }
        </style>
    </head>
    <body>
        <%@include file= "../component/header.jsp" %>

        <div class="row">
            <div class="col-md-3" style="background-color: #f0f1f2; padding: 0; margin-left: 30px">
                <p
                    style="
                    font-weight: 500;
                    color: black;
                    padding: 10px 10px 10px 20px;
                    background-color: #ffffff;
                    margin: 0;
                    "
                    >
                    Content Of Course
                </p>
                <div class="scrollable-div">
                    <%for (Mooc mooc : listMooc) {%>
                    <ul class="accordion" style="margin: 0">
                        <li>                           
                            <input type="checkbox" name="accordion" id="<%=mooc.getMoocId()%>" />
                            <label
                                for="<%=mooc.getMoocId()%>"
                                style="
                                font-weight: 600;
                                border-bottom: solid 1px rgba(0, 0, 0, 0.15);
                                "
                                >
                                <div class="row">
                                    <div class="col-sm-12">
                                        Mooc <%=mooc.getMoocNumber()%> - <%=mooc.getMoocName()%>
                                    </div>
                                    <div class="col-sm-12">
                                        <a href="MoocDetails?moocid=<%=mooc.getMoocId()%>&courseid=${courseid}">Details</a>
                                    </div>
                                </div>
                            </label>

                            <%
                            ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
                            LessonDAO lessonDao= new LessonDAO();
                            int moocid=mooc.getMoocId();
                            listLesson=lessonDao.selectLessonByMoocId(moocid);
                            %>
                            <div class="content">
                                <%for (Lesson lesson : listLesson) {%>
                                <a data-value="<%=lesson.getLessonId()%>" class="lesson" style="margin: 0px">
                                    <div style="position: relative; display: flex">
                                        <div>


                                            <a style="margin: auto 0px;
                                               font-weight: 500 "

                                               href="UpdateLesson?courseid=${courseid}&moocid=<%=mooc.getMoocId()%>&lessonid=<%=lesson.getLessonId()%>"
                                               >
                                                <%=lesson.getLessonNumber()%>. <%=lesson.getLessonName()%>
                                            </a>

                                        </div>
                                    </div>
                                </a>
                                <hr style="margin:0">

                                <%}%>
                                <p style="text-align: left" ><a href="CreateLesson?courseid=${courseid}&moocid=<%=mooc.getMoocId()%>">+ Add new Lesson</a></p>
                            </div>
                        </li>
                    </ul>
                    <%}%>


                    <ul class="accordion" style="margin: 0">
                        <li>                           
                            <input type="checkbox" name="accordion" id="test" />
                            <label
                                for="test"
                                style="
                                font-weight: 600;
                                border-bottom: solid 1px rgba(0, 0, 0, 0.15);
                                "
                                ><a href="CreateMooc?courseid=${courseid}">Add new Mooc</a>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>

            <div
                id="video"
                class="col-md-8"
                style=" padding-right: 0px"
                >
                <div class="scrollable-div ">

                    <h1 style="margin-left: 110px">Create a New Mooc</h1>
                    <div
                        style="background-color: white; padding: 3% 11%"
                        class="align-content-around"
                        >

                        <form action="CreateMooc" method="post">
                            <input type="text" value="${courseid}" name="courseid" hidden="">
                            <div style="margin-top: 30px">
                                <strong >Mooc Number</strong><br/>
                                <input style="width: 500px; background-color: #dddddd" type="text" name="moocnumber" value="${lastnumber}" readonly="" name="lessonNumber">
                            </div>
                            <div style="margin-top: 30px">
                                <strong>Mooc Name</strong><br/>
                                <input style="width: 500px;" type="text" name="moocname">
                            </div>

                            <div class="text-center mt-3 mb-3" style="margin-left: -598px">
                                <button
                                    class="button"
                                    >Create New Mooc&nbsp;
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div id="exam" class="col-md-9" style="background: white;max-height: 622px; padding:45px 190px;display:none">
            </div>

        </div>
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
            integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
            />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                // Get all checkboxes
                var checkboxes = document.querySelectorAll('input[name="accordion"]');

                // Set the initial state based on local storage
                checkboxes.forEach(function (checkbox) {
                    var checkboxId = checkbox.id;
                    var checkboxState = localStorage.getItem(checkboxId);

                    if (checkboxState === 'open') {
                        checkbox.checked = true;
                    } else {
                        checkbox.checked = false;
                    }
                });

                // Add event listeners to update local storage on checkbox change
                checkboxes.forEach(function (checkbox) {
                    checkbox.addEventListener('change', function () {
                        var checkboxId = checkbox.id;
                        var checkboxState = checkbox.checked ? 'open' : 'closed';

                        localStorage.setItem(checkboxId, checkboxState);
                    });
                });
            });
        </script>
        <script>

            function ellipsisEvent() {
                // Lấy danh sách tất cả các dấu ellipsis
                var ellipsisList = document.querySelectorAll(".ellipsis");
                // Lặp qua từng dấu ellipsis và thêm sự kiện click
                ellipsisList.forEach(function (ellipsis) {
                    ellipsis.addEventListener("click", function (event) {
                        // Đóng tất cả các popup menu trước khi mở popup menu hiện tại
                        closeAllPopupMenus();
                        // Tìm popup menu liên quan
                        var popupMenu = this.nextElementSibling;
                        // Tính toán vị trí cho popup menu
                        var ellipsisRect = this.getBoundingClientRect();
                        popupMenu.style.position = "fixed"; // Sử dụng position: fixed để định vị dựa trên cửa sổ trình duyệt
                        popupMenu.style.top = ellipsisRect.bottom + "px";
                        popupMenu.style.left = ellipsisRect.left + "px";
                        // Mở popup menu hiện tại
                        popupMenu.style.display = "block";
                        // Ngăn sự kiện click toàn cục lan ra các menu ellipsis
                        event.stopPropagation();
                    });
                });
                // Thêm sự kiện click toàn cục để đóng popup menu khi người dùng bấm bất kỳ nơi nào trên trang
                document.addEventListener("click", function () {
                    closeAllPopupMenus();
                });
                // Hàm đóng tất cả các popup menu
                function closeAllPopupMenus() {
                    var popupMenus = document.querySelectorAll(".popup-menu");
                    popupMenus.forEach(function (popupMenu) {
                        popupMenu.style.display = "none";
                    });
                }
                deleteComment();
                like();
                viewReply();
                replyWrite();
                hiddenDisplayreplyWrite();
            }
            function ellipsisEvent2() {
                // Lấy danh sách tất cả các dấu ellipsis
                var ellipsisList = document.querySelectorAll(".ellipsis");
                // Lặp qua từng dấu ellipsis và thêm sự kiện click
                ellipsisList.forEach(function (ellipsis) {
                    ellipsis.addEventListener("click", function (event) {
                        // Đóng tất cả các popup menu trước khi mở popup menu hiện tại
                        closeAllPopupMenus();
                        // Tìm popup menu liên quan
                        var popupMenu = this.nextElementSibling;
                        // Tính toán vị trí cho popup menu
                        var ellipsisRect = this.getBoundingClientRect();
                        popupMenu.style.position = "fixed"; // Sử dụng position: fixed để định vị dựa trên cửa sổ trình duyệt
                        popupMenu.style.top = ellipsisRect.bottom + "px";
                        popupMenu.style.left = ellipsisRect.left + "px";
                        // Mở popup menu hiện tại
                        popupMenu.style.display = "block";
                        // Ngăn sự kiện click toàn cục lan ra các menu ellipsis
                        event.stopPropagation();
                    });
                });
                // Thêm sự kiện click toàn cục để đóng popup menu khi người dùng bấm bất kỳ nơi nào trên trang
                document.addEventListener("click", function () {
                    closeAllPopupMenus();
                });
                // Hàm đóng tất cả các popup menu
                function closeAllPopupMenus() {
                    var popupMenus = document.querySelectorAll(".popup-menu");
                    popupMenus.forEach(function (popupMenu) {
                        popupMenu.style.display = "none";
                    });
                }
                replyDisplayHidden();

            }
        </script>

        <script src="https://www.youtube.com/iframe_api"></script>
    </body>
</html>