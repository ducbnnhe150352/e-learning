<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Exam" %>
<%@ page import="util.Helpers" %>
<%@ page import="java.util.List" %>
<%@ page import="model.Quiz" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="../admin/css/all.min.css"/>
    <link rel="stylesheet" href="../admin/css/framework.css"/>
    <link rel="stylesheet" href="../admin/css/master.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet"/>
    <title>Edit exam</title>
</head>
<body>
<style>
    .quiz-list:empty{
        display: none!important;
    }
    textarea[readonly]{
        border: none;
        outline: none;
    }
</style>
        <%@include file= "../userProfile/sub_header.jsp"%>
<div class="container">
    <div class="d-flex flex-column justify-content-center align-items-center">
        <div class="d-flex border border-primary rounded my-4 flex-column p-4">
            <form id="examForm">
                <div class="mb-3">
                    <label for="examName" class="form-label">Exam name</label>
                    <input type="text" class="form-control" id="examName" name="examName" placeholder="input Exam name"
                           value="${exam.examName}" size="50">
                    <p class="text-danger d-none" id="validateExamName"></p>
                </div>
                <div class="mb-3">
                    <label for="examDuration" class="form-label">Duration(in minutes)</label>
                    <input type="number" class="form-control w-50" id="examDuration" name="examDuration"
                           value="${exam.duration}" placeholder="input duration">
                    <p class="text-danger d-none" id="validateExamDuration"></p>
                </div>
                <div class="mb-3">
                    <label for="examQuantity" class="form-label">Quantity</label>
                    <input type="number" class="form-control w-50" id="examQuantity" name="examQuantity"
                           value="${exam.quantity}" placeholder="input quantity">
                    <input type="hidden" id="existQuantity"
                           value="${exam.quantity}">
                    <p class="text-danger d-none" id="validateExamQuantity"></p>
                </div>
                <input type="hidden" id="examId" value="${exam.examId}" name="examId">
                <%
                    int courseId = 0;
                    if (request.getAttribute("exam") == null) {
                        courseId = Helpers.parseInt(request.getParameter("courseId").toString(), 1);
                    } else {
                        Exam exam = (Exam) request.getAttribute("exam");
                        courseId = exam.getCourseId().getCourseId();
                    }
                %>
                <input type="hidden" id="courseId" value="<%=courseId%>" name="examCourseId">
                <p id="messageReturn"></p>
                <div class="mb-3 d-flex">
                    <button type="button" class="btn btn-danger" id="cancelBtn" onclick="ReloadPage()">Cancel</button>
                    <input type="submit" class="btn btn-outline-primary mx-2" value="Save"/>
                </div>
            </form>
        </div>
        <%
            if(!request.getParameter("examId").equals("-1")){
        %>
        <div class="quiz-list d-flex border border-secondary rounded my-4 flex-column" id="quiz-list">
            <c:forEach items="${listQuiz}" var="quiz" varStatus="loop">
                <div class="card quiz-item p-3 mb-3" id="quiz-item-${quiz.quizId}">
                    <div class="card-header w-100 d-flex justify-content-between p-2">
                        <p class="w-100 index-question">Question ${loop.index + 1}</p>
                        <div class="d-flex flex-shrink-1">
                            <button class="btn btn-sm btn-secondary mx-2" onclick="enableEditQuestion('${quiz.quizId}')">Edit</button>
                            <button class="btn btn-sm btn-danger" onclick="deleteQuestion('${quiz.quizId}')">Delete</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <label for="quizContent-${loop.index}" class="fs-5 fw-bold">Content for question:</label>
                        <textarea class="form-control p-1 mb-2" cols="200"  maxlength="1500" id="quizContent-${quiz.quizId}" readonly>${quiz.quizContent}
                        </textarea>

                        <input type="hidden" value="${quiz.quizId}" id="quizId-${loop.index}"/>
                        <input type="hidden" value="${quiz.examId.examId}" id="quizExamId-${loop.index}">
                        <blockquote class="blockquote">
                            <p class="mb-0">Answers of question</p>
                        </blockquote>
                        <ul class="list-group list-group-flush" id="list-choice-${quiz.quizId}">
                            <c:forEach items="${quiz.choices}" var="choice">
                                <li class="list-group-item mb-2 d-flex align-items-center justify-content-start" id="choice-${choice.choiceId}">
                                    <input type="checkbox" class="form-check-input" id="choice-${choice.choiceId}" ${choice.isTrue ? "checked" : ""} disabled="disabled">
                                    <textarea class="form-control p-2 mb-2 mx-2 flex-grow-1" maxlength="500" cols="300" readonly>${choice.description}</textarea>
                                    <a  href="#" onclick="deleteChoice('${choice.choiceId}')"><i class="fa-solid fa-circle-minus"></i></a>
                                    <input type="hidden" value="${choice.choiceId}" id="choiceId-${choice.choiceId}">
                                </li>
                            </c:forEach>
                        </ul>
                        <button type="button" class="btn btn-outline-success btn-sm d-none" onclick="addNewAnswer('${quiz.quizId}')" id="add-btn-quiz-${quiz.quizId}">Add new answer</button>
                        <p id="messsegeAddAnswer${quiz.quizId}"></p>
                    </div>
                    <div class="card-footer d-none" id="quiz-item-footer-${quiz.quizId}">
                        <button class="btn btn-danger btn-sm" onclick="ReloadPage()">Cancel</button>
                        <button class="btn btn-outline-success" onclick="updateQuestion('${quiz.quizId}')">Save</button>
                    </div>
                </div>
            </c:forEach>
        </div>
        <button class="btn btn-primary rounded" onclick="addNewQuiz()">Add new question</button>
        <p id="messageAddQuiz"></p>
        <%}%>
    </div>
</div>
<script src="../edit-exam/js/jquery.slim.js" crossorigin="anonymous"></script>
<script src="../edit-exam//js/autosize.min.js" crossorigin="anonymous"></script>
<script>
    var $j = jQuery.noConflict();
    autosize($j('textarea'));


    function deleteChoice(choiceId){
        $j.ajax({
            type: "POST",
            url: "<%=request.getContextPath()%>/edit-exam/delete-choice",
            data: {choiceId : choiceId},
            success: function (data) {
                console.log(data);
                $j("#choice-"+choiceId).remove();
            },
            error: function (data) {
                var $nearestP = $j(this).closest('p[id*="messsegeAddAnswer"]');
                $nearestP.addClass("text-danger");
                $nearestP.html(data);
            }
        });
    }

    function updateIndexQuestion(){
        $j('p.index-question').each(function(index) {
            // The 'index' is zero-based, so you might want to add 1 to start from 1
            $j(this).text('Question ' + (index + 1));
        });
    }
    function deleteQuestion(quizId){
        $j.ajax({
            type: "POST",
            url: "<%=request.getContextPath()%>/edit-exam/delete-quiz",
            data: {quizId : quizId},
            success: function (data) {
                console.log(data);
                $j("#quiz-item-"+quizId).remove();
                updateIndexQuestion();
            },
            error: function (data) {
                $j("#messsegeAddAnswer"+quizId).addClass("text-danger");
                $j("#messsegeAddAnswer"+quizId).html(data);
            }
        });
    }

    function updateQuestion(quizID){
        var dataChoicesUpdate = [];
        $j('#list-choice-'+quizID+' li').each(function (){
            var choice = {
                choiceId : $j(this).find('input[type="hidden"]').first().val(),
                Description : $j(this).find('textarea').first().val(),
                isTrue : $j(this).find('input[type="checkbox"]').first().is(':checked')
            };
            dataChoicesUpdate.push(choice);
        });

        if( $j('#quizContent-'+quizID).val().trim().length === 0 ||  $j('#quizContent-'+quizID).val().trim().length > 1500 ){
            $j("#messsegeAddAnswer"+quizID).addClass("text-danger");
            $j("#messsegeAddAnswer"+quizID).html('Content quiz can not be blank or langer than 1500 characters');
            return;
        }

        var isBlankChoice = dataChoicesUpdate.some(function (choice){
            return choice.Description.trim() === 0 || choice.Description.trim() > 500;
        });
        if(isBlankChoice){
            $j("#messsegeAddAnswer"+quizID).addClass("text-danger");
            $j("#messsegeAddAnswer"+quizID).html('Not allow blank choice or more than 500 character');
            return;
        }

        var hasTrueChoice = dataChoicesUpdate.some(function(choice) {
            return choice.isTrue === true;
        });
        if(!hasTrueChoice){
            $j("#messsegeAddAnswer"+quizID).addClass("text-danger");
            $j("#messsegeAddAnswer"+quizID).html('Must select at least one choice correct');
            return;
        }
        var dataQuizUpdate = {
            quizId : quizID,
            quizContent : $j('#quizContent-'+quizID).val(),
            choices: dataChoicesUpdate
        };

        $j.ajax({
            type: "POST",
            url: "<%=request.getContextPath()%>/edit-exam/update-quiz",
            data: JSON.stringify(dataQuizUpdate),
            contentType : "application/json",
            success: function (data) {
                console.log(data);
                disableEditQuestion(quizID);
            },
            error: function (data) {
                $j("#messsegeAddAnswer"+quizID).addClass("text-danger");
                $j("#messsegeAddAnswer"+quizID).html(data);
            }
        });
    }

    function enableEditQuestion(quizId){
        $j('#quiz-item-'+quizId).find('textarea').prop('readonly', false);
        $j('#quiz-item-'+quizId).find('input[type="checkbox"]').prop('disabled', false);
        $j("#quiz-item-footer-"+quizId+".d-none").toggleClass("d-none");
        $j("#add-btn-quiz-"+quizId+".d-none").toggleClass("d-none");
        $j("#messsegeAddAnswer"+quizId).html('');
    }

    function disableEditQuestion(quizId){
        $j('#quiz-item-'+quizId).find('textarea').prop('readonly', true);
        $j('#quiz-item-'+quizId).find('input[type="checkbox"]').prop('disabled', true);
        $j("#quiz-item-footer-"+quizId).toggleClass("d-none");
        $j("#add-btn-quiz-"+quizId).toggleClass("d-none");
        $j("#messsegeAddAnswer"+quizId).html('');
    }
    function addNewAnswer(quizId){
        var listAnswer = $j('#list-choice-'+quizId + ' li');
        if(listAnswer.length >= 6){
            $j("#messsegeAddAnswer"+quizId).addClass("text-danger");
            $j("#messsegeAddAnswer"+quizId).html("One question have maximum 6 answers");
            return;
        }
        $j.ajax({
            type: "POST",
            url: "<%=request.getContextPath()%>/edit-exam/add-choice",
            data: {quizId : quizId},
            success: function (data) {
                console.log(data);
                if (isJson(data)) {
                    var dataJson = JSON.parse(data);
                    $j('#list-choice-'+quizId).append(addNewChoiceHtml(dataJson));
                }
            },
            error: function (data) {
                $j("#messsegeAddAnswer"+quizId).addClass("text-danger");
                $j("#messsegeAddAnswer"+quizId).html(data);
            }
        });
    }

    function addNewChoiceHtml(data){
        return ` <li class="list-group-item mb-2 d-flex align-items-center justify-content-start" id="choice-\${data.choiceId}">
                                <input type="checkbox" class="form-check-input" id="choice-\${data.choiceId}" \${data.isTrue ? "checked" : ""}>
                                <textarea class="form-control p-2 mb-2 mx-2 flex-grow-1" maxlength="500" cols="300">\${data.Description}</textarea>
                                <a  href="#" onclick="deleteChoice('\${data.choiceId}')"><i class="fa-solid fa-circle-minus"></i></a>
                                <input type="hidden" value="\${data.choiceId}" id="choiceId-\${data.choiceId}">
                            </li>`;
    }

    function addNewQuizHtml(data){
        var indexLast = $j('#quiz-list').find('.quiz-item:last-child').index();
        console.log(indexLast);
        return `<div class="card quiz-item p-3 mb-3" id="quiz-item-\${data.quizId}">
                    <div class="card-header w-100 d-flex justify-content-between p-2">
                        <p class="w-100">Question \${indexLast + 2}</p>
                        <div class="d-flex flex-shrink-1">
                            <button class="btn btn-sm btn-secondary mx-2" onclick="enableEditQuestion('\${data.quizId}')">Edit</button>
                            <button class="btn btn-sm btn-danger" onclick="deleteQuestion('\${data.quizId}')">Delete</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <label for="quizContent-\${data.quizId}" class="fs-5 fw-bold">Content for question:</label>
                        <textarea class="form-control p-1 mb-2" cols="200"  maxlength="1500" id="quizContent-\${data.quizId}" readonly>\${data.quizContent}
                        </textarea>

                        <input type="hidden" value="\${data.quizId}" id="quizId-\${data.quizId}"/>
                        <input type="hidden" value="\${data.examId.examId}" id="quizExamId-\${data.quizId}">
                        <blockquote class="blockquote">
                            <p class="mb-0">Answers of question</p>
                        </blockquote>
                        <ul class="list-group list-group-flush" id="list-choice-\${data.quizId}">
                        </ul>
                        <button type="button" class="btn btn-outline-success btn-sm d-none" onclick="addNewAnswer('\${data.quizId}')" id="add-btn-quiz-\${data.quizId}">Add new answer</button>
                        <p id="messsegeAddAnswer\${data.quizId}"></p>
                    </div>
                    <div class="card-footer d-none" id="quiz-item-footer-\${data.quizId}">
                        <button class="btn btn-danger btn-sm" onclick="ReloadPage()">Cancel</button>
                        <button class="btn btn-outline-success" onclick="updateQuestion('\${data.quizId}')">Save</button>
                    </div>
                </div>`;
    }

    function addNewQuiz(){
        var existQuantity = $j('#existQuantity').val();
        if(isNaN(existQuantity) || Number(existQuantity) <= 0){
            $j("#messageAddQuiz").addClass("text-danger");
            $j("#messageAddQuiz").html("Invalid quantity number");
            return;
        }

        var listQuizItem = $j('.quiz-item');
        if(listQuizItem.length >= Number(existQuantity)){
            $j("#messageAddQuiz").addClass("text-danger");
            $j("#messageAddQuiz").html("Maximumn quantity of exam");
            return;
        }
        var examId = !$j("#examId").val() ? null : $j("#examId").val();
        if(($j.trim(examId)).length === 0){
            return;
        }

        $j.ajax({
            type: "POST",
            url: "<%=request.getContextPath()%>/edit-exam/add-quiz",
            data: {examId : examId},
            success: function (data) {
                console.log(data);
                if (isJson(data)) {
                    var dataJson = JSON.parse(data);
                    $j("#quiz-list").append(addNewQuizHtml(dataJson));
                }
            },
            error: function (data) {
                $j("#messageAddQuiz").addClass("text-danger");
                $j("#messageAddQuiz").html(data);
            }
        });

    }
    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    $j(document).ready(function () {
        $j('.quiz-list:empty').hide();

        $j("#examForm").submit(function (event) {
            event.preventDefault();
            console.log(validationExamForm());
            if (!validationExamForm()) {

                return;
            }
            var examId = !$j("#examId").val() ? null : $j("#examId").val();
            var formData = {
                examId: examId,
                examName: $j("#examName").val(),
                examDuration: $j("#examDuration").val(),
                examQuantity: $j("#examQuantity").val(),
                examCourseId: $j("#courseId").val()
            };


            $j.ajax({
                type: "POST",
                url: "<%=request.getContextPath()%>/edit-exam/edit-exam",
                data: formData,
                success: function (data, dataType) {
                    console.log(data);
                    if (isJson(data)) {
                        var dataJson = JSON.parse(data);
                        window.location.href = "<%=request.getContextPath()%>/edit-exam/edit-exam?examId=" + dataJson.examId;
                    } else {
                        $j("#messageReturn").addClass("text-success");
                        $j("#messageReturn").html(data);
                    }
                },
                error: function (data) {
                    $j("#messageReturn").addClass("text-danger");
                    $j("#messageReturn").html(data);
                }
            });
            removeValidation();
        });

        function validationExamForm() {
            var examName = $j("#examName").val();
            var examDuration = $j("#examDuration").val();
            var examQuantity = $j("#examQuantity").val();
            if (($j.trim(examName)).length === 0) {
                $j("#validateExamName.d-none").toggleClass("d-none");
                $j("#validateExamName").html("Must input name");
                return false;
            }
            if (($j.trim(examName)).length >= 100) {
                $j("#validateExamName.d-none").toggleClass("d-none");
                $j("#validateExamName").html("Must less than 100 characters");
                return false;
            }
            if (examDuration <= 0) {
                $j("#validateExamDuration.d-none").toggleClass("d-none");
                $j("#validateExamDuration").html("Must larger than 0");
                return false;
            }
            if (examQuantity <= 0) {
                $j("#validateExamQuantity.d-none").toggleClass("d-none");
                $j("#validateExamQuantity").html("Must larger than 0");
                return false;
            }
            return true;
        }

        function removeValidation() {
            $j("#validateExamName.d-none").toggleClass("d-none");
            $j("#validateExamDuration.d-none").toggleClass("d-none");
            $j("#validateExamQuantity.d-none").toggleClass("d-none");
        }

    });

    function ReloadPage() {
        location.reload();
    }
</script>
</body>
</html>
