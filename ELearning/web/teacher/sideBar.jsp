

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<div class="sidebar p-20 p-relative me-3">
    <h4 class="p-relative txt-c mt-0">Teacher Manager</h4>
    <ul>
        <li>
            <a class="d-flex align-center fs-14 c-black rad-6 p-10" href="all-courses">
                <i class="fa-regular fa-chart-bar fa-fw"></i>
                <span>All Courses</span>
            </a>
        </li>

        <li>
            <a class="active d-flex align-center fs-14 c-black rad-6 p-10" href="add-courses">
                <i class="fa-solid fa-graduation-cap fa-fw"></i>
                <span>Create New Course</span>
            </a>
        </li>
    </ul>
</div>

</html>
