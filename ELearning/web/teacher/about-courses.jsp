<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="model.Course"%>
<%@ page import="model.User"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="util.Helpers" %>
<%@ page import="dal.ExamDAO" %>


<%@ page import="model.Exam"%>
<%@ page import="model.Enroll"%>
<%@ page import="model.Lesson"%>
<%@ page import="model.Certificate"%>


<%@ page import="java.util.List" %>
<%@ page import="dal.CourseDAO"%>
<%@ page import="dal.EnrollDAO"%>
<%@ page import="dal.UserDAO"%>
<%@ page import="dal.ExamDetailDAO"%>
<%@ page import="dal.CertificateDao"%>
<%@ page import="dal.LessonDAO"%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>About Courses</title>
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/framework.css" />
        <link rel="stylesheet" href="css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />
        <style>
            .button-container {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .button-container a {
                display: inline-block;
                padding: 10px;
                color: #fff;
                font-size: 14px;
                border-radius: 5px;
                text-decoration: none;
                transition: background-color 0.3s;
            }
            .button-container a:hover {
                background-color: #009900;
            }
        </style>
    </head>
    <body>
        <div class="page d-flex">
            <%@include file="sideBar.jsp" %>
            <div class="content w-full">
                <!-- Header start -->
                <%@include file="../component/header.jsp" %>
                <!-- Header End -->
                <h1 class="p-relative">About Courses</h1>
                <div class="wrapper d-grid gap-20">
                    <div class="welcome bg-white rad-10 txt-c-mobile block-mobile">
                        <div class="intro p-20 d-flex space-between bg-eee">
                            <div>

                                <h2 class="m-0"> ${course.getCourseName()} </h2>
                                <p class="c-grey mt-5"></p>
                            </div>
                            <img style="width: 200px; height: 100px" class="hide-mobile"  src="<%= url %>/imageStorage/course/${course.getCourseImg()}" alt="" />
                        </div>
                        <div class="body txt-c d-flex p-20 mt-20 mb-20 block-mobile">
                            <div>Category<span class="d-block c-grey fs-14 mt-10">${course.getCategoryId().getCategoryName()}</span></div>
                            <div>Duration<span class="d-block c-grey fs-14 mt-10">${course.getDuration()} hour</span></div>
                            <div>Publish Date<span class="d-block c-grey fs-14 mt-10">${course.getPublish()}</span></div>
                            <div>Status<span class="d-block c-grey fs-14 mt-10">${course.isIdContinued()}</span></div>
                        </div>
                        <div>
                            <div class="c-white w-fit btn-shape d-block fs-14 button-container">
                                <form action="about-courses" method="post">
                                    <a href="edit-courses?courseID=${course.getCourseId()}" class="visit bg-blue">Edit Course</a>

                                    <button style="submit" class=" btn btn-danger delete-button bg-red"> 
                                        Delete
                                    </button>

                                    <input value="${course.getCourseId()}" hidden type="text" name="courseID">

                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Welcome Widget -->


                    <!-- Start Latest News Widget -->
                    <div class="latest-news p-20 bg-white rad-10 txt-c-mobile">
                        <h2 class="mt-0 mb-20">Mooc</h2>
                        <c:forEach items="${mooc}" var="mooc">
                            <div class="news-row d-flex align-center">
                                <img src="" alt="" />
                                <div class="info d-flex">
                                    <h3> ${mooc.getMoocNumber()}&nbsp; ${mooc.getMoocName()}</h3>
                                </div>
                                <div class="btn-shape bg-eee fs-13 label"></div>
                            </div>
                        </c:forEach>
                        <a href="/ELearning/MoocDetails?courseid=${course.getCourseId()}" class="visit d-block fs-14 bg-blue c-white w-fit btn-shape">Edit Mooc</a>
                    </div>
                    <!-- <div>
                        <div class="row">
                    <%
                    if (request.getParameter("courseID") == null) {
                        response.sendRedirect("/error");
                    }
                    int courseId = Helpers.parseInt(request.getParameter("courseID"), 1);
                    ExamDAO examDao = new ExamDAO();
                    List<Exam> examList = examDao.selectExamByCourseId(courseId);
                    %>
                    <!--  <div class="col-xl-3 col-xxl-4 col-lg-4 d-flex flex-column align-items-start mt-3">
                          <div class="p-2 border border-secondary w-100">
                              <h5 class="text-primary fs-6">Exam</h5>
                              <ul class="list-group list-group-flush ms-2 p-2">
                    <% for (Exam exam : examList) { %>
                    <li class="list-group-item p-2">
                        <div class="d-flex  align-items-center">
                            <div class="w-100">
                                <p class="m-0"><%= exam.getExamName() %></p>
                            </div>
                            <div class="flex-shrink-1 d-flex">
                                <a class="mx-2" href="<%= url %>/edit-exam/edit-exam?examId=<%= exam.getExamId() %>"><i class="fa-solid fa-pen-to-square"></i></a>
                                <a href="#" onclick="deleteExam('<%= exam.getExamId() %>')"><i class="fa-solid fa-circle-minus"></i></a>
                            </div>
                        </div>
                    </li>
                    <% } %>
                </ul>
                <a href="<%= url %>/edit-exam/edit-exam?examId=-1&courseId=<%= courseId %>" class="btn btn-outline-success btn-sm">Add new exam</a>
            </div>
        </div> 
    </div>
</div> -->
                    <!-- End Latest News Widget -->
                    <!-- Start Latest Uploads Widget -->
                    <div class="latest-uploads p-20 bg-white rad-10">
                        <h2 class="mt-0 mb-20">Lesson in course</h2>
                        <ul class="m-0">
                            <li class="between-flex pb-10 mb-10">
                                <div class=" align-center">
                                    <%        ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
                                    listLesson= new LessonDAO().selectAllByCourseID(courseId);
                                    %>
                                    <%for (Lesson lesson : listLesson) {%>
                                    <div class="news-row d-flex align-center" >
                                        <div class=" d-flex">
                                            <p> <%=lesson.getLessonNumber()%>&nbsp; <%=lesson.getLessonName()%></p>
                                        </div>
                                    </div>
                                    <%} %>

                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- End Latest Uploads Widget -->
                </div>
                <!-- Start Projects Table -->
                <div class="projects p-20 bg-white rad-10 m-20">
                    <h2 class="mt-0 mb-20">Students Register</h2>
                    <div class="responsive-table">
                        <table class="fs-15 w-full">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Name</td>
                                    <td>Enrollment Date</td>
                                    <td>Completion Date</td>
                                    <td>GPA</td>
                                    <td>Status</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%  int courseID = Integer.parseInt(request.getParameter("courseID"));%>
                                <%
                                    int count=1;
                                    ArrayList<Enroll> listEnroll = new ArrayList<Enroll>();
                                    listEnroll= new EnrollDAO().selectListStuEnroll(courseID);
                                %>
                                <%for (Enroll enroll : listEnroll) {%>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=enroll.getUserId().getFullName()%></td>
                                    <td><%=enroll.getDateEnroll()%></td>

                                    <%if(enroll.getProgess()==100){%>
                                    <%Certificate cer = new CertificateDao().getCertificateByUserCourseId(enroll.getUserId().getUserId(),courseID);%> 
                                    <td><%=cer.getDate()%></td>
                                    <%float score= new ExamDetailDAO().avgScore(enroll.getUserId().getUserId(),courseID);%>
                                    <td><%=score%></td>
                                    <td><span class="label btn-shape bg-green c-white">Completed</span></td>
                                    <%}else{%>
                                    <td>N/A</td>   
                                    <td>N/A</td>
                                    <td><span class="label btn-shape bg-blue c-white">In Progress</span></td>
                                    <%}%>
                                </tr>

                                <%count++;} %>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Projects Table -->
            </div>
        </div>
    </body>
</html>
