<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            User user_login = (User) session.getAttribute("acc");


%>
<%@page import="dal.TeacherDAO"%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Profile</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <link rel="stylesheet" href="../asset/CSS/avatar.css"/>
        <style>
            body {
                background: #06BBCC;
            }

            .ui-w-80 {
                width: 80px !important;
                height: auto;
            }

            .btn-default {
                border-color: rgba(24, 28, 33, 0.1);
                background: rgba(0, 0, 0, 0);
                color: #4E5155;
            }

            label.btn {
                margin-bottom: 0;
            }

            .btn-outline-primary {
                border-color: #26B4FF;
                background: transparent;
                color: #26B4FF;
            }

            .btn {
                cursor: pointer;
            }

            .text-light {
                color: #babbbc !important;
            }

            .btn-facebook {
                border-color: rgba(0, 0, 0, 0);
                background: #3B5998;
                color: #fff;
            }

            .btn-instagram {
                border-color: rgba(0, 0, 0, 0);
                background: #000;
                color: #fff;
            }

            .card {
                background-clip: padding-box;
                box-shadow: 0 1px 4px rgba(24, 28, 33, 0.012);
            }

            .row-bordered {
                overflow: hidden;
            }

            .account-settings-fileinput {
                position: absolute;
                visibility: hidden;
                width: 1px;
                height: 1px;
                opacity: 0;
            }

            .account-settings-links .list-group-item.active {
                font-weight: bold !important;
            }

            html:not(.dark-style) .account-settings-links .list-group-item.active {
                background: transparent !important;
            }

            .account-settings-multiselect ~ .select2-container {
                width: 100% !important;
            }

            .light-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(24, 28, 33, 0.03) !important;
            }

            .light-style .account-settings-links .list-group-item.active {
                color: #4e5155 !important;
            }

            .material-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(24, 28, 33, 0.03) !important;
            }

            .material-style .account-settings-links .list-group-item.active {
                color: #4e5155 !important;
            }

            .dark-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(255, 255, 255, 0.03) !important;
            }

            dark-style .account-settings-links .list-group-item.active {
                color: #fff !important;
            }

            .light-style .account-settings-links .list-group-item.active {
                color: #4E5155 !important;
            }

            .light-style .account-settings-links .list-group-item {
                padding: 0.85rem 1.5rem;
                border-color: rgba(24, 28, 33, 0.03) !important;
            }

        </style>

    </head>

    <body>

        <%@include file= "sub_header.jsp" %>
        <div class="container light-style flex-grow-1 container-p-y">
            <form action="UpdateProfile?aid=${up.userId}" enctype="multipart/form-data" method="post" id="formUpdate">
                <h4 class="font-weight-bold py-3 mb-4">
                    My Profile
                </h4>
                <% if (request.getAttribute("updateSuccess") != null && (boolean) request.getAttribute("updateSuccess")) { %>
                <div class="alert alert-success">
                    Successfully updated !
                </div>
                <% } %>
                <div class="card overflow-hidden">
                    <div class="row no-gutters row-bordered row-border-light">
                        <div class="col-md-3 pt-0">
                            <div class="list-group list-group-flush account-settings-links">
                                <a class="list-group-item list-group-item-action active" data-toggle="list"
                                   href="#account-general">My Profile</a>
                                <a class="list-group-item list-group-item-action" data-toggle="list"
                                   href="#account-change-password">Change password</a>
                                <a class="list-group-item list-group-item-action" data-toggle="list"
                                   href="#account-connections">Certificate</a>
                                <%if(user_login.getRoleid().getRoleId()==0){%>
                                <a class="list-group-item list-group-item-action" data-toggle="list"
                                   href="#account-teacher">Become Teacher</a>
                                <%}%>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="account-general" onsubmit="return checkPassword()">
                                    <div class="card-body media align-items-center">

                                        <%String avatar = user.getAvatar(); %>
                                        <%if (avatar.contains("//")) {%>
                                        <img class="d-block ui-w-80 bigAvatar" alt ="avatar" src="<%=user.getAvatar()%>" alt=""> 
                                        <%}else{%>
                                        <img class="d-block ui-w-80 bigAvatar" alt ="avatar" style="margin-right:5px" src="<%=url%>/imageStorage/user/<%=user.getAvatar()%>" alt=""> 
                                        <%}%>
                                        <div class="media-body ml-4">
                                            <label class="btn btn-outline-primary d-none" id="btn-upload-image">
                                                Upload Image
                                                <input type="file" class="account-settings-fileinput" accept="image/*"
                                                       id="avaUpload" name="avatar"/>
                                            </label> &nbsp;
                                            <button type="button" class="btn btn-outline-primary"
                                                    onclick="enableEditProfile(this)">
                                                Edit profile
                                            </button>
                                        </div>
                                    </div>
                                    <hr class="border-light m-0">
                                    <div class="card-body w-50">
                                        <div class="form-group">
                                            <label class="form-label">Username</label>
                                            <input type="text" class="form-control mb-1" value="${up.fullName}" name="name"
                                                   readonly>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Phone</label>
                                            <input type="text" class="form-control" value="${up.phone}" name="mobile" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">E-mail</label>
                                            <input type="email" class="form-control" value="${up.email}" name="email" disabled/>
                                            <!--                                    <div class="alert alert-warning mt-3">
                                                                                    Your email is not confirmed. Please check your inbox.<br>
                                                                                </div>-->
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Address</label>
                                            <input type="text" class="form-control" value="${up.address}" name="address"
                                                   readonly>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Address</label>
                                            <input type="date" class="form-control" value='<fmt:formatDate pattern = "yyyy-MM-dd"
                                                            value = "${up.dateOfBirth}"/>' name="dob" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label class="labels">Gender</label>
                                            <select id="profile-gender" name="gender" class="form-control" disabled="true">
                                                <option value="">-Select Gender-</option>
                                                <option value="Male"
                                                        ${up.gender ? 'selected' : ''}>Male
                                                </option>
                                                <option value="Female"
                                                        ${!up.gender ? 'selected' : ''}>Female
                                                </option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="my-3 text-center d-none"
                                         id="btn-group-profile">
                                        <div class="d-flex">
                                            <button type="button" class="btn btn-danger profile-button mx-2"
                                                    onclick="reloadPage()">
                                                Cancel
                                            </button>
                                            <button class="btn btn-primary profile-button" type="submit" id="saveProfile">Save Profile
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="account-change-password">
                                    <div class="card-body pb-2">
                                        <div class="form-group">
                                            <label class="form-label">Current password</label>
                                            <input type="password" class="form-control" id="current-password" name="oldpass">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">New password</label>
                                            <input type="password" class="form-control" id="new-password" name="newpass">
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Repeat new password</label>
                                            <input type="password" class="form-control" id="repeat-password" name="repass">
                                        </div>
                                        <div id="password-error" style="color: red;"></div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" id="changePass">Change
                                                Password
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="account-connections">


                                    <c:forEach items="${requestScope.certificate}" var="ce">
                                        <section id="testimonials" class="border border-1 shadow rounded-2 mb-3 p-3">
                                            <div class="testimonials-box-container">
                                                <div class="testimonials-box" style="padding-bottom:  5px">

                                                    <div class="box-top row">
                                                        <div class="col-3">
                                                            <img
                                                                src="<%=url%>/userProfile/certificate-icon.jpg" class="img-thumbnail" alt="certificate"/>
                                                        </div>

                                                        <div class="profile col-9">

                                                            <div class="name-user">
                                                                <div style="cursor: pointer">
                                                                    <a href="certificatedeatil?id=${ce.certificateId}">
                                                                        <strong style="color:#0066e5">${ce.courseId.courseName}</strong></a>
                                                                    <p style="color:black; margin-bottom:0px"> ${ce.category.categoryName}</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <hr style="margin-bottom:5px">
                                                    <div class="text-center">
                                                        <p style="color: #979797;font-size: 1rem; margin-bottom: 0px;display: inline-block;cursor: pointer ">
                                                            Share <i class="fa-solid fa-angle-down" style="font-size:10px"></i>
                                                        </p>
                                                        <a href="<%=url%>/exportcertificate?id=${ce.certificateId}" style="color: #02c730;font-size: 1rem; margin-bottom: 0px;display: inline-block;cursor: pointer ">
                                                            Export PDF <i class="fa-solid fa-angle-down" style="font-size:10px"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </c:forEach>
                                </div>

                                <div class="tab-pane fade" id="account-teacher">
                                    <%if(new TeacherDAO().checkSentRequest(user.getUserId())){%>

                                    <p style="color:blue" id="title">Your request to become a teacher is in the approval stage!. Please check your email regularly for more details.</p>

                                    <%}else{%>

                                    <div id="question">
                                        <p id="title">Do you really want to be a teacher</p>
                                        <a id="continue" class="btn btn-primary">Continue</a> 
                                    </div>
                                    <%}%>

                                    <div id="content" style="display: none" >
                                        <div class="card-body pb-2">
                                            <!--                                        <p style="color:red">* Please fill in the information below to become a teacher!</p>-->
                                            <p style="color:red; font-size: 13px">* Require</p>

                                            <div class="form-group">
                                                <label class="form-label">Current Job <span style="color:red">*</span></label>
                                                <input id="job" type="text" class="form-control check" placeholder="Teacher Programing" >
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Current Workplace <span style="color:red">*</span></label>
                                                <input id="place" type="text" class="form-control check" placeholder="FPT University" >
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Personal Website</label>
                                                <input id="website" type="text" class="form-control" placeholder="http://localhost:9999/ELearning">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label"> Facebook</label>
                                                <input id="facebook" type="text" class="form-control" placeholder="https://www.facebook.com/">
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label"> Linkedin</label>
                                                <input id="linkedin" type="text" class="form-control" placeholder="https://www.linkedin.com/in/">
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label"> CV (Google Drive) <span style="color:red">*</span>
                                                </label>
                                                <input id="cv" type="text" class="form-control check" placeholder="Cv">
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Bio <span style="color:red">*</span></label>
                                                <textarea id="about" class="form-control check"
                                                          rows="3" placeholder="About me"></textarea>
                                            </div>
                                        </div>


                                        <div class="text-right mt-3 mb-4" style="margin-bottom: 20px">
                                            <button style="pointer-events: none; cursor: not-allowed; opacity: 0.4" type="button" class="btn btn-primary mb-4" id="sendTeacher">Send Require</button>&nbsp;
                                            <button type="button" class="btn btn-default  mb-4" id="cancelTeacher">Cancel</button>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <input type="hidden" name="update" id="updateField">
                    </div>
                </div>
            </form>
        </div>
        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
        <script>

                                                        function readURL() {
                                                            var $input = $(this);
                                                            var $newinput = $(this).parent().parent().parent().find('.bigAvatar');
                                                            if (this.files && this.files[0]) {
                                                                var reader = new FileReader();
                                                                reader.onload = function (e) {
                                                                    $newinput.attr('src', e.target.result).show();
                                                                };
                                                                reader.readAsDataURL(this.files[0]);
                                                            }
                                                        }

                                                        $("#avaUpload").change(readURL);


                                                        function reloadPage() {
                                                            location.reload();
                                                        }

                                                        function enableEditProfile(button) {
                                                            $('#formUpdate').find('input[readonly]').prop('readonly', false)
                                                            $('#formUpdate').find('select').prop('disabled', false)
                                                            $('#btn-upload-image.d-none').toggleClass('d-none')
                                                            $('#btn-group-profile.d-none').toggleClass('d-none')
                                                            $(button).toggleClass('d-none')
                                                        }

                                                        function validateDate(date) {
                                                            var inputDate = new Date(date);
                                                            // Boundary dates
                                                            var minDate = new Date('1-1-1900');
                                                            var maxDate = new Date(); // today

                                                            // Validate the input date
                                                            if (inputDate > maxDate) {
                                                                alert('Date cannot be later than today.');
                                                                return false;
                                                            } else if (inputDate < minDate) {
                                                                alert('Date cannot be earlier than 1-1-1900.');
                                                                return false;
                                                            } else {
                                                                return true;
                                                            }
                                                            return false;
                                                        }

                                                        document.addEventListener("DOMContentLoaded", function () {
                                                            var formUpdate = document.getElementById("formUpdate");
                                                            var saveProfileButton = document.getElementById('saveProfile');
                                                            var changePasswordButton = document.getElementById('changePass');
                                                            var currentPasswordInput = document.getElementById("current-password");
                                                            var newPasswordInput = document.getElementById("new-password");
                                                            var repeatPasswordInput = document.getElementById("repeat-password");
                                                            var passwordError = document.getElementById("password-error");
                                                            var phoneInput = document.querySelector("input[name='mobile']");
                                                            var usernameInput = document.querySelector("input[name='name']");
                                                            var addressInput = document.querySelector("input[name='address']");
                                                            var dobInput = document.querySelector("input[name='dob']");
                                                            var passAcc = '<%= request.getAttribute("passAcc") %>';


                                                            saveProfileButton.addEventListener("click", function (event) {
                                                                event.preventDefault();

                                                                // Hỏi người dùng trước khi cập nhật hồ sơ
                                                                if (!confirm("Bạn có chắc muốn cập nhật hồ sơ không?")) {
                                                                    return; // Nếu người dùng nhấn "Cancel", hàm sẽ dừng lại ở đây
                                                                }
                                                                if (usernameInput.value.trim().length === 0 || usernameInput.value.trim().length > 200) {
                                                                    alert("Tên không được trống hoặc quá 200 kí tự");
                                                                    return;
                                                                }
                                                                const validatePhone = (phone) => {
                                                                    return phone.match(
                                                                            /(?:7|0\d|\+94\d)\d{8}/
                                                                            );
                                                                };

                                                                // Check phone number format (10 digits)
                                                                var phoneRegex = /^\d{10}$/;
                                                                if (!validatePhone(phoneInput.value)) {
                                                                    alert("Vui lòng nhập đúng định dạng số điện thoại");
                                                                    return;
                                                                }

                                                                // Check username and address for special characters
                                                                var specialCharRegex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                                                                if (specialCharRegex.test(usernameInput.value)) {
                                                                    alert("Tên không chứa kí tự đặc biệt");
                                                                    return;
                                                                }

                                                                if (addressInput.value.trim().length > 300) {
                                                                    alert("Địa chỉ không  quá 300 kí tự");
                                                                    return;
                                                                }
                                                                if (!validateDate(dobInput.value)) {
                                                                    return;
                                                                }
                                                                // If all validations pass, set the update field and submit the form
                                                                document.getElementById("updateField").value = "profile";
                                                                formUpdate.submit();
                                                            });

                                                            changePasswordButton.addEventListener("click", function (event) {
                                                                event.preventDefault();
                                                                if (!confirm("Bạn có chắc muốn thay đổi mật khẩu không?")) {
                                                                    return; // Nếu người dùng nhấn "Cancel", hàm sẽ dừng lại ở đây
                                                                }
                                                                if (currentPasswordInput.value !== passAcc) {
                                                                    passwordError.textContent = "Current password is incorrect.";
                                                                    return;
                                                                }

                                                                // Check if new password and repeat password match and are longer than 6 characters
                                                                if (newPasswordInput.value !== repeatPasswordInput.value || newPasswordInput.value.length < 6) {
                                                                    passwordError.textContent = "New password and Repeat new password must match and be longer than 6 characters.";
                                                                    return;
                                                                }

                                                                // If all validations pass, set the update field and submit the form
                                                                document.getElementById("updateField").value = "pass";
                                                                formUpdate.submit();
                                                            });
                                                        });


        </script>
        <script>
            var continueButton = document.getElementById("continue");
            continueButton.addEventListener("click", function () {

                var content = document.getElementById("content");
                content.style.display = "block";

                var question = document.getElementById("question");
                question.style.display = "none";
            });


            var teacher_input = document.getElementById("account-teacher");
            var inputsToCheck = teacher_input.querySelectorAll('.check');
            var sendTeacher = document.getElementById('sendTeacher');

// Thêm sự kiện "input" cho từng trường nhập liệu
            inputsToCheck.forEach(function (input) {
                input.addEventListener('input', function () {
                    // Biến để kiểm tra xem tất cả input đã được nhập hay chưa
                    var allInputsFilled = true;

                    // Kiểm tra từng trường nhập liệu
                    inputsToCheck.forEach(function (input) {
                        if (input.value === '') {
                            allInputsFilled = false;
                            return;
                        }
                    });

                    // Kiểm tra kết quả và cập nhật trạng thái của nút "Gửi thông tin"
                    if (allInputsFilled) {
                        sendTeacher.style.pointerEvents = "auto";
                        sendTeacher.style.cursor = "pointer";
                        sendTeacher.style.opacity = "1";
                    }
                });
            });

            sendTeacher.addEventListener("click", function () {
                var job = document.getElementById("job").value;
                var place = document.getElementById("place").value;
                var website = document.getElementById("website").value;
                var facebook = document.getElementById("facebook").value;
                var linkedin = document.getElementById("linkedin").value;
                var cv = document.getElementById("cv").value;
                var about = document.getElementById("about").value;

                var myData = {job: job, place: place, website: website, facebook: facebook, linkedin: linkedin, cv: cv, about: about};
                $.ajax({
                    type: "POST",
                    url: "http://localhost:9999/ELearning/RequestTeacher",
                    data: myData,
                    success: function (response) {
                        if (response == "1") {
                            Swal.fire(
                                    'Congratulations',
                                    'Request Sent Successfully',
                                    'success'
                                    )

                            var content = document.getElementById("content");
                            content.style.display = "none";
                            var continue_div = document.getElementById("continue");
                            continue_div.style.display = "none";

                            var question = document.getElementById("question");
                            question.style.display = "block";

                            var title = document.getElementById("title");
                            title.style.color = "blue";
                            title.innerHTML = "Your request to become a teacher is in the approval stage!. Please check your email regularly for more details.";
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong! Please try again!',
                            })
                        }
                    },
                    error: function (xhr, status, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong! Please try again!',
                        })
                    }
                });


            });


        </script>
    </body>
</html>