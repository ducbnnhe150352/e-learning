<%-- 
    Document   : exportFile
    Created on : Oct 23, 2023, 3:36:20 PM
    Author     : ADMIN
--%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dal.UserDAO"%>
<%@page import="dal.EnrollDAO"%>

<%@page import="model.User"%>
<%@page import="model.Course"%>
<%@page import="model.Lesson"%>
<%@page import="model.Review"%>
<%@page import="model.Enroll"%>


<%
        int count=1;
%>
<%
    String select = (String) request.getAttribute("select");
    if(select == null){select = "-1";}
            ArrayList<User> listUser = new ArrayList<User>();
            ArrayList<Course> listCoursee = new ArrayList<Course>();
            ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
                    ArrayList<Enroll> listEnroll = new ArrayList<Enroll>();
                    ArrayList<Review> listReview = new ArrayList<Review>();


    
    if(select.equals("0")){
        listUser= (ArrayList) request.getAttribute("listUser");
    }else if(select.equals("1")){
            listCoursee= (ArrayList) request.getAttribute("listCourse");
    }else if(select.equals("2")){
            listLesson= (ArrayList) request.getAttribute("listLesson");
    }else if(select.equals("3")){
        listEnroll=(ArrayList) request.getAttribute("listEnroll");
    }else if(select.equals("4")){
            listReview=(ArrayList) request.getAttribute("listReview");

    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="<%=url%>/admin/css/all.min.css" />
        <link rel="stylesheet" href="<%=url%>/admin/css/framework.css" />
        <link rel="stylesheet" href="<%=url%>/admin/css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.6/xlsx.full.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.6/jszip.js"></script>
        <script src="https://unpkg.com/exceljs/dist/exceljs.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/exceljs/4.4.0/exceljs.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>

    </head>
    <body>

        <div class="page d-flex">
            <jsp:include page="sidebar.jsp"></jsp:include>
                <div class="content w-full">
                    <!-- Start Head -->

                    <div>
                    <%@include file= "../component/header.jsp" %>
                </div>

                <!-- End Head -->


                <!-- Start Projects Table -->
                <div class="projects p-20 bg-white rad-10 m-20">

                    <div class="head bg-white p-15 between-flex">
                        <h2 class="mt-0 mb-20">Export Files</h2>
                    </div>   

                    <label for="inputStatus" class="form-label">Options<span class="red-star"></span></label>

                    <div class="d-flex">
                        <div class="form-group mb-3" style="width: 20%;">
                            <form action="http://localhost:9999/ELearning/FileExport" method="post">

                                <div class="d-flex align-items-center">
                                    <select name="select" class="form-control form-select" id="comboBox">
                                        <option selected>--select--</option>
                                        <option value="0">Users Information</option>
                                        <option value="1">Courses Details</option>
                                        <option value="2">Lessons Details</option>
                                        <option value="3">Enroll Students</option>
                                        <option value="4">Reviews Course</option>
                                    </select>
                                    <button type="submit" class="btn btn-primary" style="margin-left: 1rem">Select</button>
                                </div>

                            </form>

                        </div>
                        <div class="d-flex align-items-center" style="margin-left: auto;">
                            <button id="exportButton" class="btn btn-success"> 
                                <i class="fa-solid fa-file-export"></i>    
                                Export To File Excel
                            </button>   
                        </div>
                    </div>


                    <div class="responsive-table">
                        <table class="fs-14 w-full " id="dataTable">
                            <thead>
                                <%if(select.equals("0")){%>
                                <tr>
                                    <th>No</th>
                                    <th>Full Name</th>
                                    <th>Date Of Birth</th>
                                    <th>Email</th>                                    
                                    <th>Phone</th>                                    
                                    <th>Address</th>
                                    <th>Gender</th>
                                    <th>Role</th>
                                    <th>Reason Ban</th>
                                    <th>Time Ban</th>
                                    <th>Is Verify</th>
                                </tr>
                                <% }%>
                                <%if(select.equals("1")){%>
                                <tr>
                                    <th>No</th>
                                    <th>Course Name</th>
                                    <th>Category</th>
                                    <th>Author</th>
                                    <th>Publish Date</th>                                    
                                    <th>Duration</th>                                    
                                    <th>Report</th>
                                    <th>Is Discontinued</th>
                                    <th class="w-40">Description</th>
                                </tr>
                                <% }%>
                                <%if(select.equals("2")){%>
                                <tr>
                                    <th>No</th>
                                    <th>Lesson Number</th>
                                    <th>Course Name</th>
                                    <th>Lesson Name</th>
                                    <th>Mooc Name</th>
                                    <th>Lesson Url</th>                                    
                                    <th class="w-40">Description</th>
                                </tr>
                                <% }%>
                                <%if(select.equals("3")){%>
                                <tr>
                                    <th>No</th>
                                    <th>Student Name</th>
                                    <th>Course Name</th>
                                    <th>Progress</th>
                                    <th>Date Enroll</th>
                                    <th>Status</th>
                                </tr>
                                <% }%>
                                <%if(select.equals("4")){%>
                                <tr>
                                    <th>No</th>
                                    <th>Course Name</th>
                                    <th>User Name</th>
                                    <th>Rating(/5*)</th>
                                    <th>Review Time</th>
                                    <th>Review Content</th>
                                    <th>Is Report</th>
                                </tr>
                                <% }%>

                            </thead>
                            <tbody>
                                <%if(select.equals("0")){%>
                                <%for (User u : listUser) {%>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=u.getFullName()%></td>
                                    <td><%=u.getDateOfBirth()%></td>
                                    <td><%=u.getEmail()%></td>
                                    <td><%=u.getPhone()%></td>
                                    <td><%=u.getAddress()%></td>
                                    <%if(u.getGender()==true){%>
                                    <td>Male</td>
                                    <% }else{%>
                                    <td>Female</td>
                                    <% }%>
                                    <td><%=u.getRoleid().getRoleName()%></td>
                                    <td><%=u.getReason()%></td>
                                    <td><%=u.getTimeBan()%></td>
                                    <td><%=u.getIsVerify()%></td>
                                </tr>
                                <%count++;} %>
                                <% }%>

                                <%if(select.equals("1")){%>
                                <%for (Course course : listCoursee) {%>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=course.getCourseName()%></td>
                                    <td><%=course.getCategoryId().getCategoryName()%></td>
                                    <td><%=course.getUserId().getFullName()%></td>
                                    <td><%=course.getPublish()%></td>
                                    <td><%=course.getDuration()%></td>
                                    <td><%=course.getReport()%></td>
                                    <td><%=course.isIdContinued()%></td>
                                    <td><%=course.getDescription()%></td>

                                </tr>
                                <%count++;} %>
                                <% }%>

                                <%if(select.equals("2")){%>
                                <%for (Lesson lesson : listLesson) {%>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=lesson.getLessonNumber()%></td>
                                    <td><%=lesson.getCourse().getCourseName()%></td>
                                    <td><%=lesson.getLessonName()%></td>
                                    <td><%=lesson.getMoocId().getMoocName()%></td>
                                    <td><%=lesson.getLessonUrl()%></td>
                                    <td><%=lesson.getDescription()%></td>

                                </tr>
                                <%count++;} %>
                                <% }%>
                                <%if(select.equals("3")){%>
                                <%for (Enroll enroll : listEnroll) {%>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=enroll.getUserId().getFullName()%></td>
                                    <td><%=enroll.getCourseId().getCourseName()%></td>
                                    <td><%=enroll.getProgess()%></td>
                                    <td><%=enroll.getDateEnroll()%></td>
                                    <%if(enroll.isStatus()==true){%>
                                    <td>Complete</td>
                                    <% }else{%>
                                    <td>Studying</td>
                                    <% }%>
                                </tr>
                                <%count++;} %>
                                <% }%>
                                <%if(select.equals("4")){%>
                                <%for (Review review : listReview) {%>
                                <tr>
                                    <td><%=count%></td>
                                    <td><%=review.getCourseId().getCourseName()%></td>
                                    <td><%=review.getUserId().getFullName()%></td>
                                    <td><%=review.getRating()%></td>
                                    <td><%=review.getTime()%></td>
                                    <td><%=review.getReviewContent()%></td>
                                    <td><%=review.isIsReport()%></td>

                                </tr>
                                <%count++;} %>
                                <% }%>

                            </tbody>
                        </table>
                    </div>
                </div>   
            </div>
        </div>

        <script>
            var value =<%=select%>;

            document.getElementById("exportButton").addEventListener("click", function () {
                // Lấy bảng HTML có id là "dataTable"
                var table = document.getElementById("dataTable");

                // Tạo một Workbook mới
                var workbook = new ExcelJS.Workbook();
                var worksheet = workbook.addWorksheet("Sheet 1");
                worksheet.getColumn(2).width = 25;
                worksheet.getColumn(4).width = 25;
                worksheet.getColumn(6).width = 25;

                // Tạo dòng tiêu đề "Danh sách Người dùng (Student)"
                var titleRow = worksheet.addRow([]);
                if (value == 0) {
                    titleRow.getCell(1).value = "List Of All Users";
                } else if (value == 1) {
                    titleRow.getCell(1).value = "List Of Course Details";
                } else if (value == 2) {
                    titleRow.getCell(1).value = "List Of Lessons Details";
                } else if (value == 3) {
                    titleRow.getCell(1).value = "List Of Enroll Students";
                } else if (value == 4) {
                    titleRow.getCell(1).value = "List Of Reviews Courses";
                }
                // Merge các ô từ cột 1 đến cột số cột trong tiêu đề
                if (value == 0) {
                    worksheet.mergeCells("A1:K1");
                } else if (value == 1) {
                    worksheet.mergeCells("A1:I1");
                } else if (value == 2) {
                    worksheet.mergeCells("A1:G1");
                } else if (value == 3) {
                    worksheet.mergeCells("A1:F1");
                } else if (value == 4) {
                    worksheet.mergeCells("A1:G1");
                }
                // Đặt cỡ chữ cho ô tiêu đề
                titleRow.getCell(1).font = {bold: true, size: 14};
                titleRow.getCell(1).alignment = {horizontal: "center", vertical: "middle"};


                // Tạo một dòng để lưu trữ tiêu đề cột
                var headerRow = worksheet.addRow([]);
                table.querySelectorAll("th").forEach(function (th) {
                    headerRow.getCell(th.cellIndex + 1).value = th.innerText;
                    // Tô đậm tiêu đề cột
                    headerRow.getCell(th.cellIndex + 1).font = {bold: true};
                    headerRow.getCell(th.cellIndex + 1).fill = {
                        type: "pattern",
                        pattern: "solid",
                        fgColor: {argb: "c6efce"}
                    };
                });

                // Sao chép dữ liệu từ bảng HTML vào bảng Excel
                table.querySelectorAll("tr").forEach(function (row) {
                    if (!row.querySelector("th")) {
                        var newRow = worksheet.addRow([]);
                        row.querySelectorAll("td").forEach(function (cell) {
                            newRow.getCell(cell.cellIndex + 1).value = cell.innerText;
                        });
                    }
                });

                var currentDate = new Date();
                var month = currentDate.getMonth() + 1;

                var link = "UserList_" + currentDate.getDate() + "_" + month + "_" + currentDate.getFullYear()
                        + "_" + currentDate.getHours() + "_" + currentDate.getMinutes() + "_" + currentDate.getSeconds() + ".xlsx";

                // Lưu tệp Excel
                workbook.xlsx.writeBuffer().then(function (buffer) {
                    var blob = new Blob([buffer], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
                    saveAs(blob, link);
                });
            });

            var selectElement = document.getElementById("comboBox");

            for (var i = 0; i < selectElement.options.length; i++) {
                var option = selectElement.options[i];
                // Kiểm tra giá trị của tùy chọn
                if (option.value == value) {
                    // Đặt thuộc tính "selected" cho tùy chọn cần chọn
                    option.selected = true;
                }
            }

        </script>
    </body>
</html>
