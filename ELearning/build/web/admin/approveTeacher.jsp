<%-- 
    Document   : approveTeacher
    Created on : Oct 26, 2023, 9:31:05 PM
    Author     : Thanh
--%>




<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="dal.CourseDAO"%>
<%@page import="dal.RoleDAO"%>
<%@page import="dal.AccountDAO"%>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Settings</title>
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/framework.css" />
        <link rel="stylesheet" href="css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />

    </head>
    <body>
        <div class="page d-flex">
                

                <!-- Start Projects Table -->
                <div class="projects p-20 bg-white rad-10 m-20">

                    <div class="head bg-white p-15 between-flex">
                        <h2 class="mt-0 mb-20">Apply become Teacher</h2>

                        <form action="user" method="post">
                            <div class="search p-relative" type="submit">

                                <input class="p-10" type="search" name="txtSearch" placeholder="Type A Keyword" />

                            </div>     
                        </form>

                    </div>
                    <div class="responsive-table">
                        <table class="fs-14 w-full ">
                            <thead>
                                <tr>
                                    <td class="w-10">User ID</td>
                                    <td>User Name</td>
                                    <td>Email</td>
                                    <td>Access</td>                                    
                                    <td>Role</td>                                    
                                    <td>Reason</td>
                                    <td>Action</td>



                                </tr>
                            </thead>
                            <tbody>
                               

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Projects Table -->
                <!-- Thêm phân trang -->
                <!-- Thêm một lớp CSS cho phân trang -->
                <div class="pagination-sm" style="" >
                    <c:forEach begin="1" end="${endPage}" var="i">
                            <a href="#">${i}</a>
                         
                    </c:forEach>
                </div>



            </div>
        </div>
    </body> 
</html>

