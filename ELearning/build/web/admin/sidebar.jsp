<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    User user = (User)  session.getAttribute("acc");
    if(user == null){
        response.sendRedirect("login");
    }
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Settings</title>
        <link rel="stylesheet" href="<%=url%>/admin/css/all.min.css" />
        <link rel="stylesheet" href="<%=url%>/admin/css/framework.css" />
        <link rel="stylesheet" href="<%=url%>/admin/css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />
    </head>
    <body>
        <!-- sidebar start -->
        <div class="sidebar bg-white p-20 p-relative">
            <h3 class="p-relative txt-c mt-0"><a href="<%=url%>/">Admin</a></h3>
            <ul>
                <li>
                    <a class="active d-flex align-center fs-14 c-black rad-6 p-10" href="dashboard">
                        <i class="fa-regular fa-chart-bar fa-fw"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14 c-black rad-6 p-10" href="user">
                        <i class="fa-regular fa-user fa-fw"></i>
                        <span>User management</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14 c-black rad-6 p-10" href="reportReview.jsp">
                        <i class="fa-regular fa-user fa-fw"></i>
                        <span>Report reviews</span>
                    </a>
                </li>
                <li>
                    <a class="d-flex align-center fs-14 c-black rad-6 p-10" href="course">
                        <i class="fa-solid fa-graduation-cap fa-fw"></i>
                        <span>Approve Course</span>
                    </a>
                </li>

               

                <li>
                    <a class="d-flex align-center fs-14 c-black rad-6 p-10" href="teacher">
                        <i class="fa-regular fa-circle-user fa-fw"></i>
                        <span>Approve Teacher</span>
                    </a>
                </li>
                
                
                <li>
                    <a class="d-flex align-center fs-14 c-black rad-6 p-10" href="exportFile.jsp">
                        <i class="fa-solid fa-file-export"></i>        
                        <span>Export To Excel Files </span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- end sidebar -->
    </body>