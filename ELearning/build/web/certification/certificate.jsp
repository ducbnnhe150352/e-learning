<%-- 
    Document   : cerficate
    Created on : Sep 21, 2023, 9:38:43 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>JSP Page</title>

        <style>
            @import url('https://fonts.googleapis.com/css?family=Open+Sans|Pinyon+Script|Rochester');

            /* Reset and base styles */
            *, *:before, *:after {
                box-sizing: border-box;
            }

            body {
                padding: 20px 0;
                background: #ccc;
                font-family: 'Open Sans', sans-serif;
                margin: 0; /* Resets default margin */
            }

            .container {
                margin: 0 auto;
            }

            .text-center {
                text-align: center;
            }

            /* Grid system */
            .row {
                width: 100%;
                clear: both; /* Clears floats */
            }

            .col, .col-auto, .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {
                float: left;
                min-height: 1px;
            }

            .col-1 {
                width: 8.33333%;
            }
            .col-2 {
                width: 16.66667%;
            }
            /* ...repeat for other column sizes... */
            .col-12 {
                width: 100%;
            }

            /* Typography and elements styling */
            .cursive {
                font-family: 'Pinyon Script', cursive;
            }

            .sans {
                font-family: 'Open Sans', sans-serif;
            }

            .bold {
                font-weight: bold;
            }

            .block {
                display: block;
            }

            .underline {
                border-bottom: 1px solid #777;
                padding: 5px;
                margin-bottom: 15px;
            }

            .margin-0 {
                margin: 0;
            }

            .padding-0 {
                padding: 0;
            }

            .pm-empty-space {
                height: 40px;
                width: 100%;
            }

            /* Certificate container and border styles */
            .pm-certificate-container {
                width: 800px;
                height: 600px;
                background-color: #618597;
                padding: 30px;
                color: #333;
                margin: 100px auto; /* Center the container */
                position: relative; /* For absolute positioned children */
            }

            .outer-border,
            .inner-border,
            .pm-certificate-border {
                border: 2px solid #fff;
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%); /* Centers the element */
            }

            .outer-border {
                width: 794px;
                height: 594px;
            }

            .inner-border {
                width: 730px;
                height: 530px;
            }

            .pm-certificate-border {
                width: 720px;
                height: 520px;
                background-color: #fff;
                padding: 0;
            }

            /* Inner content styles */
            .pm-certificate-block {
                width: 650px;
                height: 200px;
                position: absolute;
                left: 50%;
                top: 70px;
                transform: translateX(-50%);
            }

            .pm-certificate-title h2 {
                font-size: 34px;
                margin: 0; /* Resets default margin */
            }

            /* ... */

            /* Footer layout */
            .pm-certificate-footer {
                width: 650px;
                height: 100px;
                position: absolute;
                left: 50%;
                bottom: 30px; /* Adjust based on actual footer position */
                transform: translateX(-50%);
            }

            .pm-certificate-footer .col-4,
            .pm-certificate-footer .col-8 {
                float: left;
            }

            .pm-certificate-footer .col-4 {
                width: 33.33333%;
            }

            .pm-certificate-footer .col-8 {
                width: 66.66667%;
            }

            .pm-certificate-footer img {
                width: 150px;
                height: auto;
                display: inline-block;
            }

        </style>
    </head>

    <body style=" user-select: none;">
        <div class="pm-certificate-container">
            <div class="outer-border"></div>
            <div class="inner-border"></div>

            <div class="pm-certificate-border col-12">
                <div class="row pm-certificate-header">
                    <div class="pm-certificate-title cursive col-12 text-center">
                        <h2>Elearning Online Learning System</h2>
                    </div>
                </div>

                <div class="row pm-certificate-body">

                    <div class="pm-certificate-block">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                                <div class="pm-certificate-name underline margin-0 col-8 text-center">
                                    <span class="pm-name-text bold">${certificate.userId.fullName}</span>
                                </div>
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                                <div class="pm-earned col-8 text-center">
                                    <span class="pm-earned-text padding-0 block cursive">has earned</span>
                                    <span class="pm-credits-text block bold sans">PD175: 1.0 Credit Hours</span>
                                </div>
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                                <div class="col-12"></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                                <div class="pm-course-title col-8 text-center">
                                    <span class="pm-earned-text block cursive">while completing the training course entitled</span>
                                </div>
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                                <div class="pm-course-title underline col-8 text-center">
                                    <span class="pm-credits-text block bold sans">${certificate.courseId.courseName}</span>
                                </div>
                                <div class="col-2"><!-- LEAVE EMPTY --></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="pm-certificate-footer">
                            <div class="row">
                                <div class="col-4 pm-certified col-4 text-center">
                                    <span class="pm-credits-text block sans">Hoa Lac Hi-Tech Park </span>
                                    <span class="pm-empty-space block underline"></span>
                                    <span class="bold block">FPT University</span>
                                </div>
                                <div class="col-4 text-center">
                                    <img style="height: 100px; margin-bottom: 50px;" src="<%=request.getContextPath()%>/certification/1.png"
                                         alt="icon"/>
                                </div>
                                <div class=" pm-certified col-4 text-center">
                                    <span class="pm-credits-text block sans">Date Completed</span>
                                    <span class="pm-empty-space block underline">${certificate.date}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </body>
</html>
