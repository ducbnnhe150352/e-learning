<%-- 
    Document   : teacherProfile
    Created on : Oct 9, 2023, 5:46:49 PM
    Author     : ADMIN
--%>
<%@page import="model.Teacher"%>
<%@page import="model.User"%>

<%@page import="dal.TeacherDAO"%>
<%@page import="dal.UserDAO"%>
<%@page import="dal.ReviewDAO "%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<%
    int userIdParam = 0;
    String userIdParamString = request.getParameter("teacherId");
if (userIdParamString != null && !userIdParamString.isEmpty()) {
    try {
        userIdParam = Integer.parseInt(userIdParamString);
    } catch (NumberFormatException e) {
        userIdParam = 0; 
    }
}
    
    User user_input = new UserDAO().selectById(userIdParam);
    
    Teacher teacher = new TeacherDAO().selectByUserId(user_input.getUserId());
    int numberCourse = new CourseDAO().countCourseByUserId(user_input.getUserId());
    int numberErnolled = new CourseDAO().countNumberEnrolledByUserId(user_input.getUserId());
    
    
    double ratingAvg= new ReviewDAO().ratingAverageByUserId(teacher.getUser().getUserId());
            List<Course> listCourses = new ArrayList<>();
            listCourses= new CourseDAO().selectByUserID(user_input.getUserId());
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Teacher Profile</title>
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
            integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
            />
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
            />
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>
        <link rel="stylesheet" href="../asset/CSS/teachProfile.css"/> 

    </head>
    <body>
        <%@include file= "../component/header.jsp" %>

        <section class="teacher">
            <div class="container">
                <div class="row p-5">
                    <div class="col-lg-12 mb-4 mb-sm-5">
                        <div class="card card-style1 border-0">
                            <div class="card-body p-1-2 p-sm-2-2 p-md-1 p-lg-1">
                                <div class="row align-items-center">
                                    <div class="col-lg-3 mb-4 mb-lg-0 text-center">


                                        <%String avatar = teacher.getUser().getAvatar(); %>
                                        <%if (avatar.contains("//")) {%>
                                        <img  style="
                                              border-radius: 50%;
                                              width: 180px;
                                              height: 180px;
                                              border: 6px solid #138496;
                                              " src="<%=teacher.getUser().getAvatar()%>" alt="">
                                        <%}else{%>
                                        <img style="margin-right:5px; border-radius: 50%;
                                             width: 180px;
                                             height: 180px;
                                             border: 6px solid #138496;" src="<%=url%>/imageStorage/user/<%=teacher.getUser().getAvatar()%>" alt=""> 
                                        <%}%>


                                    </div>
                                    <div class="col-lg-5 mb-4 mb-lg-0">
                                        <div class="">
                                            <h3 style="margin-bottom: 5px"><%=teacher.getUser().getFullName()%></h3>
                                            <p style="margin: 0px; font-size: 17px; font-weight: 600">
                                                <%=teacher.getPosition()%>
                                            </p>
                                            <p style="margin: 0px; font-size: 14px; font-weight: 600">
                                                <%=teacher.getWorkPlace()%>

                                            </p>
                                            <br />
                                            <div class="d-flex number">
                                                <div style="font-weight: 600">
                                                    <p style="color: #7e7e7e">COURSE</p>
                                                    <p class="text-center"><%=numberCourse%></p>
                                                </div>
                                                <div style="font-weight: 600; margin-left: 3rem">
                                                    <p style="color: #7e7e7e">REGISTERED</p>
                                                    <p class="text-center"><%=numberErnolled%></p>
                                                </div>
                                                <div style="font-weight: 600; margin-left: 3rem">
                                                    <p style="color: #7e7e7e">REATING</p>
                                                    <p class="text-center"><%=ratingAvg%>/5</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 px-xl-10 link">
                                        <div class="d-flex align-items-center">
                                            <i class="fa-solid fa-earth-asia"></i>
                                            <a href="<%=teacher.getPersonalWebsite()%>" target="_blank"
                                               ><%=teacher.getPersonalWebsite()%></a
                                            >
                                        </div>
                                        <div class="d-flex align-items-center mt-1">
                                            <i class="fa-solid fa-envelope"></i>
                                            <a href="<%=teacher.getUser().getEmail()%>"
                                               ><%=teacher.getUser().getEmail()%></a
                                            >
                                        </div>
                                        <div class="d-flex align-items-center mt-1">
                                            <i class="fa-brands fa-facebook"></i>
                                            <a
                                                href="<%=teacher.getFacebook()%>"
                                                target="_blank"
                                                >  <%=teacher.getFacebook()%></a
                                            >
                                        </div>
                                        <div class="d-flex align-items-center mt-1">
                                            <i class="fa-brands fa-linkedin"></i>
                                            <a
                                                href="<%=teacher.getLinkedin()%>"
                                                target="_blank"
                                                ><%=teacher.getLinkedin()%></a
                                            >
                                        </div>
                                        <div class="d-flex align-items-center mt-1">
                                            <i
                                                class="fa-solid fa-user-group"
                                                style="font-size: 12px"
                                                ></i>
                                            <p style="margin: 0px; font-size: 14px; font-weight: 400">
                                                <%        String[] parts = teacher.getDateJoin().toString().split(" ");
                                                %>
                                                Joined on <%=parts[0]%>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="thin" />
                    <div class="col-lg-12 mb-4 mb-sm-5">
                        <div>
                            <span class="section-title text-primary mb-3 mb-sm-4"
                                  >About Me</span
                            >
                            <p>
                                <%=teacher.getAbout()%>
                            </p>
                            <!--                            <p class="mb-0">
                                                            It is a long established fact that a reader will be distracted
                                                            by the readable content of a page when looking at its layout.
                                                            The point of using Lorem Ipsum is that it has a more-or-less
                                                            normal distribution of letters, as opposed.
                                                        </p>-->
                        </div>
                    </div>
                    <hr class="thin" />

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12 mb-4 mb-sm-5">
                                <div>
                                    <span class="section-title text-primary mb-3 mb-sm-4"
                                          >Courses(<%=numberCourse%>)</span
                                    >
                                    <div class="row courseTeacher d-flex">
                                        <%for (Course course : listCourses) {%>
                                        <div class="col-md-3">
                                            <div class="card" style="width: 18rem">
                                                <img
                                                    class="card-img-top"
                                                    src="<%=url%>/imageStorage/course/<%=course.getCourseImg()%>"
                                                    alt="Card image cap"
                                                    />
                                                <div class="card-body">
                                                    <h5 class="card-title"><%=course.getCourseName()%></h5>
                                                    <p class="card-text truncate-text" >
                                                        <%=course.getDescription()%>
                                                    </p>
                                                    <a href="http://localhost:9999/ELearning/course/courseDetail.jsp?courseId=<%=course.getCourseId()%>" class="btn btn-primary">Go To Details</a>
                                                </div>
                                            </div>
                                        </div> 
                                        <%} %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%@include file= "../component/footer.jsp" %>

    </body>
</html>
