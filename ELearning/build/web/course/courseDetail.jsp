<%-- 
    Document   : courseDetail
    Created on : Sep 11, 2023, 12:43:52 AM
    Author     : ADMIN
--%>
<%@page import="model.Course"%>
<%@page import="model.Mooc"%>
<%@page import="model.Lesson"%>
<%@page import="model.Review"%>
<%@page import="model.Problem"%>
<%@page import="model.Certificate"%>

<%@page import="dal.CourseDAO"%>
<%@page import="dal.EnrollDAO"%>
<%@page import="dal.MoocDAO"%>
<%@page import="dal.LessonDAO"%>
<%@page import="dal.ReviewDAO"%>
<%@page import="dal.ProblemDAO"%>
<%@page import="dal.CertificateDao"%>

<%@page import="java.util.ArrayList"%>


<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>


<%int totalLesson=0;%>


<%
             int courseID = Integer.parseInt(request.getParameter("courseId"));
                    CourseDAO courseDao=null;
            courseDao = new CourseDAO();
            Course course=new Course();
            course = courseDao.selectById(courseID);
           
            EnrollDAO enrollDao = new EnrollDAO();
                                            int total= enrollDao.totalParticipationInCourse(courseID);                    
           ArrayList<Mooc> listMooc = new ArrayList<Mooc>();
                MoocDAO moocDao= new MoocDAO();
                listMooc = moocDao.getMoocByCourseId(courseID);
                    
            

                
                
                ArrayList<Review> listReview = new ArrayList<Review>();
                ReviewDAO reviewDao= new ReviewDAO();
                listReview = reviewDao.selectReviewByCourseId(courseID);

                Lesson fristLesson = new LessonDAO().selectFirstLesson(courseID);
                
//                User user1 = (User) session.getAttribute("acc");
//                EnrollDAO dao2 = new EnrollDAO();
//                boolean c = dao2.checkCompleteCourse(courseID, user1.getUserId());
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <link rel="icon" href="<%=url%>/homepage/img/tab_icon.png" type="image/x-icon">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Course Detail</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9"
            crossorigin="anonymous"
            />
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
            integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa"
            crossorigin="anonymous"
        ></script>

        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
            integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"
            />

        <link rel="stylesheet" href="../asset/CSS/courseDetail.css"/>
        <link rel="stylesheet" href="../asset/CSS/starRating.css"/>
        <link rel="stylesheet" href="../asset/CSS/avatar.css"/>
        <link rel="stylesheet" href="../asset/CSS/style.css"/>
        <script>
            var a = 0; // Khai báo biến JavaScript 'a' và gán giá trị ban đầu là 0
        </script>
        <script>

            window.addEventListener("load", function () {
                var myData = {courseId: courseID, action: "check"};
                var enrollButton = document.getElementById("button-enroll");
                $.ajax({
                    type: "POST",
                    url: "http://localhost:9999/ELearning/EnrollServlet",
                    data: myData,
                    success: function (response) {
                        if (response === "not enroll") {

                        } else if (response === "enrolled") {
                            enrollButton.textContent = "Go to course!";
                        }
                    },
                    error: function (xhr, status, error) {
                        console.error("Lỗi:", error);
                    }
                });
            });
        </script>
        <style>
            .popup-menu {
                display: none;
                background-color: #f4f4f4;
                border-radius: 8px;
            }
            .popup-menu-hiding ul{
                list-style: none;
            }
            .popup-menu-hiding {

                display: flex;
                flex-direction: column;
                align-items: center;
                width: 200px;
                z-index: 10;
            }
            .popup-menu-item:hover{
                background-color: #dbdbdb;
                cursor: pointer;
                transition: 1s;
                border-radius: 8px
            }
            .popup-menu-item {
                padding: 16px;
                width: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .report-review-reserve{
                display: none;
            }
            .report-review-hiding{
                display: block;
                position: fixed;
                top: 0;
                background-color: rgba(180, 180, 180, 0.5);
                width: 100%;
                z-index: 100;
            }
            .card-body textarea:focus-visible {
                outline: none
            }
            .review-reserve{
                display: none;
            }
            .review-hiding{
                display: block;
                position: fixed;
                top: 0;
                background-color: rgba(180, 180, 180, 0.5);
                width: 100%;
                z-index: 100;
            }
            .delete-reserve{
                display: none;
            }
            .delete-hiding{
                display: block;
                position: fixed;
                top: 0;
                background-color: rgba(180, 180, 180, 0.5);
                width: 100%;
                z-index: 100;
            }

        </style>
    </head>
    <body>  
        <%@include file= "../component/header.jsp" %>
        <div class="row p-5" style="padding-bottom: 0rem !important;padding-top: 2rem !important; margin-top:0px !important">
            <div class="col-md-8">
                <div class="card border-0">
                    <div class="card-body" style="padding-bottom: 0rem !important">
                        <h1><%=course.getCourseName()%></h1>
                        <div>
                            <span
                                style="
                                display: inline-block;
                                vertical-align: middle;
                                font-size: 17px;
                                "
                                ><i class="fa-solid fa-graduation-cap"></i
                                ></span>
                            <h5
                                style="
                                display: inline-block;
                                vertical-align: middle;
                                margin-bottom: 0;
                                "
                                >
                                Teacher:
                            </h5>

                            <span style="display: inline-block; vertical-align: middle"
                                  ><a href="http://localhost:9999/ELearning/teacherProfile/teacherProfile.jsp?teacherId=<%=course.getUserId().getUserId()%>" style="color: blue; margin-bottom: 0; cursor: pointer; font-size: 20px; font-weight: 500">
                                    <%=course.getUserId().getFullName()%>
                                </a></span
                            >  

                        </div>

                        <div class="rating">
                            <!--                            <i class="fas fa-star star-rating"></i>
                                                        <i class="fas fa-star star-rating"></i>
                                                        <i class="fas fa-star star-rating"></i>
                                                        <i class="fas fa-star star-rating"></i>
                                                        <i class="far fa-star star-rating"></i>-->
                            <i data-star=""></i>

                            <div class="ratings-info">

                                <p style="margin-bottom: 0; font-size: 14px">
                                    <span id="avgStar" style="font-weight: 600"></span> Star - <span style="font-weight: 600"><%=listReview.size()%> </span> Reviews
                                </p>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div
                            style="
                            position: relative;
                            padding-bottom: 56.25%;
                            height: 0;
                            overflow: hidden;
                            "
                            >
                            <iframe
                                class="text-center"
                                src="https://www.youtube.com/embed/<%=fristLesson.getLessonUrl()%>"
                                title="YouTube video player"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowfullscreen
                                style="
                                position: absolute;
                                top: 0;
                                left: 0;
                                width: 85%;
                                height: 85%;
                                "
                                ></iframe>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card border-0">
                    <div class="card-body custom-card-body">
                        <img
                            src="<%=url%>/imageStorage/course/<%=course.getCourseImg()%>"
                            style="width: 80%; border: 2px solid ; border-radius: 10px"
                            />
                        <h3 class="mt-3 animate-charcter" style="color: red; font-weight: bold">free course</h3><br>
                        <button name="enroll-button" id="button-enroll" onclick="clickEnroll()" type="button" class="btn btn-primary btn-lg">Enroll Now</button>

                        <c:if test="${not empty acc}">
                            <%                
                                User user1 = (User) session.getAttribute("acc");
                                 EnrollDAO dao2 = new EnrollDAO();
                                 boolean c = dao2.checkCompleteCourse(courseID, user1.getUserId());
                            %>                  
                            <%if(c){%>
                            <%
                                                User user2 = (User) session.getAttribute("acc");

                                    Certificate certi = new CertificateDao().getCertificateByUserCourseId(user2.getUserId(),courseID);
                            %>
                            <a href="http://localhost:9999/ELearning/certificatedeatil?id=<%=certi.getCertificateId()%>" target="_blank" name="enroll-button" id="button-enroll" type="button" class="btn btn-primary btn-lg">View Certificate</a>
                            <%}%>
                        </c:if>

                        <!-- Nút lớn -->
                        <p class="mt-4">
                            <i class="fa-solid fa-users" style="margin-right: 0.6rem"></i> Quantity participated: 
                            <span style="font-weight: 700"><%=total%></span>
                        </p>
                        <p>
                            <i class="fa-solid fa-layer-group" style="margin-right: 1rem"></i
                            >Category: <span style="font-weight: 700"> <%=course.getCategoryId().getCategoryName()%></span>

                        </p>
                        <p>
                            <i class="fa-solid fa-film" style="margin-right: 1rem"></i>Total number of lessons:
                            <span style="font-weight: 700" id="totalLesson">0</span>

                        </p>
                        <p>
                            <i class="fa-solid fa-clock" style="margin-right: 1rem"></i>Duration:
                            <span style="font-weight: 700"> <%=course.getDuration()%></span> hour
                        </p>
                        <p>
                            <i class="fa-regular fa-calendar-days" style="margin-right: 1rem"></i> Publish Date:
                            <span style="font-weight: 700" id="totalLesson"><%=course.getPublish()%></span>
                        </p>
                        <p>
                            <i class="fa-solid fa-street-view" style="margin-right: 1rem"></i
                            >Study every time and everywhere
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <!-- Tab items -->
            <div class="tabs">
                <div class="tab-item active">
                    <i class="tab-icon fa-solid fa-circle-info"></i>
                    Description
                </div>
                <div class="tab-item">
                    <i class="tab-icon fa-solid fa-book-open"></i>

                    Content
                </div>
                <div class="tab-item">
                    <i class="tab-icon fa-regular fa-star-half-stroke"></i> Reviews
                </div>
            </div>

            <!-- Tab content -->
            <div class="tab-content">
                <div class="tab-pane active">
                    <div style="text-align: center; margin-bottom: 2rem">
                        <span
                            style="
                            font-size: 1.3rem;
                            color: #252525;
                            margin-bottom: 10px;
                            letter-spacing: 2px;
                            text-transform: uppercase;
                            "
                            >Description</span
                        >
                    </div>
                    <p style="width: 80%; margin:auto;  text-align: justify;text-justify: inter-word;">
                        <%=course.getDescription()%>
                    </p>
                </div>
                <div class="tab-pane">
                    <div style="text-align: center; margin-bottom: 2rem">
                        <span
                            style="
                            font-size: 1.3rem;
                            color: #252525;
                            margin-bottom: 10px;
                            letter-spacing: 2px;
                            text-transform: uppercase;
                            "
                            >Content</span
                        >
                    </div>
                    <ul class="accordion">
                        <%for (Mooc mooc : listMooc) {%>
                        <li>
                            <input type="checkbox" name="accordion" id="<%=mooc.getMoocNumber()%>" />
                            <label for="<%=mooc.getMoocNumber()%>"> Mooc <%=mooc.getMoocNumber()%>: &nbsp;    <%=mooc.getMoocName()%></label>
                            <div class="content">
                                <%
                                ArrayList<Lesson> listLesson = new ArrayList<Lesson>();
                                LessonDAO lessonDao= new LessonDAO();
                                int moocid=mooc.getMoocId();
                                listLesson=lessonDao.selectLessonByMoocId(moocid);
                                %>

                                <%for (Lesson lesson : listLesson) {%>
                                <p>
                                    <i
                                        class="fa-solid fa-circle-play"
                                        style="margin-right: 1rem"
                                        ></i
                                    >Lesson <%=lesson.getLessonNumber()%>:&nbsp;  <%=lesson.getLessonName()%>
                                </p>
                                <hr />

                                <%
                                    totalLesson++;}           
                                %>
                            </div>
                        </li> 
                        <%
                           }
                        %>

                    </ul>
                </div>

                <div class="tab-pane">
                    <section id="testimonials">
                        <div style="text-align: center; margin-bottom: 2rem">
                            <span
                                style="
                                font-size: 1.3rem;
                                color: #252525;
                                margin-bottom: 10px;
                                letter-spacing: 2px;
                                text-transform: uppercase;
                                "
                                >Comments And Reviews</span>
                        </div>
                        <!-- Review course start -->
                        <c:if test="${not empty acc}">
                            <%if(new EnrollDAO().checkPassCourse(user.getUserId(),courseID) && !(new ReviewDAO().checkReview(user.getUserId(),courseID))){%>
                            <button name="review-button" id="review-button" onclick="clickReview()" type="button" class="btn btn-primary btn-lg">Review course</button>
                            <%}%>
                        </c:if>



                        <div class="review-reserve">
                            <div class="review-hiding" >
                                <div class="d-flex align-items-center justify-content-center" style="min-height: 100vh">
                                    <div class="col-12 col-sm-4" style="border-radius: 8px;">
                                        <div class="card">
                                            <div class="d-flex align-items-center justify-content-between card-headertext-white"
                                                 style="background-color: #06BBCC; padding:16px">
                                                <div style="color: #FFFFFF; font-size: 24px; font-weight: 700">Review</div>
                                                <div style="cursor: pointer" onclick="clickReview()">
                                                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0,0,256,256">
                                                    <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(8.53333,8.53333)"><path d="M7,4c-0.25587,0 -0.51203,0.09747 -0.70703,0.29297l-2,2c-0.391,0.391 -0.391,1.02406 0,1.41406l7.29297,7.29297l-7.29297,7.29297c-0.391,0.391 -0.391,1.02406 0,1.41406l2,2c0.391,0.391 1.02406,0.391 1.41406,0l7.29297,-7.29297l7.29297,7.29297c0.39,0.391 1.02406,0.391 1.41406,0l2,-2c0.391,-0.391 0.391,-1.02406 0,-1.41406l-7.29297,-7.29297l7.29297,-7.29297c0.391,-0.39 0.391,-1.02406 0,-1.41406l-2,-2c-0.391,-0.391 -1.02406,-0.391 -1.41406,0l-7.29297,7.29297l-7.29297,-7.29297c-0.1955,-0.1955 -0.45116,-0.29297 -0.70703,-0.29297z"></path></g></g>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="card-body" style="background-color:#f4f4f4; padding: 16px">
                                                <form action="../AddReview" method="GET">
                                                    Your vote:      
                                                    <!-- vote -->
                                                    <div class="vote-rating" style="height: 40px">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                                            <!--                                                            <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>-->
                                                            <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                                            <!--                                                            <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>-->
                                                            <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                                            <!--                                                            <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>-->
                                                            <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                                            <!--                                                            <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>-->
                                                            <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                                            <!--                                                            <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>-->
                                                        </fieldset>
                                                    </div>
                                                    <!--vote-->
                                                    </br>
                                                    Comment:
                                                    <br>
                                                    <textarea placeholder="Your comment..." name="comment" rows="3" cols="30"style="width: 100%; padding: 16px; border: 1px solid #06BBCC;"></textarea>
                                                    <input type="hidden" name="courseId" id="course-id-hiden" value="" />

                                                    <div class="d-flex align-items-center justify-content-center mt-2">
                                                        <button type="submit" class="btn btn-primary" style="font-size: 18px">
                                                            Send review
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Review course end -->


                        <%int totalStar=0;%>
                        <%for (Review review : listReview) {%>
                        <div class="testimonials-box-container" data-value ="<%=review.getReviewId()%>">
                            <div class="testimonials-box">
                                <div class="box-top">
                                    <div class="profile">
                                        <div class="profile-img">
                                            <%String avatar = review.getUserId().getAvatar(); %>
                                            <%if (avatar.contains("//")) {%>
                                            <img src="<%=review.getUserId().getAvatar()%>" alt=""><%=review.getUserId().getFullName()%>
                                            <%}else{%>
                                            <img style="margin-right:5px" src="<%=url%>/imageStorage/user/<%=review.getUserId().getAvatar()%>" alt=""> <%=review.getUserId().getFullName()%>
                                            <%}%>

                                        </div>
                                        <div class="name-user">
                                            <strong><%=review.getUserId().getFullName()%></strong>
                                            <span><%=review.getTime()%></span>
                                        </div>
                                    </div>
                                    <div style="display:flex; justify-content: flex-end">
                                        <%
                                        int star = review.getRating(); 
                                        totalStar+=star;
                                        %>

                                        <div class="review">
                                            <%for (int i = 0; i < star; i++) {%>
                                            <i class="fas fa-star"></i>
                                            <%
                                             }
                                            %>
                                            <%for (int i = star; i < 5; i++) {%>
                                            <i class="far fa-star"></i>

                                            <%                 
                                            }
                                            %>
                                        </div>
                                    </div>
                                </div>

                                <div class="clinet-comment" style="display: flex; justify-content: space-between; align-items: center">
                                    <div>
                                        <span><%=review.getReviewContent()%></span>
                                    </div>

                                    <div style="position: relative">
                                        <i style="padding: 5px 7px" class="fa-solid fa-ellipsis-vertical" onclick="menuReview(<%=review.getReviewId()%>)"></i>
                                        <div class="popup-menu" style="position: absolute" id="popup<%=review.getReviewId()%>">
                                            <div class="popup-menu-hiding">
                                                <c:if test="${not empty acc}">
                                                    <%if(user.getRoleid().getRoleId() == 2 || user.getRoleid().getRoleId() == 3){%>
                                                    <div class="popup-menu-item" onclick="reportReview(<%=review.getReviewId()%>)"> Report review </div>
                                                    <div class="popup-menu-item" onclick="deleteReview(<%=review.getReviewId()%>)"> Delete review</div>
                                                    <%}else{%>
                                                    <div class="popup-menu-item" onclick="reportReview(<%=review.getReviewId()%>)"> Report review </div>
                                                    <%}%>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%}%>
                    </section>
                </div>
                <!--                    report review start-->
                <div class="report-review-reserve">
                    <div class="report-review-hiding" >
                        <div class="d-flex align-items-center justify-content-center" style="min-height: 100vh">
                            <div class="col-12 col-sm-4" style="border-radius: 8px;">
                                <div class="card">
                                    <div class="d-flex align-items-center justify-content-between card-headertext-white"
                                         style="background-color: #06BBCC; padding:16px">
                                        <div style="color: #FFFFFF; font-size: 24px; font-weight: 700">Report review</div>
                                        <div style="cursor: pointer" onclick="reportReview()">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0,0,256,256">
                                            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(8.53333,8.53333)"><path d="M7,4c-0.25587,0 -0.51203,0.09747 -0.70703,0.29297l-2,2c-0.391,0.391 -0.391,1.02406 0,1.41406l7.29297,7.29297l-7.29297,7.29297c-0.391,0.391 -0.391,1.02406 0,1.41406l2,2c0.391,0.391 1.02406,0.391 1.41406,0l7.29297,-7.29297l7.29297,7.29297c0.39,0.391 1.02406,0.391 1.41406,0l2,-2c0.391,-0.391 0.391,-1.02406 0,-1.41406l-7.29297,-7.29297l7.29297,-7.29297c0.391,-0.39 0.391,-1.02406 0,-1.41406l-2,-2c-0.391,-0.391 -1.02406,-0.391 -1.41406,0l-7.29297,7.29297l-7.29297,-7.29297c-0.1955,-0.1955 -0.45116,-0.29297 -0.70703,-0.29297z"></path></g></g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="card-body" style="background-color:#f4f4f4; padding: 16px">
                                        <form action="../ReportReview" method="POST">
                                            Choose problem: 
                                            <%
                                            ArrayList<Problem> listProblem = new ArrayList<Problem>();
                                            ProblemDAO daoProblem = new ProblemDAO();
                                            listProblem = daoProblem.selectAll();
                                            %>
                                            <div>                                        
                                                <select name="problem" style="width: 100%; padding: 16px; border: 1px solid #06BBCC">
                                                    <%
                                                    for(Problem problem : listProblem){         
                                                    %>
                                                    <option value="<%=problem.getProblemId()%>"><%=problem.getProblem()%></option>  
                                                    <%}%>        
                                                </select>
                                            </div>
                                            <br>
                                            Reason:
                                            <br>
                                            <textarea placeholder="Your reason..." name="reason" rows="3" cols="30"style="width: 100%; padding: 16px; border: 1px solid #06BBCC;"></textarea>
                                            <input type="hidden" name="reviewId" id="review-hiden" value="" />
                                            <input type="hidden" name="courseId" id="course-hiden" value="" />
                                            <div class="d-flex align-items-center justify-content-center mt-2">
                                                <button type="submit" class="btn btn-primary" style="font-size: 18px">
                                                    Send report
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Delete review start -->
                <div class="delete-reserve">
                    <div class="delete-hiding" >
                        <div class="d-flex align-items-center justify-content-center" style="min-height: 100vh">
                            <div class="col-12 col-sm-4" style="border-radius: 8px;">
                                <div class="card">
                                    <div class="d-flex align-items-center justify-content-between card-headertext-white"
                                         style="background-color: #06BBCC; padding:16px">
                                        <div style="color: #FFFFFF; font-size: 24px; font-weight: 700">Delete review</div>
                                        <div style="cursor: pointer" onclick="deleteReview()">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0,0,256,256">
                                            <g fill="#ffffff" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(8.53333,8.53333)"><path d="M7,4c-0.25587,0 -0.51203,0.09747 -0.70703,0.29297l-2,2c-0.391,0.391 -0.391,1.02406 0,1.41406l7.29297,7.29297l-7.29297,7.29297c-0.391,0.391 -0.391,1.02406 0,1.41406l2,2c0.391,0.391 1.02406,0.391 1.41406,0l7.29297,-7.29297l7.29297,7.29297c0.39,0.391 1.02406,0.391 1.41406,0l2,-2c0.391,-0.391 0.391,-1.02406 0,-1.41406l-7.29297,-7.29297l7.29297,-7.29297c0.391,-0.39 0.391,-1.02406 0,-1.41406l-2,-2c-0.391,-0.391 -1.02406,-0.391 -1.41406,0l-7.29297,7.29297l-7.29297,-7.29297c-0.1955,-0.1955 -0.45116,-0.29297 -0.70703,-0.29297z"></path></g></g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="card-body" style="background-color:#f4f4f4; padding: 16px">
                                        <div>Do you want to delete this review?</div>  

                                        <form action="../DeleteReview" method="POST"> 
                                            <input type="hidden" name="reviewId" id="review-hiden-id" value="" />
                                            <input type="hidden" name="courseId" id="course-hiden-id" value="" />
                                            <div class="d-flex align-items-center justify-content-center mt-2">
                                                <div>
                                                    <button type="submit" class="btn btn-primary" style="font-size: 18px">
                                                        Yes
                                                    </button> 
                                                    <button type="button" onclick="deleteReview()" class="btn btn-secondary" style="font-size: 18px">
                                                        No
                                                    </button> 

                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Delete review end -->


                <!--                Handle click outside-->
                <!--                <div class="flyout" id="flyout-example" style="border: 1px solid red">
                                    <h5 class="flyout-title">This could be a flyout;</h5>
                                    <div class="flyout-debug" id="flyout-debug"></div>
                                    <div class="flyout-buttons">
                                        <button class="button button-outline" type="button">Cancel</button>
                                        <button class="button" type="button">Ok</button>
                                    </div>
                                </div>-->
                <!--...-->
                <!--                    report review end-->
            </div>
        </div>
        <%@include file= "../component/footer.jsp" %>   
        <script>
            const tabs = document.querySelectorAll(".tab-item");
            const all_content = document.querySelectorAll(".tab-pane");
            tabs.forEach((tab, index) => {
                tab.addEventListener('click', (e) => {
                    tabs.forEach(tab => {
                        tab.classList.remove('active')
                    });
                    tab.classList.add('active');
                    all_content.forEach(content => {
                        content.classList.remove('active')
                    });
                    all_content[index].classList.add('active');
                });
            });
            //  const tabs = b(".tab-item");
            // const panes = b(".tab-pane");

//            const tabActive = a(".tab-item.active");
//
//            tabs.forEach((tab, index) => {
//                const pane = panes[index];
//
//                tabs.onclick = function () {
//                    tabs(".tab-item.active").classList.remove("active");
//                    tabs(".tab-pane.active").classList.remove("active");
//
//                    this.classList.add("active");
//                    all_content.classList.add("active");
//                };
//            });

        </script>
        <script>
            totalLesson = <%=totalLesson%>;
            avgStar = (<%=totalStar%> /<%=listReview.size()%>).toFixed(1);
            document.getElementById("totalLesson").innerText = totalLesson;
            document.getElementById("avgStar").innerText = avgStar;
            const starElement = document.querySelector('i[data-star=""]');
            starElement.setAttribute('data-star', avgStar);
        </script>
        <script>
            var courseID = <%=courseID%>;
            var enrollButton = document.getElementById("button-enroll");
            function clickEnroll() {
                var myData = {courseId: courseID, action: "add"};
                $.ajax({
                    type: "POST",
                    url: "http://localhost:9999/ELearning/EnrollServlet",
                    data: myData,
                    success: function (response) {
                        if (response === "acc null") {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'You must be logged in',
                                confirmButtonText: 'Login',
                                showCancelButton: true,
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    // Nếu người dùng nhấn nút OK, chuyển hướng đến trang khác
                                    window.location.href = 'http://localhost:9999/ELearning/login'; // Thay thế URL 'http://example.com' bằng URL bạn muốn chuyển hướng.
                                }
                            });
                        } else if (response === "not enroll") {
                            Swal.fire({
                                title: 'Successfully!!',
                                text: 'Congratulations, you have successfully enrolled for the course!',
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'OK'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    // Nếu người dùng nhấn nút OK, chuyển hướng đến trang khác
                                    window.location.href = 'http://localhost:9999/ELearning/learning/hi.jsp?courseId=' + courseID; // Thay thế URL 'http://example.com' bằng URL bạn muốn chuyển hướng.
                                }
                            });
                        } else if (response === "enrolled") {
                            enrollButton.textContent = "Go to course";
                            window.location.href = "http://localhost:9999/ELearning/learning/hi.jsp?courseId=" + courseID;
                        }
                    },
                    error: function (xhr, status, error) {
                        console.error("Lỗi:", error);
                    }
                });
            }

        </script>
        <script>
            function  reportReview(id) {
                var modal = document.querySelector(".report-review-reserve");
                modal.classList.toggle("report-review-hiding");
                var reviewHiden = document.getElementById("review-hiden");
                reviewHiden.value = id;
                var courseHiden = document.getElementById("course-hiden");
                courseHiden.value = <%=courseID%>;
            }

            function menuReview(reviewID) {
                var modal = document.getElementById("popup" + reviewID);
                modal.classList.toggle("popup-menu-hiding");
            }

            function clickReview() {
                var modal = document.querySelector(".review-reserve");
                modal.classList.toggle("review-hiding");
                var courseHiden = document.getElementById("course-id-hiden");
                courseHiden.value = <%=courseID%>;

            }

            function deleteReview(id) {
                var modal = document.querySelector(".delete-reserve");
                modal.classList.toggle("delete-hiding");
                var reviewHiden = document.getElementById("review-hiden-id");
                reviewHiden.value = id;
                var courseHiden = document.getElementById("course-hiden-id");
                courseHiden.value = <%=courseID%>;
                console.log(reviewHiden.value + "   " + courseHiden.value);
            }


//            Handle abcxyz
//            document.addEventListener("click", (evt) => {
//                const flyoutEl = document.getElementById("popup-menu-hiding");
//                let targetEl = evt.target; // clicked element      
//                do {
//                    if (targetEl == flyoutEl) {
//                        // This is a click inside, does nothing, just return.
//                        document.getElementById("flyout-debug").textContent = "Clicked inside!";
//                        return;
//                    }
//                    // Go up the DOM
//                    targetEl = targetEl.parentNode;
//                } while (targetEl);
//                // This is a click outside.      
//                document.getElementById("flyout-debug").textContent = "Clicked outside!";
//            });
        </script>
    </body>
</html>
