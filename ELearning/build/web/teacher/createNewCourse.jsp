
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<%@page import="model.Course"%>
<%@page import="model.User"%>
<%@page import="java.util.ArrayList"%>

<%@page import="dal.CourseDAO"%>
<%@page import="dal.EnrollDAO"%>
<%@page import="dal.UserDAO"%>
<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>About-Courses</title>
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/framework.css" />
        <link rel="stylesheet" href="css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />
        <style>
            /* Thay đổi màu nền và đường viền của biểu mẫu */
            form {
                background-color: #f5f5f5;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 5px;
            }

            /* Thiết lập kiểu cho nhãn và trường nhập */
            .form-group {
                margin: 10px 0;
            }

            label {
                display: block;
                font-weight: bold;
            }

            input[type="text"],
            input[type="file"],
            textarea {
                width: 100%;
                padding: 10px;
                margin-top: 5px;
                border: 1px solid #ccc;
                border-radius: 3px;
            }

            /* Thiết lập kiểu cho nút gửi */
            button[type="submit"] {
                background-color: #007BFF;
                color: #fff;
                padding: 10px 20px;
                border: none;
                border-radius: 3px;
                cursor: pointer;
            }

            /* Thêm kiểu đẹp cho nút gửi khi rê chuột vào nó */
            button[type="submit"]:hover {
                background-color: #0056b3;
            }
            /* Thiết lập kiểu cho thẻ select */
            select {
                width: 100%;
                padding: 10px;
                margin-top: 5px;
                border: 1px solid #ccc;
                border-radius: 3px;
                background-color: #fff;
                font-size: 14px;
            }

            /* Tùy chỉnh kiểu cho mũi tên xuống của select */
            select::-ms-expand {
                display: none;
            }

            /* Tùy chỉnh kiểu khi chọn một tùy chọn */
            select option {
                background-color: #fff;
                color: #333;
            }

            /* Tùy chỉnh kiểu khi select được focus */
            select:focus {
                border-color: #007BFF;
                outline: none;
            }

        </style>

    </head>
    <body>
        <div class="page d-flex">
            <%@include file="sideBar.jsp" %>
            <div class="content w-full">
                <!-- Header start-->
                <%@include file= "../component/header.jsp" %>
                <!-- Header End -->
                <h1 class="p-relative">Create-Course</h1>
                <div class="wrapper d-grid gap-20">


                </div>
                <!-- Start Projects Table -->
                <div class="projects p-20 bg-white rad-10 m-20">
                    <h2 class="mt-0 mb-20"></h2>
                    <div class="responsive-table">


                        <form action="add-courses" method="post" enctype="multipart/form-data" >

                            <div class="form-group">
                                <label for="category">Category:</label>
                                <select id="category" name="category" required>
                                    <c:forEach items="${listCategory}" var="listc">
                                        <option value="${listc.getCategoryId()}">${listc.getCategoryName()}</option>
                                    </c:forEach>
                                    
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="courseImg">Course Image:</label>
<!--                                <input type="file" id="courseImg" name="courseImg" accept="image/*" required>-->
                                <input type="file" id="courseImg" name="courseImg"  required>
                            </div>

                            <div class="form-group">
                                <label for="courseName">Course Name:</label>
                                <input type="text" id="courseName" name="courseName" required>
                            </div>

                            <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea id="description" name="description" required></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit">Create Course</button>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- End Projects Table -->
            </div>
        </div>
    </body>
</html>
