
<%@page import="model.Category"%>
<%@page import="dal.CategoryDAO"%>
<%@page import= "java.util.Comparator" %>
<%@page import= "java.util.Collections" %>

<%
    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>All Course</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicon -->
        <link href="<%=url%>img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;500;600&family=Nunito:wght@600;700;800&display=swap" rel="stylesheet">

        <!-- Icon Font Stylesheet -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Libraries Stylesheet -->
        <link href="<%=url%>/homepage/lib/animate/animate.min.css" rel="stylesheet">

        <link href="<%=url%>/homepage/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <link href="<%=url%>/homepage/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/all.min.css" />
        <link rel="stylesheet" href="css/framework.css" />
        <link rel="stylesheet" href="css/master.css" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;500&display=swap" rel="stylesheet" />

        <!-- Template Stylesheet -->

        <style>

            .navbar-search-wrap {
                position: relative;
            }
            .navbar-search-icon {
                position: absolute;
                left: 8px;
                top: 8px;
                cursor: pointer;
            }
            .navbar-search-wrap input {
                border: 1px solid #06bbcc;
                border-radius: 8px;
                padding: 8px 16px 8px 32px;
                min-width: 360px;
            }
            .navbar-search-wrap input::placeholder {
                color: #06bbcc;
            }
            .navbar-search-wrap input:focus {
                outline: #06bbcc;
                color: #06bbcc;
            }
            @media all and (max-width: 991px) {
                .navbar-search-wrap {
                    display: none !important;
                }
            }

            .main {
                background: #f5f7f8;
            }

            .custom-checkbox {
                transform: scale(1.5); /* Adjust the scale factor to change the size */
                margin: 0 1rem;
            }

            /* Change the color of the checkmark to blue-white */
            .custom-checkbox + label::before {
                border: 2px solid #007bff; /* Blue border for the checkbox */
                background-color: #fff; /* White background for the checkbox */
            }

            .card-container {
                background: #fff;
                border: 1px solid lightgrey;
                border-radius: 8px;
                width: 30.33333% !important;
                margin: 10px 5px;
                cursor: pointer;
            }

            .card-container:hover {
                transform: scale(1.05);
                box-shadow: 0 0 11px rgba(33,33,33,.2);
                transition-duration: 0.5s;
            }

            .card-img {
                aspect-ratio: 16/9;
                margin-bottom: 8px;
                overflow: hidden;
                border-bottom-right-radius: 8px !important;
                border-bottom-left-radius: 8px !important;
            }

            img {
                margin-top: 8px;
                border-radius: 8px; /* Apply border-radius to all corners of the image */
                width: 100%;
                height: 100%;
                object-fit: cover;
            }

            .card-icon-container {
                align-items: center;
                gap: 4px;
                margin: 1rem 0;
            }

            .card-icon {
                width: 24px;
                height: 24px;
                border: 1px solid lightgray;
                padding: 2px;
                user-select: none;
                box-sizing: content-box;
            }

            .text-eclipse-1 {
                display: -webkit-box;
                -webkit-line-clamp: 1;
                -webkit-box-orient: vertical;
                overflow: hidden;
            }

            .text-eclipse-3 {
                display: -webkit-box;
                -webkit-line-clamp: 3;
                -webkit-box-orient: vertical;
                overflow: hidden;
            }

            .card-bottom {
                display: flex;
                -webkit-flex-direction: column;
                -ms-flex-direction: column;
                flex-direction: column;
                gap: 4px;
                -webkit-flex: 1;
                -ms-flex: 1;
                flex: 1;
                -webkit-box-pack: end;
                -ms-flex-pack: end;
                -webkit-justify-content: end;
                justify-content: end;
                padding: 1rem 0;
            }

            .card-bottom-review {
                align-items: initial;
                margin-bottom: 4px;
            }

            .description-text{
                font-size: 13px
            }

            a {
                text-decoration: none !important;
            }
            .content a{
                color: black !important;

            }

            a:hover{
                color: none !important;
            }
        </style>
    </head>
    <body>
        <div class="page d-flex">

            <%@include file="sideBar.jsp" %>
            <div class="content w-full">



                <!-- Header start-->
                <%@include file= "../component/header.jsp" %>
                <!-- Header End -->


                <!-- Main start -->
                <div class="main">



                    <div class="container d-flex col-12">


                        <div class="col-12">
                            <h4 class="mt-3">All course</h4>
                            <div class="row col-12">

                                <!-- test course list start -->
                                <div class="container-xxl py-5">
                                    <div class="container">
                                        <div class="row g-4 justify-content-start" id="course-list-container">  
                                           
                                            <c:forEach items="${listCourseById}" var="list">


                                                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                                                    <div class="course-item bg-light">
                                                        <div class="position-relative overflow-hidden">

                                                            <a href="about-courses?courseID=${list.getCourseId()}">

                                                                <img class="img-course img-fluid" src="<%=url%>/imageStorage/course/${list.getCourseImg()}" alt="" style="width: 100%; height: 200px">
                                                            </a>

                                                        </div>
                                                        <div class="text-center p-4 pb-0 ">
                                                            <div class="d-flex justify-content-around">
                                                                <h3 class="mb-0 text-danger">Free</h3>
                                                                <div class="mb-3">
                                                                    <small class="fa fa-star text-primary"></small>
                                                                    <small class="fa fa-star text-primary"></small>
                                                                    <small class="fa fa-star text-primary"></small>
                                                                    <small class="fa fa-star text-primary"></small>
                                                                    <small class="fa fa-star text-primary"></small>
                                                                    <small>(123)</small>
                                                                </div>  
                                                            </div>

                                                            <h5 class="mb-4 mt-3 course-name">${list.getCourseName()}</h5>
                                                            </a>
                                                            <p>Publish Date: ${list.getPublish()}</p>
                                                            <p>Category: ${list.getCategoryId().getCategoryName()}</p>
                                                        </div>
                                                        <div class="d-flex border-top">
                                                            <small class="flex-fill text-center border-end py-2"><i class="fa fa-user-tie text-primary me-2"></i>name</small>
                                                            <small class="flex-fill text-center border-end py-2"><i class="fa fa-clock text-primary me-2"></i> Hrs</small>

                                                            <small class="flex-fill text-center py-2"><i class="fa fa-user text-primary me-2"></i>Students</small>
                                                        </div>
                                                    </div>
                                                </div>

                                            </c:forEach>

                                            <!-- Page number start -->
                                            <div class="number-page">

                                                <!-- Page number end -->
                                            </div>
                                            <!-- test course list end -->
                                            <!-- comment -->


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main end --> 






        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>


        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="<%=url%>/homepage/lib/wow/wow.min.js"></script>
        <script src="<%=url%>/homepage/lib/easing/easing.min.js"></script>
        <script src="<%=url%>/homepage/lib/waypoints/waypoints.min.js"></script>
        <script src="<%=url%>/homepage/lib/owlcarousel/owl.carousel.min.js"></script>

        <!-- Template Javascript -->
        <script src="<%=url%>/homepage/js/main.js"></script>

    </body>
</html>
