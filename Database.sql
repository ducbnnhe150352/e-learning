USE [ELearning2]
GO
/****** Object:  FullTextCatalog [CourseSFT]    Script Date: 11/7/2023 9:20:47 PM ******/
CREATE FULLTEXT CATALOG [CourseSFT] WITH ACCENT_SENSITIVITY = ON
GO
/****** Object:  Table [dbo].[Category]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(0,1) NOT NULL,
	[CategoryName] [nvarchar](100) NOT NULL,
 CONSTRAINT [Category_PK] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificate](
	[CertificateID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Satus] [nvarchar](100) NULL,
 CONSTRAINT [Certificate_PK] PRIMARY KEY CLUSTERED 
(
	[CertificateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Choice]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Choice](
	[ChoiceID] [int] NOT NULL,
	[QuizID] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[IsTrue] [bit] NOT NULL,
 CONSTRAINT [Choice_PK] PRIMARY KEY CLUSTERED 
(
	[ChoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Choices]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Choices](
	[ChoiceID] [int] IDENTITY(0,1) NOT NULL,
	[QuizID] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[IsTrue] [bit] NOT NULL,
 CONSTRAINT [Choices_PK] PRIMARY KEY CLUSTERED 
(
	[ChoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[CommentID] [int] IDENTITY(0,1) NOT NULL,
	[LessonID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[Like] [int] NOT NULL,
	[Dislike] [int] NOT NULL,
	[IsReply] [bit] NOT NULL,
	[Image] [nvarchar](100) NULL,
	[Status] [bit] NOT NULL,
	[CommentDate] [datetime] NULL,
 CONSTRAINT [Comment_PK] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[CourseID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NULL,
	[CategoryID] [int] NOT NULL,
	[CourseImg] [nvarchar](500) NULL,
	[CourseName] [nvarchar](300) NOT NULL,
	[Publish] [date] NOT NULL,
	[Duration] [float] NULL,
	[Report] [nvarchar](500) NULL,
	[IsDiscontinued] [bit] NULL,
	[newVersionId] [int] NULL,
	[Description] [text] NOT NULL,
 CONSTRAINT [Course_PK] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Enroll]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enroll](
	[UserID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[Status] [bit] NOT NULL,
	[Progress] [int] NOT NULL,
	[DateEnroll] [date] NOT NULL,
 CONSTRAINT [Enroll_PK] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exam]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exam](
	[ExamID] [int] NOT NULL,
	[ExamName] [nvarchar](300) NOT NULL,
	[CourseID] [int] NOT NULL,
	[Duration] [int] NOT NULL,
 CONSTRAINT [Exam_PK] PRIMARY KEY CLUSTERED 
(
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamDetail]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamDetail](
	[ExamDetailID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ExamID] [int] NOT NULL,
	[TimeStart] [datetime] NULL,
	[TimeEnd] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[Score] [float] NOT NULL,
 CONSTRAINT [ExamDetail_PK] PRIMARY KEY CLUSTERED 
(
	[ExamDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exams]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exams](
	[ExamID] [int] IDENTITY(0,1) NOT NULL,
	[ExamName] [varchar](100) NOT NULL,
	[CourseID] [int] NOT NULL,
	[Duration] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [Exams_PK] PRIMARY KEY CLUSTERED 
(
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lessons]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lessons](
	[LessonID] [int] IDENTITY(0,1) NOT NULL,
	[LessonNumber] [int] NOT NULL,
	[LessonName] [nvarchar](300) NOT NULL,
	[MoocID] [int] NOT NULL,
	[LessonUrl] [nvarchar](300) NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
 CONSTRAINT [Lessons_PK] PRIMARY KEY CLUSTERED 
(
	[LessonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mooc]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mooc](
	[MoocID] [int] IDENTITY(0,1) NOT NULL,
	[MoocNumber] [int] NOT NULL,
	[MoocName] [nvarchar](400) NOT NULL,
	[CourseID] [int] NOT NULL,
 CONSTRAINT [Mooc_PK] PRIMARY KEY CLUSTERED 
(
	[MoocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[NotificationID] [int] IDENTITY(0,1) NOT NULL,
	[FromUserID] [int] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[Link] [nvarchar](500) NULL,
	[Date] [datetime] NOT NULL,
	[IsRead] [bit] NOT NULL,
	[ToUserID] [int] NOT NULL,
 CONSTRAINT [Notification_PK] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Problem]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Problem](
	[problemID] [int] IDENTITY(1,1) NOT NULL,
	[problem] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[problemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Progress]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Progress](
	[UserID] [int] NOT NULL,
	[LessonID] [int] NOT NULL,
	[State] [bit] NULL,
	[CourseID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Progress2]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Progress2](
	[UserID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[NumberLearned] [int] NOT NULL,
 CONSTRAINT [Progress2_PK] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quiz]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quiz](
	[QuizID] [int] IDENTITY(0,1) NOT NULL,
	[QuizContent] [nvarchar](1500) NOT NULL,
	[ExamID] [int] NOT NULL,
 CONSTRAINT [Quiz_PK] PRIMARY KEY CLUSTERED 
(
	[QuizID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reply]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reply](
	[ReplyID] [int] IDENTITY(0,1) NOT NULL,
	[CommentID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Content] [nvarchar](500) NOT NULL,
	[DateReply] [datetime] NOT NULL,
	[Image] [nvarchar](100) NULL,
	[Status] [bit] NOT NULL,
	[Like] [varchar](100) NULL,
	[Dislike] [varchar](100) NULL,
 CONSTRAINT [Reply_PK] PRIMARY KEY CLUSTERED 
(
	[ReplyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReplyReaction]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReplyReaction](
	[UserID] [int] NOT NULL,
	[ReplyID] [int] NOT NULL,
	[isLike] [bit] NOT NULL,
	[isDislike] [bit] NOT NULL,
 CONSTRAINT [ReplyReaction_PK] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[ReplyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Review]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Review](
	[ReviewID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[Rating] [int] NULL,
	[Time] [datetime] NULL,
	[ReviewContent] [nvarchar](500) NULL,
	[isReport] [bit] NULL,
 CONSTRAINT [Review_PK] PRIMARY KEY CLUSTERED 
(
	[ReviewID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReviewReport]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReviewReport](
	[ReviewReportID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[ReviewID] [int] NULL,
	[ProblemID] [int] NULL,
	[Reason] [nvarchar](511) NULL,
	[ReportDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ReviewReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] IDENTITY(0,1) NOT NULL,
	[RoleName] [nvarchar](100) NOT NULL,
 CONSTRAINT [Role_PK] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teacher]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teacher](
	[TeacherID] [int] IDENTITY(0,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Position] [varchar](100) NOT NULL,
	[Workplace] [varchar](100) NULL,
	[PersonalWebsite] [varchar](100) NULL,
	[Facebook] [varchar](100) NULL,
	[Linkedin] [varchar](100) NULL,
	[DateJoin] [datetime] NULL,
	[About] [text] NOT NULL,
	[CV] [varchar](500) NOT NULL,
	[Status] [bit] NULL,
	[DateRequest] [datetime] NULL,
 CONSTRAINT [Teacher_PK] PRIMARY KEY CLUSTERED 
(
	[TeacherID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(0,1) NOT NULL,
	[FullName] [nvarchar](200) NOT NULL,
	[DateOrBirth] [date] NULL,
	[Email] [nvarchar](200) NOT NULL,
	[Password] [nvarchar](500) NULL,
	[Phone] [varchar](100) NULL,
	[Address] [nvarchar](300) NULL,
	[Gender] [bit] NULL,
	[RoleID] [int] NOT NULL,
	[Reason] [nvarchar](500) NULL,
	[TimeBan] [date] NULL,
	[Avatar] [nvarchar](400) NULL,
	[isVerify] [bit] NOT NULL,
 CONSTRAINT [User_PK] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserChoices]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserChoices](
	[UserChoiceID] [int] IDENTITY(0,1) NOT NULL,
	[ExamDetailID] [int] NOT NULL,
	[ChoiceID] [int] NOT NULL,
 CONSTRAINT [UserChoices_PK] PRIMARY KEY CLUSTERED 
(
	[UserChoiceID] ASC,
	[ChoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserReaction]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserReaction](
	[UserID] [int] NOT NULL,
	[CommentID] [int] NOT NULL,
	[isLike] [bit] NOT NULL,
	[isDisLike] [bit] NOT NULL,
 CONSTRAINT [UserReaction_PK] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (0, N'Programing')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (1, N'Cooking')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (2, N'Marketing')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (3, N'Economic')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (4, N'English')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Certificate] ON 

INSERT [dbo].[Certificate] ([CertificateID], [UserID], [CourseID], [Date], [Satus]) VALUES (0, 1, 9, CAST(N'2023-01-10' AS Date), N'0')
INSERT [dbo].[Certificate] ([CertificateID], [UserID], [CourseID], [Date], [Satus]) VALUES (1, 1, 0, CAST(N'2023-11-02' AS Date), N'0')
SET IDENTITY_INSERT [dbo].[Certificate] OFF
GO
SET IDENTITY_INSERT [dbo].[Choices] ON 

INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (1, 3, N'To understand why a user would want to use a product or service at all.', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (2, 3, N'To test whether one version of a product or service is better than another.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (3, 3, N'To learn how users actually use a product or service.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (4, 3, N'To see what problems users have when using a product or service.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (5, 4, N'Conducting interviews about how people use Excel  spreadsheets', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (6, 4, N'Analyzing statistical data about visits to Amazon', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (7, 4, N'Writing software to classify user posts on Facebook', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (8, 4, N'Running an experimental trial to see if users like Google with banner ads', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (9, 5, N'Qualitative research generates hypotheses that can be verified with quantitative research. ', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (10, 5, N'Qualitative research offers potential explanations for outcomes from quantitative research. ', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (11, 5, N'Qualitative research tends to provide close-up, in-depth description while quantitative research tends to provide aggregate, large-scale description', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (12, 5, N'Running an experimental trial to see if users like Google with banne', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (13, 6, N'alk about what other participants have been saying', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (14, 6, N'Make small talk at first.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (15, 6, N'Listen and observe how they respond.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (16, 6, N'Adopt a learning mindset.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (17, 7, N'Golf, because negative events can affect your performance.', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (18, 7, N'Driving, because you have to navigate adversarial situations.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (19, 7, N'Playing jazz, because improvisation is required.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (20, 7, N'Running a marathon, because it is exhausting.', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (21, 8, N'1', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (22, 8, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (23, 8, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (24, 8, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (25, 9, N'2', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (26, 9, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (27, 9, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (28, 9, N'5', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (29, 10, N'1', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (30, 10, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (31, 10, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (32, 10, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (33, 11, N'1', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (34, 11, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (35, 11, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (36, 11, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (38, 11, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (39, 11, N'3', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (40, 11, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (41, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (42, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (43, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (44, 12, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (45, 12, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (46, 12, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (47, 12, N'5', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (48, 11, N'1', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (49, 11, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (50, 11, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (51, 11, N'4', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (52, 11, N'la the1', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (53, 11, N'la the 2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (54, 11, N'la the 3', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (55, 11, N'la the 4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (56, 11, N'12', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (57, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (58, 15, N'1', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (59, 15, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (60, 15, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (61, 15, N'4', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (62, 14, N'1', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (63, 14, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (64, 14, N'3', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (65, 14, N'4', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (66, 11, N'1', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (67, 11, N'2', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (68, 11, N'3', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (69, 11, N'4', 1)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (70, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (71, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (72, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (73, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (74, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (75, 11, N'New answer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (76, 17, N'New an1swer', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (77, 17, N'New answ3er', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (78, 17, N'New answe41r', 0)
INSERT [dbo].[Choices] ([ChoiceID], [QuizID], [Description], [IsTrue]) VALUES (79, 17, N'New answer32', 1)
SET IDENTITY_INSERT [dbo].[Choices] OFF
GO
SET IDENTITY_INSERT [dbo].[Comment] ON 

INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (0, 55, 1, N'ba nay kho qua', 1, 0, 1, N'', 0, CAST(N'2023-11-03T10:31:40.827' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (1, 55, 1, N'hi', 1, 1, 1, N'', 0, CAST(N'2023-11-03T10:38:49.337' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (3, 77, 1, N'12345', 6, 2, 1, N'', 0, CAST(N'2023-11-03T11:35:36.550' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (4, 5, 1, N'a', 0, 1, 0, N'', 0, CAST(N'2023-11-03T12:41:12.980' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (6, 58, 1, N'alas', 1, 0, 1, N'', 0, CAST(N'2023-11-03T14:35:21.057' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (7, 0, 1, N'hi', 1, 0, 0, N'', 0, CAST(N'2023-11-06T14:05:38.557' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (8, 76, 1, N'hy', 4, 1, 1, N'', 0, CAST(N'2023-11-06T14:15:19.797' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (9, 86, 14, N'a', 0, 0, 0, N'', 0, CAST(N'2023-11-06T16:23:27.803' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (10, 0, 36, N'12', 1, 0, 1, N'', 0, CAST(N'2023-11-06T16:47:18.363' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (11, 1, 0, N'12', 1, 0, 1, N'', 0, CAST(N'2023-11-07T13:35:11.517' AS DateTime))
INSERT [dbo].[Comment] ([CommentID], [LessonID], [UserID], [Content], [Like], [Dislike], [IsReply], [Image], [Status], [CommentDate]) VALUES (12, 1, 0, N'56', 0, 1, 1, N'', 0, CAST(N'2023-11-07T13:36:56.617' AS DateTime))
SET IDENTITY_INSERT [dbo].[Comment] OFF
GO
SET IDENTITY_INSERT [dbo].[Course] ON 

INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (0, 0, 0, N'java.jpg', N'Head First Java by Kathy Sierra &', CAST(N'2023-09-01' AS Date), 2, NULL, 1, NULL, N'Java is a widely-used programming language for coding web applications. It has been a popular choice among developers for over two decades, with millions of Java applications in use today. Java is a multi-platform, object-oriented, and network-centric language that can be used as a platform in itself.')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (2, 1, 1, N'.net.jpg', N'Cooking begin', CAST(N'2023-01-16' AS Date), 3, NULL, 1, NULL, N'Day la sach hoc nau an')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (3, 2, 3, N'css.png', N'Kinh te hoc', CAST(N'2023-01-19' AS Date), 2, NULL, 1, NULL, N'day la sach hoc ve kinh te')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (4, 2, 2, N'c++.jpg', N'HTML AND CSS', CAST(N'2023-08-19' AS Date), 5, NULL, 1, NULL, N'day la khoa hoc ve front end css html')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (5, 1, 1, N'python.jpg', N'Cooking Master', CAST(N'2023-01-29' AS Date), 3, NULL, 1, NULL, N'day la khoa hoc day nau an nang cao')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (6, 6, 4, N'.net.jpg', N'EngLish For Begin', CAST(N'2023-02-22' AS Date), 7.8, NULL, 1, NULL, N'Khoa hoc tieng anh gianh cho nguoi bat dau')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (7, 1, 0, N'.net.jpg', N'Javascript', CAST(N'2023-10-20' AS Date), NULL, NULL, 1, 4, N'Java is a widely-used programming language for coding web applications. It has been a popular choice among developers for over two decades, with millions of Java applications in use today. Java is a multi-platform, object-oriented, and network-centric language that can be used as a platform in itself.')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (8, 1, 0, N'1542957949936317424252.png', N'Javascript', CAST(N'2023-10-20' AS Date), NULL, NULL, 1, 7, N'Javascript co ban')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (9, 1, 0, N'02-25-2018-html-css-announcement-blog.png', N'ReactJS', CAST(N'2023-10-20' AS Date), NULL, NULL, 1, 7, N'  React co ban 2')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (15, 1, 3, N'2023-10-16 (3).png', N'PRN231', CAST(N'2023-11-03' AS Date), NULL, NULL, 1, 1, N'123')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (16, 1, 1, N'maxresdefault.jpg', N'PRO192', CAST(N'2023-11-03' AS Date), 4, NULL, 0, 1, N'haacas')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (17, 1, 0, N'vuejs.png', N'VUE JS', CAST(N'2023-11-06' AS Date), NULL, NULL, 1, 1, N'ngocduc')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (18, 36, 2, N'02-25-2018-html-css-announcement-blog.png', N'CSS', CAST(N'2023-11-06' AS Date), NULL, NULL, 1, 12, N'123')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (19, 0, 0, N'maxresdefault.jpg', N'PRN231', CAST(N'2023-11-06' AS Date), NULL, NULL, 1, 1, N'123')
INSERT [dbo].[Course] ([CourseID], [UserID], [CategoryID], [CourseImg], [CourseName], [Publish], [Duration], [Report], [IsDiscontinued], [newVersionId], [Description]) VALUES (20, 0, 2, N'Screenshot 2023-09-21 215047.png', N'PRN231', CAST(N'2023-11-07' AS Date), NULL, NULL, NULL, 4, N'rty')
SET IDENTITY_INSERT [dbo].[Course] OFF
GO
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 0, 0, 100, CAST(N'2023-11-06' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 2, 0, 89, CAST(N'2023-05-27' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 3, 0, 22, CAST(N'2022-05-19' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 4, 0, 0, CAST(N'2023-10-14' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 7, 0, 0, CAST(N'2023-10-25' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 8, 0, 0, CAST(N'2023-10-25' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (0, 9, 0, 0, CAST(N'2023-10-25' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (1, 0, 0, 100, CAST(N'2023-11-03' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (1, 9, 0, 100, CAST(N'2023-11-03' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (1, 15, 0, 33, CAST(N'2023-11-03' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (1, 16, 0, 100, CAST(N'2023-11-03' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 0, 0, 22, CAST(N'2022-12-31' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 2, 0, 0, CAST(N'2023-10-13' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 3, 0, 12, CAST(N'2022-12-13' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 4, 0, 0, CAST(N'2023-10-22' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 5, 0, 0, CAST(N'2023-10-22' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 6, 0, 0, CAST(N'2023-10-22' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (2, 17, 0, 0, CAST(N'2023-11-06' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (14, 0, 0, 100, CAST(N'2023-11-06' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (14, 18, 0, 0, CAST(N'2023-11-06' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (36, 0, 0, 11, CAST(N'2023-11-06' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (54, 0, 0, 0, CAST(N'2023-10-13' AS Date))
INSERT [dbo].[Enroll] ([UserID], [CourseID], [Status], [Progress], [DateEnroll]) VALUES (54, 3, 0, 0, CAST(N'2023-10-13' AS Date))
GO
SET IDENTITY_INSERT [dbo].[ExamDetail] ON 

INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (3, 0, 0, CAST(N'2023-02-02T02:12:44.000' AS DateTime), CAST(N'2023-02-02T23:22:22.000' AS DateTime), 0, 75.52)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (7, 0, 0, CAST(N'2023-01-31T12:44:22.000' AS DateTime), CAST(N'2023-09-14T04:12:44.000' AS DateTime), 0, 20.3)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (8, 0, 0, CAST(N'2011-01-01T12:32:11.000' AS DateTime), CAST(N'2023-01-29T17:22:11.000' AS DateTime), 0, 45.62)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (10, 0, 0, CAST(N'2023-10-01T14:15:55.093' AS DateTime), CAST(N'2023-10-01T14:15:55.093' AS DateTime), 0, 34.11)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (11, 0, 0, CAST(N'2023-10-01T14:16:38.203' AS DateTime), CAST(N'2023-10-01T14:16:38.203' AS DateTime), 1, 90)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (13, 0, 0, CAST(N'2023-10-02T01:30:00.910' AS DateTime), CAST(N'2023-10-02T01:30:00.910' AS DateTime), 1, 80)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (14, 0, 0, CAST(N'2023-10-03T15:15:06.420' AS DateTime), CAST(N'2023-10-03T15:15:06.420' AS DateTime), 0, 60)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (15, 0, 0, CAST(N'2023-10-03T15:17:02.030' AS DateTime), CAST(N'2023-10-03T15:17:02.030' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (16, 0, 0, CAST(N'2023-10-03T15:21:43.323' AS DateTime), CAST(N'2023-10-03T15:21:43.323' AS DateTime), 0, 20)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (17, 0, 0, CAST(N'2023-10-03T15:23:00.587' AS DateTime), CAST(N'2023-10-03T15:23:00.587' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (18, 0, 0, CAST(N'2023-10-05T16:44:55.000' AS DateTime), CAST(N'2023-10-05T16:44:55.000' AS DateTime), 0, 60)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (19, 0, 0, CAST(N'2023-10-18T00:03:24.347' AS DateTime), CAST(N'2023-10-18T00:03:39.347' AS DateTime), 0, 40)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (20, 0, 0, CAST(N'2023-10-18T00:03:48.477' AS DateTime), CAST(N'2023-10-18T00:03:57.477' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (21, 0, 0, CAST(N'2023-10-18T00:04:21.597' AS DateTime), CAST(N'2023-10-18T00:04:27.597' AS DateTime), 0, 40)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (149, 2, 0, CAST(N'2023-10-20T15:38:55.673' AS DateTime), CAST(N'2023-10-20T15:39:00.673' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (150, 2, 1, CAST(N'2023-10-21T22:38:51.457' AS DateTime), CAST(N'2023-10-21T22:38:58.457' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (151, 2, 1, CAST(N'2023-10-24T01:12:57.690' AS DateTime), CAST(N'2023-10-24T01:13:06.690' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (152, 0, 0, CAST(N'2023-10-25T10:50:27.043' AS DateTime), CAST(N'2023-10-25T10:50:31.043' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (153, 1, 0, CAST(N'2023-11-03T12:43:06.817' AS DateTime), CAST(N'2023-11-03T12:43:10.817' AS DateTime), 1, 80)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (156, 1, 1, CAST(N'2023-11-03T12:54:15.480' AS DateTime), CAST(N'2023-11-03T12:54:20.480' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (157, 1, 0, CAST(N'2023-11-03T13:20:46.950' AS DateTime), CAST(N'2023-11-03T13:20:51.950' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (158, 1, 1, CAST(N'2023-11-03T13:23:29.870' AS DateTime), CAST(N'2023-11-03T13:23:34.870' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (159, 1, 1, CAST(N'2023-11-03T13:24:03.950' AS DateTime), CAST(N'2023-11-03T13:24:08.950' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (160, 1, 1, CAST(N'2023-11-03T13:24:45.953' AS DateTime), CAST(N'2023-11-03T13:24:55.953' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (161, 1, 1, CAST(N'2023-11-03T13:27:33.207' AS DateTime), CAST(N'2023-11-03T13:27:38.207' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (162, 1, 2, CAST(N'2023-11-03T14:02:31.713' AS DateTime), CAST(N'2023-11-03T14:02:34.713' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (163, 1, 2, CAST(N'2023-11-03T14:02:37.967' AS DateTime), CAST(N'2023-11-03T14:02:38.967' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (164, 1, 2, CAST(N'2023-11-03T14:02:41.887' AS DateTime), CAST(N'2023-11-03T14:02:42.887' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (165, 1, 2, CAST(N'2023-11-03T14:06:54.660' AS DateTime), CAST(N'2023-11-03T14:06:56.660' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (166, 1, 0, CAST(N'2023-11-06T14:03:58.827' AS DateTime), CAST(N'2023-11-06T14:04:24.827' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (167, 1, 0, CAST(N'2023-11-06T14:04:44.987' AS DateTime), CAST(N'2023-11-06T14:04:52.987' AS DateTime), 1, 80)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (168, 14, 0, CAST(N'2023-11-06T16:30:29.063' AS DateTime), CAST(N'2023-11-06T16:30:33.063' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (169, 14, 1, CAST(N'2023-11-06T16:30:49.373' AS DateTime), CAST(N'2023-11-06T16:30:57.373' AS DateTime), 0, 0)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (170, 14, 1, CAST(N'2023-11-06T16:32:33.457' AS DateTime), CAST(N'2023-11-06T16:32:37.457' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (171, 14, 1, CAST(N'2023-11-06T16:33:07.687' AS DateTime), CAST(N'2023-11-06T16:33:10.687' AS DateTime), 0, 66)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (172, 14, 1, CAST(N'2023-11-06T16:33:31.187' AS DateTime), CAST(N'2023-11-06T16:33:35.187' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (173, 14, 1, CAST(N'2023-11-06T16:38:18.637' AS DateTime), CAST(N'2023-11-06T16:38:21.637' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (174, 0, 0, CAST(N'2023-11-07T16:05:09.307' AS DateTime), CAST(N'2023-11-07T16:05:12.307' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (175, 0, 0, CAST(N'2023-11-07T16:10:13.537' AS DateTime), CAST(N'2023-11-07T16:10:17.537' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (176, 0, 1, CAST(N'2023-11-07T16:11:02.063' AS DateTime), CAST(N'2023-11-07T16:11:05.063' AS DateTime), 1, 100)
INSERT [dbo].[ExamDetail] ([ExamDetailID], [UserID], [ExamID], [TimeStart], [TimeEnd], [Status], [Score]) VALUES (177, 0, 1, CAST(N'2023-11-07T16:11:40.677' AS DateTime), CAST(N'2023-11-07T16:11:43.677' AS DateTime), 1, 100)
SET IDENTITY_INSERT [dbo].[ExamDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[Exams] ON 

INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (0, N'Java Core For Begin Student', 0, 19, 20)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (1, N'Java OOP 2 For Begin Student', 0, 25, 15)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (2, N'exam1', 9, 12, 10)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (3, N'Exam1', 17, 10, 10)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (4, N'bai thi so 1', 18, 12, 10)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (5, N'exam2', 17, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (6, N'exam1', 19, 12, 31)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (7, N'exam1', 19, 12, 31)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (8, N'123131', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (9, N'123', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (10, N'1234', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (11, N'dfhdhdhdhdfdfge', 19, 33, 33)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (12, N'123', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (13, N'123', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (14, N'ukuk', 19, 4, 5)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (15, N'123', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (16, N'1231231dfwefwfferreferfwreferfw4 54t45t 45t45t45t', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (17, N'eeee#$@$@!@#!@#!@#', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (18, N'qwdqdqwwsqw', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (19, N'qwdqdqwwsqw', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (20, N'exam112313eeffcwsfcwe', 19, 121, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (21, N'duc', 19, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (22, N'test1', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (23, N'test1', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (24, N'test1', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (25, N'test111111', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (26, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (27, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (28, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (29, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (30, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (31, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (32, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (33, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (34, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (35, N'abc', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (36, N'q?qw', 20, 12, 12)
INSERT [dbo].[Exams] ([ExamID], [ExamName], [CourseID], [Duration], [Quantity]) VALUES (37, N'12', 20, 12, 12)
SET IDENTITY_INSERT [dbo].[Exams] OFF
GO
SET IDENTITY_INSERT [dbo].[Lessons] ON 

INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (0, 1, N'How to install the environment', 0, N'PKkKCv7Lno0', N'cach de cai dat moi truong')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (1, 2, N'How to install and run netbean environment', 0, N'LaQvccaXi-E', N'cach de chay netbean')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (2, 3, N'How to install java 1.8', 1, N'VOww8yPUcJE', N'chay java')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (3, 4, N'How to install java 1.20', 1, N'PWbsH2q8g3c', N'java 1.20')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (4, 5, N'Int String Boolean', 2, N'YmXTeuw_-8o', N'var')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (5, 7, N'Servlet', 3, N'R1e83SdO8nw', N'serlvet jsp')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (6, 1, N'JAVAAA', 4, N'HQiharwG4kI', N'java')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (7, 6, N'JSP JSLT', 2, N'PKkKCv7Lno0', N'jsp')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (8, 2, N'python', 4, N'PKkKCv7Lno0', N'python')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (10, 3, N'Dotnet 2', 5, N'fyMgBQioTLo', N'dotnet2')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (11, 4, N'Dotnet 3', 6, N'fyMgBQioTLo', N'dotnet3')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (12, 5, N'dotnet 4', 7, N'fyMgBQioTLo', N'dotnet4')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (13, 2, N'Dotnet 1', 4, N'fyMgBQioTLo', N'dotnet1')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (52, 1, N'1', 4, N'hasdfa', N'In this step-by-step tutorial, learn how to create a simple web page using HTML. You ll learn the basics of HTML, from how to structure an HTML document to how to use some of the most common HTML elements.')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (55, 1, N'What is React? Why do you learning React', 27, N'0sbdOpU9o6o', N'Neu cac ban thay hay thi co the dang ki kenh ')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (56, 2, N'What is SPA/MPA ?2', 27, N'0sbdOpU9o6o', N'Neu cac ban thay hay')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (58, 3, N'Arrow Function', 28, N'0sbdOpU9o6o', N'dang ki kenh ')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (67, 4, N'que', 43, N'2xIdharGZCE', N'eqw')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (75, 5, N'123', 50, N'5GRVNAh37O8', N'asdaf')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (76, 1, N'https://www.youtube.com/watch?v=0sbdOpU9o6o', 53, N'0sbdOpU9o6o', N'')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (77, 2, N'lesson2', 53, N'PKkKCv7Lno0', N'https://www.youtube.com/watch?v=0sbdOpU9o6o')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (78, 1, N'lesson 3', 54, N'PKkKCv7Lno0', N'https://www.youtube.com/watch?v=0sbdOpU9o6o')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (79, 1, N'lesson1', 55, N'PKkKCv7Lno0', N'hihihihih')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (80, 2, N'lesson2', 55, N'PKkKCv7Lno0', N'adasd')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (81, 1, N'lesson1', 56, N'PKkKCv7Lno0', N'bai hoc 1')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (82, 2, N'lesson2', 56, N'PKkKCv7Lno0', N'bai hoc 2')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (83, 3, N'lesson3', 56, N'PKkKCv7Lno0', N'lesson3')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (84, 1, N'lesson4', 57, N'PKkKCv7Lno0', N'bai so 4')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (85, 1, N'lesson5', 58, N'PKkKCv7Lno0', N'lesson5')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (86, 1, N'the a', 59, N'1jTSZWRFEB8', N'https://www.youtube.com/watch?v=1jTSZWRFEB8')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (87, 2, N'the div', 59, N'1jTSZWRFEB8', N'https://www.youtube.com/watch?v=1jTSZWRFEB8')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (88, 1, N'css *', 60, N'1jTSZWRFEB8', N'https://www.youtube.com/watch?v=1jTSZWRFEB8')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (89, 1, N'js co ban', 61, N'1jTSZWRFEB8', N'https://www.youtube.com/watch?v=1jTSZWRFEB8')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (90, 2, N'js nag cao', 61, N'1jTSZWRFEB8', N'https://www.youtube.com/watch?v=1jTSZWRFEB8')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (91, 1, N'12', 63, N'zQSKL8Ehwjw', N'https://www.youtube.com/watch?v=zQSKL8Ehwjw')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (92, 1, N'https://www.youtube.com/watch?v=zQSKL8Ehwjw', 64, N'zQSKL8Ehwjw', N'https://www.youtube.com/watch?v=zQSKL8Ehwjw')
INSERT [dbo].[Lessons] ([LessonID], [LessonNumber], [LessonName], [MoocID], [LessonUrl], [Description]) VALUES (93, 2, N'https://www.youtube.com/watch?v=zQSKL8Ehwjw', 63, N'zQSKL8Ehwjw', N'https://www.youtube.com/watch?v=zQSKL8Ehwjw')
SET IDENTITY_INSERT [dbo].[Lessons] OFF
GO
SET IDENTITY_INSERT [dbo].[Mooc] ON 

INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (0, 1, N'Introduction to the Java programming', 0)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (1, 2, N'Learn about variables in java', 0)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (2, 3, N'Learn about Java OOP', 0)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (3, 4, N'Introduction to Servlets and Jsp/Jstl', 0)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (4, 1, N'Dotnet', 2)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (5, 2, N'Dot net asp', 3)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (6, 3, N'Dotnet mvc', 3)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (7, 4, N'Dotnet api', 3)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (27, 1, N'Introduction', 9)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (28, 2, N'On lai ES6', 9)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (43, 1, N'HTML & CSS Basic', 7)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (44, 2, N'JavaScript ES6', 7)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (49, 2, N'JavaScript ES6', 2)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (50, 1, N'HTML & CSS', 8)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (51, 2, N'JS ES6', 8)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (52, 3, N'test', 9)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (53, 1, N'test mooc1', 15)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (54, 2, N'test mooc2', 15)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (55, 1, N'mooc1', 16)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (56, 1, N'mooc1', 17)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (57, 2, N'mooc2', 17)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (58, 3, N'mooc3', 17)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (59, 1, N'cac the co ban', 18)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (60, 2, N'css', 18)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (61, 3, N'js', 18)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (62, 1, N'2', 19)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (63, 1, N'1', 20)
INSERT [dbo].[Mooc] ([MoocID], [MoocNumber], [MoocName], [CourseID]) VALUES (64, 2, N'2', 20)
SET IDENTITY_INSERT [dbo].[Mooc] OFF
GO
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (0, 50, N'lam teacher da bi tu choi.. hay check mail de biet them chi tiet co gi thi lien he voi gia chung toi', NULL, CAST(N'2023-02-01T00:00:00.000' AS DateTime), 0, 0)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (1, 51, N'thong bao so 1', NULL, CAST(N'2023-01-12T00:00:00.000' AS DateTime), 1, 0)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (2, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=9', CAST(N'2023-11-03T10:43:05.320' AS DateTime), 1, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (3, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=9', CAST(N'2023-11-03T10:43:54.683' AS DateTime), 1, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (4, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=9', CAST(N'2023-11-03T10:44:19.470' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (5, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=15', CAST(N'2023-11-03T12:00:22.520' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (6, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=15', CAST(N'2023-11-03T12:27:26.987' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (7, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=15', CAST(N'2023-11-03T12:28:46.647' AS DateTime), 1, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (8, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=15', CAST(N'2023-11-03T12:31:04.797' AS DateTime), 1, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (9, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=9', CAST(N'2023-11-03T14:36:28.737' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (10, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=9', CAST(N'2023-11-03T14:36:57.340' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (11, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=15', CAST(N'2023-11-06T14:28:04.767' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (12, 1, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=15', CAST(N'2023-11-06T14:28:39.790' AS DateTime), 0, 1)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (13, 0, N'Congratulations on being approved as a teacher. You can now post your course', N'', CAST(N'2023-11-06T15:44:56.597' AS DateTime), 1, 2)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (14, 0, N'Congratulations on being approved as a teacher. You can now post your course', N'', CAST(N'2023-11-06T16:12:06.753' AS DateTime), 1, 36)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (15, 0, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=0', CAST(N'2023-11-07T13:36:05.453' AS DateTime), 0, 0)
INSERT [dbo].[Notification] ([NotificationID], [FromUserID], [Content], [Link], [Date], [IsRead], [ToUserID]) VALUES (16, 0, N'has responded to your comment', N'http://localhost:9999/ELearning/learning/hi.jsp?courseId=0', CAST(N'2023-11-07T13:37:28.897' AS DateTime), 0, 0)
SET IDENTITY_INSERT [dbo].[Notification] OFF
GO
SET IDENTITY_INSERT [dbo].[Problem] ON 

INSERT [dbo].[Problem] ([problemID], [problem]) VALUES (1, N'Posting inappropriate content')
INSERT [dbo].[Problem] ([problemID], [problem]) VALUES (2, N'Spam')
INSERT [dbo].[Problem] ([problemID], [problem]) VALUES (3, N'Other')
SET IDENTITY_INSERT [dbo].[Problem] OFF
GO
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 56, 1, 9)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 58, 1, 9)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 0, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 7, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 5, 0, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 79, 1, 16)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 80, 1, 16)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 86, 1, 18)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 1, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 4, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (36, 0, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 1, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 2, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 3, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 4, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 7, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 76, 1, 15)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 77, 1, 15)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 2, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 4, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (2, 82, 0, 17)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 87, 1, 18)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 0, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 7, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 5, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (36, 1, 0, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 0, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (0, 5, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 55, 1, 9)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 1, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (1, 3, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (2, 81, 1, 17)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 2, 1, 0)
INSERT [dbo].[Progress] ([UserID], [LessonID], [State], [CourseID]) VALUES (14, 3, 1, 0)
GO
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (0, 0, 29)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (0, 4, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (0, 7, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (0, 8, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (0, 9, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (1, 0, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (1, 9, 2)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (2, 0, 7)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (2, 4, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (2, 5, 0)
INSERT [dbo].[Progress2] ([UserID], [CourseID], [NumberLearned]) VALUES (2, 6, 0)
GO
SET IDENTITY_INSERT [dbo].[Quiz] ON 

INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (3, N'Which of the following is NOT a reason to conduct a user needs assessment?', 0)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (4, N'At what point in a product or service''s life-cycle should one conduct a user needs assessment? ', 0)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (5, N'Which of the following steps will you be learning about in this user needs assessment course? ', 0)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (6, N'What of the following tasks is most representative of the kind of work involved in qualitative research?', 0)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (7, N'Who of the following describes potential relationships between qualitative and quantitative research? ', 0)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (8, N'1+1=', 1)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (9, N'2+2=', 1)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (10, N'3+3=', 1)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (11, N'3-2=
                        ', 3)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (12, N'1
                        ', 2)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (13, N'New question', 3)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (14, N'2+2=
                        ', 4)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (15, N'1+1=12
                        ', 4)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (16, N'New question', 4)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (17, N'ngocduc
                        ', 6)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (18, N'New question', 6)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (19, N'New question', 6)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (20, N'New question', 21)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (21, N'New question', 21)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (22, N'New question', 21)
INSERT [dbo].[Quiz] ([QuizID], [QuizContent], [ExamID]) VALUES (23, N'New question', 37)
SET IDENTITY_INSERT [dbo].[Quiz] OFF
GO
SET IDENTITY_INSERT [dbo].[Reply] ON 

INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (0, 1, 1, N'123', CAST(N'2023-11-03T10:43:05.320' AS DateTime), N'', 0, N'0', N'0')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (1, 0, 1, N'12', CAST(N'2023-11-03T10:43:54.683' AS DateTime), N'', 0, N'0', N'0')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (2, 0, 1, N'123331', CAST(N'2023-11-03T10:44:19.470' AS DateTime), N'', 0, N'0', N'0')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (3, 3, 1, N'1234', CAST(N'2023-11-03T12:00:22.520' AS DateTime), N'', 0, N'0', N'1')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (7, 6, 1, N'12', CAST(N'2023-11-03T14:36:28.737' AS DateTime), N'', 0, N'0', N'0')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (8, 6, 1, N'646', CAST(N'2023-11-03T14:36:57.340' AS DateTime), N'', 0, N'0', N'0')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (9, 8, 1, N'123', CAST(N'2023-11-06T14:28:04.767' AS DateTime), N'', 0, N'0', N'1')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (10, 8, 1, N'12', CAST(N'2023-11-06T14:28:39.790' AS DateTime), N'', 0, N'1', N'0')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (11, 10, 36, N'122', CAST(N'2023-11-06T16:50:42.880' AS DateTime), N'', 0, N'0', N'1')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (12, 11, 0, N'12', CAST(N'2023-11-07T13:36:05.453' AS DateTime), N'', 0, N'0', N'1')
INSERT [dbo].[Reply] ([ReplyID], [CommentID], [UserID], [Content], [DateReply], [Image], [Status], [Like], [Dislike]) VALUES (13, 12, 0, N'12', CAST(N'2023-11-07T13:37:28.897' AS DateTime), N'', 0, N'1', N'0')
SET IDENTITY_INSERT [dbo].[Reply] OFF
GO
INSERT [dbo].[ReplyReaction] ([UserID], [ReplyID], [isLike], [isDislike]) VALUES (0, 12, 0, 1)
INSERT [dbo].[ReplyReaction] ([UserID], [ReplyID], [isLike], [isDislike]) VALUES (0, 13, 1, 0)
INSERT [dbo].[ReplyReaction] ([UserID], [ReplyID], [isLike], [isDislike]) VALUES (1, 3, 0, 1)
INSERT [dbo].[ReplyReaction] ([UserID], [ReplyID], [isLike], [isDislike]) VALUES (1, 9, 0, 1)
INSERT [dbo].[ReplyReaction] ([UserID], [ReplyID], [isLike], [isDislike]) VALUES (1, 10, 1, 0)
INSERT [dbo].[ReplyReaction] ([UserID], [ReplyID], [isLike], [isDislike]) VALUES (36, 11, 0, 1)
GO
SET IDENTITY_INSERT [dbo].[Review] ON 

INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (1, 22, 0, 5, CAST(N'2023-05-12T00:00:00.000' AS DateTime), N'This is a good and very useful course. I recommend taking this course', 0)
INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (2, 2, 0, 1, CAST(N'2023-01-20T00:00:00.000' AS DateTime), N'This is not a suitable course for me because the way the course is organized has many problems', 0)
INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (3, 1, 9, 5, CAST(N'2023-11-03T10:30:41.707' AS DateTime), N'12', 0)
INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (4, 1, 0, 5, CAST(N'2023-11-03T13:45:20.293' AS DateTime), N'q', 0)
INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (5, 1, 16, 5, CAST(N'2023-11-03T14:33:50.880' AS DateTime), N'asd', 0)
INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (6, 14, 0, 4, CAST(N'2023-11-06T16:38:58.897' AS DateTime), N'okee', 0)
INSERT [dbo].[Review] ([ReviewID], [UserID], [CourseID], [Rating], [Time], [ReviewContent], [isReport]) VALUES (7, 0, 0, 3, CAST(N'2023-11-07T16:14:08.170' AS DateTime), N'hay ', 0)
SET IDENTITY_INSERT [dbo].[Review] OFF
GO
SET IDENTITY_INSERT [dbo].[ReviewReport] ON 

INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (1, 1, 1, 1, N'ko phu hop', CAST(N'2023-10-18T00:00:00.000' AS DateTime))
INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (2, 0, 2, 1, N'Ten khong hop le', CAST(N'2023-10-22T00:00:00.000' AS DateTime))
INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (3, 0, 1, 3, N'i hate her', CAST(N'2023-10-22T00:00:00.000' AS DateTime))
INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (4, 0, 2, 2, N'tên ko phù hợp', CAST(N'2023-10-22T23:29:04.267' AS DateTime))
INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (5, 0, 2, 3, N'áda', CAST(N'2023-10-22T23:34:43.000' AS DateTime))
INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (6, 0, 2, 1, N'4123', CAST(N'2023-10-22T23:35:41.180' AS DateTime))
INSERT [dbo].[ReviewReport] ([ReviewReportID], [UserID], [ReviewID], [ProblemID], [Reason], [ReportDate]) VALUES (7, 0, 2, 1, N'abacas', CAST(N'2023-10-22T23:36:55.667' AS DateTime))
SET IDENTITY_INSERT [dbo].[ReviewReport] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (0, N'User')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (1, N'Teacher')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (2, N'Moderator')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (3, N'Admin System')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (4, N'Admin Business')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Teacher] ON 

INSERT [dbo].[Teacher] ([TeacherID], [UserID], [Position], [Workplace], [PersonalWebsite], [Facebook], [Linkedin], [DateJoin], [About], [CV], [Status], [DateRequest]) VALUES (0, 2, N'Teacher Programing', N'FPT University', N'http://localhost:9999/ELearning2', N'https://www.facebook.com/ngduc101/', N'https://www.linkedin.com/in/ngoc-duc-921461208/', CAST(N'2023-11-06T15:44:56.540' AS DateTime), N'Toi ten la duc ', N'http://localhost:9999/ELearning2/userProfile/profile.jsp', 1, NULL)
INSERT [dbo].[Teacher] ([TeacherID], [UserID], [Position], [Workplace], [PersonalWebsite], [Facebook], [Linkedin], [DateJoin], [About], [CV], [Status], [DateRequest]) VALUES (1, 1, N'Teacher', N'FPT', N'https://chat.openai.com/c/b6d02823-1e40-4dc4-8743-43ccd4eed419', N'https://chat.openai.com/c/b6d02823-1e40-4dc4-8743-43ccd4eed419', N'https://chat.openai.com/c/b6d02823-1e40-4dc4-8743-43ccd4eed419', CAST(N'2023-11-06T15:44:56.540' AS DateTime), N'toi ten la ngoc duc ', N'https://chat.openai.com/c/b6d02823-1e40-4dc4-8743-43ccd4eed419', 1, CAST(N'2023-11-06T15:16:50.093' AS DateTime))
INSERT [dbo].[Teacher] ([TeacherID], [UserID], [Position], [Workplace], [PersonalWebsite], [Facebook], [Linkedin], [DateJoin], [About], [CV], [Status], [DateRequest]) VALUES (2, 0, N'1', N'2', N'3', N'4', N'5', CAST(N'2023-11-06T15:44:56.540' AS DateTime), N'7', N'6', NULL, CAST(N'2023-11-06T16:03:06.587' AS DateTime))
INSERT [dbo].[Teacher] ([TeacherID], [UserID], [Position], [Workplace], [PersonalWebsite], [Facebook], [Linkedin], [DateJoin], [About], [CV], [Status], [DateRequest]) VALUES (3, 36, N'12', N'3', N'123', N'123', N'12313', CAST(N'2023-11-06T16:12:06.697' AS DateTime), N'12313', N'123', 1, CAST(N'2023-11-06T16:11:17.607' AS DateTime))
SET IDENTITY_INSERT [dbo].[Teacher] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (0, N'Ngoc Duc Bui', CAST(N'2022-02-02' AS Date), N'duc@gmail.com', N'202CB962AC59075B964B07152D234B70', N'0123456789', N'Ha Noi', 1, 1, NULL, NULL, N'2.jpg', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (1, N'Nguyen Thị An', CAST(N'2012-09-14' AS Date), N'hi@gmail.com', N'202CB962AC59075B964B07152D234B70', N'1234567896', N'Ha Noi221', 1, 0, NULL, NULL, N'3.jpg', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (2, N'Tran Van Bo', CAST(N'2011-03-28' AS Date), N'ducbui0011@gmail.com', N'0C8ABDB962F042D1857C66DD26B0C87B', N'1234567891', N'Ha Noi', 0, 1, NULL, NULL, N'1.jpg', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (5, N'Ngoc Thi', NULL, N'duc2@gmail.com', N'202CB962AC59075B964B07152D234B70', NULL, NULL, 0, 0, NULL, NULL, N'1.jpg', 0)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (6, N'Le Thi B', NULL, N'hoa@gmail.com', NULL, NULL, NULL, 0, 0, NULL, NULL, N'3.jpg', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (7, N'Bui Nguyen Ngoc Duc', NULL, N'52@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'ducbnnhe150352@fpt.edu.vn', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (8, N'Dex In', NULL, N'specialissndex01@gmail.com', NULL, NULL, NULL, 0, 0, NULL, NULL, N'specialindex01@gmail.com', 0)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (9, N'Bui Nguyen Ngoc Duc', NULL, N'nnh22e150352@fpt.edu.vn', NULL, NULL, NULL, 1, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (10, N'Đức Bùi', NULL, N'180801@gmail.com', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocK6dD3a9ZM9X9s7z0zatwjkpl53A_W-8DK-e1d1JQ_V6w=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (11, N'Bui Nguyen Ngoc ', NULL, N'ducbnnhe1503ss352@fpt.edu', NULL, N'0123456789', N'', 1, 1, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 0)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (12, N'Ngoc 0352218407', CAST(N'2023-09-14' AS Date), N'ducbui00121@gmail.com', N'E10ADC3949BA59ABBE56E057F20F883E', N'0352218407', N'Viet Nam', 1, 0, NULL, NULL, N'https://as2.ftcdn.net/v2/jpg/03/49/49/79/1000_F_349497933_Ly4im8BDmHLaLzgyKg2f2yZOvJjBtlw5.jpg', 0)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (13, N'Ngoc 0352218407', CAST(N'2023-09-07' AS Date), N'specialin01@gmail.com', N'E10ADC3949BA59ABBE56E057F20F883E', N'0352218407', N'Viet Nam', 1, 0, NULL, NULL, N'https://as2.ftcdn.net/v2/jpg/03/49/49/79/1000_F_349497933_Ly4im8BDmHLaLzgyKg2f2yZOvJjBtlw5.jpg', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (14, N'Ngoc Duc', CAST(N'2023-09-08' AS Date), N'specialindex01@gmail.com', N'123456', N'0352218407', N'Viet Nam', 1, 0, NULL, NULL, N'https://as2.ftcdn.net/v2/jpg/03/49/49/79/1000_F_349497933_Ly4im8BDmHLaLzgyKg2f2yZOvJjBtlw5.jpg', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (22, N'Bui Nguyen Ngoc Duc', NULL, N'he150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (23, N'Bui Nguyen Ngoc Duc', NULL, N'duc2bnnhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (24, N'Bui Nguyen Ngoc Duc', NULL, N'ducbwwnnhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (25, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnh1e150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (26, N'Bui Nguyen Ngoc Duc', NULL, N'duc2bnnshe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (27, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnn11he150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (28, N'Bui Nguyen Ngoc Duc', NULL, N'du2222cbnnhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (29, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe15022352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (30, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnh12312312e150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (31, N'Bui Nguyen Ngoc Duc', NULL, N'112', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (32, N'Bui Nguyen Ngoc Duc', NULL, N'ducbn11111nhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (33, N'Bui Nguyen Ngoc Duc', NULL, N'ducb1212nnhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (34, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe15w0352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (35, N'Bui Nguyen Ngoc Duc', NULL, N'ducbn56556nhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (36, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 1, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (44, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe1333350352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (45, N'Bui Nguyen Ngoc Duc', NULL, N'1', N'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (46, N'Bui Nguyen Ngoc Duc', NULL, N'11ducbnnhe1502352@fpt.edu.vn', N'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (47, N'Bui Nguyen Ngoc Duc', NULL, N'ducbn3323424nhe150352@fpt.edu.vn', N'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (48, N'Bui Nguyen Ngoc Duc', NULL, N'ducb2nnhe150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (49, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe122250352@fpt.edu.vn', N'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (50, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe1503512@fpt.edu.vn', N'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (51, N'Bui Nguyen Ngoc Duc', NULL, N'ducbnnhe777150352@fpt.edu.vn', NULL, NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocJLXCjt-Ws0eljYjBnJj0yERxl97i_IJS0ksiKMuXmu=s96-c', 1)
INSERT [dbo].[User] ([UserID], [FullName], [DateOrBirth], [Email], [Password], [Phone], [Address], [Gender], [RoleID], [Reason], [TimeBan], [Avatar], [isVerify]) VALUES (54, N'Đức Bùi', NULL, N'ducbui180801@gmail.com', N'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, 0, 0, NULL, NULL, N'https://lh3.googleusercontent.com/a/ACg8ocK6dD3a9ZM9X9s7z0zatwjkpl53A_W-8DK-e1d1JQ_V6w=s96-c', 1)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[UserChoices] ON 

INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (695, 174, 1)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (696, 174, 5)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (697, 174, 9)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (698, 174, 13)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (699, 174, 17)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (700, 175, 1)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (701, 175, 5)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (702, 175, 9)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (703, 175, 13)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (704, 175, 17)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (705, 176, 21)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (706, 176, 25)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (707, 176, 29)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (708, 177, 21)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (709, 177, 25)
INSERT [dbo].[UserChoices] ([UserChoiceID], [ExamDetailID], [ChoiceID]) VALUES (710, 177, 29)
SET IDENTITY_INSERT [dbo].[UserChoices] OFF
GO
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (0, 1, 1, 0)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (0, 3, 1, 1)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (0, 7, 0, 0)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (0, 8, 1, 0)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (0, 11, 1, 0)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (0, 12, 0, 1)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (1, 3, 0, 1)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (1, 4, 0, 1)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (1, 6, 1, 0)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (1, 8, 0, 1)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (36, 7, 1, 0)
INSERT [dbo].[UserReaction] ([UserID], [CommentID], [isLike], [isDisLike]) VALUES (36, 10, 1, 0)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [User_UN]    Script Date: 11/7/2023 9:20:47 PM ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [User_UN] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comment] ADD  DEFAULT ((0)) FOR [Like]
GO
ALTER TABLE [dbo].[Comment] ADD  DEFAULT ((0)) FOR [Dislike]
GO
ALTER TABLE [dbo].[Reply] ADD  DEFAULT ((0)) FOR [Like]
GO
ALTER TABLE [dbo].[Reply] ADD  DEFAULT ((0)) FOR [Dislike]
GO
ALTER TABLE [dbo].[Certificate]  WITH CHECK ADD  CONSTRAINT [Certificate_FK] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Certificate] CHECK CONSTRAINT [Certificate_FK]
GO
ALTER TABLE [dbo].[Certificate]  WITH CHECK ADD  CONSTRAINT [Certificate_FK_1] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Certificate] CHECK CONSTRAINT [Certificate_FK_1]
GO
ALTER TABLE [dbo].[Choice]  WITH CHECK ADD  CONSTRAINT [Choice_FK] FOREIGN KEY([QuizID])
REFERENCES [dbo].[Quiz] ([QuizID])
GO
ALTER TABLE [dbo].[Choice] CHECK CONSTRAINT [Choice_FK]
GO
ALTER TABLE [dbo].[Choices]  WITH CHECK ADD  CONSTRAINT [Choices_FK] FOREIGN KEY([QuizID])
REFERENCES [dbo].[Quiz] ([QuizID])
GO
ALTER TABLE [dbo].[Choices] CHECK CONSTRAINT [Choices_FK]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [Comment_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [Comment_FK]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [Comment_FK_1] FOREIGN KEY([LessonID])
REFERENCES [dbo].[Lessons] ([LessonID])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [Comment_FK_1]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [Course_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [Course_FK]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD  CONSTRAINT [Course_FK_1] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
ALTER TABLE [dbo].[Course] CHECK CONSTRAINT [Course_FK_1]
GO
ALTER TABLE [dbo].[Enroll]  WITH CHECK ADD  CONSTRAINT [Enroll_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Enroll] CHECK CONSTRAINT [Enroll_FK]
GO
ALTER TABLE [dbo].[Enroll]  WITH CHECK ADD  CONSTRAINT [Enroll_FK_1] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Enroll] CHECK CONSTRAINT [Enroll_FK_1]
GO
ALTER TABLE [dbo].[Exam]  WITH CHECK ADD  CONSTRAINT [Exam_FK] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Exam] CHECK CONSTRAINT [Exam_FK]
GO
ALTER TABLE [dbo].[ExamDetail]  WITH CHECK ADD  CONSTRAINT [ExamDetail_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ExamDetail] CHECK CONSTRAINT [ExamDetail_FK]
GO
ALTER TABLE [dbo].[ExamDetail]  WITH CHECK ADD  CONSTRAINT [ExamDetail_FK_1] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ExamID])
GO
ALTER TABLE [dbo].[ExamDetail] CHECK CONSTRAINT [ExamDetail_FK_1]
GO
ALTER TABLE [dbo].[Exams]  WITH CHECK ADD  CONSTRAINT [Exams_FK] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Exams] CHECK CONSTRAINT [Exams_FK]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [Lessons_FK] FOREIGN KEY([MoocID])
REFERENCES [dbo].[Mooc] ([MoocID])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [Lessons_FK]
GO
ALTER TABLE [dbo].[Mooc]  WITH CHECK ADD  CONSTRAINT [Mooc_FK] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Mooc] CHECK CONSTRAINT [Mooc_FK]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [Notification_FK] FOREIGN KEY([ToUserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [Notification_FK]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [Notification_FK_1] FOREIGN KEY([FromUserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [Notification_FK_1]
GO
ALTER TABLE [dbo].[Progress]  WITH CHECK ADD  CONSTRAINT [Progress_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Progress] CHECK CONSTRAINT [Progress_FK]
GO
ALTER TABLE [dbo].[Progress]  WITH CHECK ADD  CONSTRAINT [Progress_FK_1] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Progress] CHECK CONSTRAINT [Progress_FK_1]
GO
ALTER TABLE [dbo].[Progress]  WITH CHECK ADD  CONSTRAINT [Progress_FK_2] FOREIGN KEY([LessonID])
REFERENCES [dbo].[Lessons] ([LessonID])
GO
ALTER TABLE [dbo].[Progress] CHECK CONSTRAINT [Progress_FK_2]
GO
ALTER TABLE [dbo].[Progress2]  WITH CHECK ADD  CONSTRAINT [Progress2_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Progress2] CHECK CONSTRAINT [Progress2_FK]
GO
ALTER TABLE [dbo].[Progress2]  WITH CHECK ADD  CONSTRAINT [Progress2_FK_1] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Progress2] CHECK CONSTRAINT [Progress2_FK_1]
GO
ALTER TABLE [dbo].[Quiz]  WITH CHECK ADD  CONSTRAINT [Quiz_FK] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ExamID])
GO
ALTER TABLE [dbo].[Quiz] CHECK CONSTRAINT [Quiz_FK]
GO
ALTER TABLE [dbo].[Reply]  WITH CHECK ADD  CONSTRAINT [Reply_FK] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Comment] ([CommentID])
GO
ALTER TABLE [dbo].[Reply] CHECK CONSTRAINT [Reply_FK]
GO
ALTER TABLE [dbo].[Reply]  WITH CHECK ADD  CONSTRAINT [Reply_FK_1] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Reply] CHECK CONSTRAINT [Reply_FK_1]
GO
ALTER TABLE [dbo].[Reply]  WITH CHECK ADD  CONSTRAINT [Reply_FK_2] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Comment] ([CommentID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Reply] CHECK CONSTRAINT [Reply_FK_2]
GO
ALTER TABLE [dbo].[ReplyReaction]  WITH CHECK ADD  CONSTRAINT [ReplyReaction_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ReplyReaction] CHECK CONSTRAINT [ReplyReaction_FK]
GO
ALTER TABLE [dbo].[ReplyReaction]  WITH CHECK ADD  CONSTRAINT [ReplyReaction_FK_1] FOREIGN KEY([ReplyID])
REFERENCES [dbo].[Reply] ([ReplyID])
GO
ALTER TABLE [dbo].[ReplyReaction] CHECK CONSTRAINT [ReplyReaction_FK_1]
GO
ALTER TABLE [dbo].[Review]  WITH CHECK ADD  CONSTRAINT [Review_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Review] CHECK CONSTRAINT [Review_FK]
GO
ALTER TABLE [dbo].[Review]  WITH CHECK ADD  CONSTRAINT [Review_FK_1] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([CourseID])
GO
ALTER TABLE [dbo].[Review] CHECK CONSTRAINT [Review_FK_1]
GO
ALTER TABLE [dbo].[ReviewReport]  WITH CHECK ADD FOREIGN KEY([ProblemID])
REFERENCES [dbo].[Problem] ([problemID])
GO
ALTER TABLE [dbo].[ReviewReport]  WITH CHECK ADD FOREIGN KEY([ReviewID])
REFERENCES [dbo].[Review] ([ReviewID])
GO
ALTER TABLE [dbo].[ReviewReport]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Teacher]  WITH CHECK ADD  CONSTRAINT [Teacher_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Teacher] CHECK CONSTRAINT [Teacher_FK]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [User_FK] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([RoleID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [User_FK]
GO
ALTER TABLE [dbo].[UserChoices]  WITH CHECK ADD  CONSTRAINT [UserChoices_FK] FOREIGN KEY([ExamDetailID])
REFERENCES [dbo].[ExamDetail] ([ExamDetailID])
GO
ALTER TABLE [dbo].[UserChoices] CHECK CONSTRAINT [UserChoices_FK]
GO
ALTER TABLE [dbo].[UserChoices]  WITH CHECK ADD  CONSTRAINT [UserChoices_FK_1] FOREIGN KEY([ChoiceID])
REFERENCES [dbo].[Choices] ([ChoiceID])
GO
ALTER TABLE [dbo].[UserChoices] CHECK CONSTRAINT [UserChoices_FK_1]
GO
ALTER TABLE [dbo].[UserReaction]  WITH CHECK ADD  CONSTRAINT [UserReaction_FK] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[UserReaction] CHECK CONSTRAINT [UserReaction_FK]
GO
ALTER TABLE [dbo].[UserReaction]  WITH CHECK ADD  CONSTRAINT [UserReaction_FK_1] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Comment] ([CommentID])
GO
ALTER TABLE [dbo].[UserReaction] CHECK CONSTRAINT [UserReaction_FK_1]
GO
ALTER TABLE [dbo].[UserReaction]  WITH CHECK ADD  CONSTRAINT [UserReaction_FK_2] FOREIGN KEY([CommentID])
REFERENCES [dbo].[Comment] ([CommentID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserReaction] CHECK CONSTRAINT [UserReaction_FK_2]
GO
/****** Object:  StoredProcedure [dbo].[usp_deleteLessonByID]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    PROCEDURE [dbo].[usp_deleteLessonByID](
	@Lesson_ID int, @Mooc_ID   int
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

		 DELETE u
		 FROM Comment c
		 JOIN UserReaction u ON c.CommentID = u.CommentID
		 WHERE c.LessonID = @Lesson_ID

		 DELETE R
		 FROM Comment c
		 JOIN Reply R ON c.CommentID = r.CommentID
		 WHERE c.LessonID = @Lesson_ID

		 Delete Comment where LessonID = @Lesson_ID

		 DELETE Progress where LessonID = @Lesson_ID

		 update Lessons set LessonNumber = LessonNumber - 1 where LessonNumber > (select LessonNumber from Lessons where LessonID = @Lesson_ID) and MoocID = @Mooc_ID;

		 Delete Lessons where LessonID = @Lesson_ID

		 COMMIT TRAN
	END TRY
	BEGIN CATCH
	    IF(XACT_STATE() = -1)
		BEGIN
		    PRINT 'LOI ROI ROLLBACK'
			ROLLBACK TRAN
		END
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_deleteMoocByID]    Script Date: 11/7/2023 9:20:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_deleteMoocByID](
	@Mooc_ID   int , @courseid int
)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

		 DELETE Progress where MoocID = @Mooc_ID

		 DELETE u
		 FROM Comment c
		 JOIN UserReaction u ON c.CommentID = u.CommentID
		 join Lessons l on l.LessonID = c.LessonID
		 WHERE l.MoocID = @Mooc_ID

		 DELETE R
		 FROM Comment c
		 JOIN Reply R ON c.CommentID = r.CommentID
		 join Lessons l on l.LessonID = c.LessonID
		 WHERE l.MoocID = @Mooc_ID

		 Delete c from Comment c join Lessons l on c.LessonID = l.LessonID where l.MoocID = @Mooc_ID

		 Delete Lessons where MoocID = @Mooc_ID

		 update Mooc set MoocNumber = MoocNumber-1  where MoocNumber > (select MoocNumber from Mooc where MoocID = @Mooc_ID)
		 and CourseID = @courseid

		 delete Mooc where MoocID = @Mooc_ID

		 COMMIT TRAN
	END TRY
	BEGIN CATCH
	    IF(XACT_STATE() = -1)
		BEGIN
		    PRINT 'LOI ROI ROLLBACK'
			ROLLBACK TRAN
		END
	END CATCH
END
GO
